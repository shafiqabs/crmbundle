<?php
/**
 * Created by PhpStorm.
 * User: shafiq
 * Date: 9/29/19
 * Time: 9:09 PM
 */

namespace Terminalbd\CrmBundle\Controller;

use App\Entity\Core\Agent;
use App\Entity\User;
use App\Entity\Admin\Location;
use App\Service\SmsSender;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Terminalbd\CrmBundle\Entity\Api;
use Terminalbd\CrmBundle\Entity\ApiDetails;
use Terminalbd\CrmBundle\Entity\AppVersions;
use Terminalbd\CrmBundle\Entity\BroilerStandard;
use Terminalbd\CrmBundle\Entity\CattleLifeCycle;
use Terminalbd\CrmBundle\Entity\CattleLifeCycleDetails;
use Terminalbd\CrmBundle\Entity\ChickLifeCycle;
use Terminalbd\CrmBundle\Entity\ChickLifeCycleDetails;
use Terminalbd\CrmBundle\Entity\CompanyWiseFeedSale;
use Terminalbd\CrmBundle\Entity\CrmCustomer;
use Terminalbd\CrmBundle\Entity\CrmVisit;
use Terminalbd\CrmBundle\Entity\CrmVisitPlan;
use Terminalbd\CrmBundle\Entity\Expense;
use Terminalbd\CrmBundle\Entity\ExpenseApiResponse;
use Terminalbd\CrmBundle\Entity\ExpenseChart;
use Terminalbd\CrmBundle\Entity\ExpenseChartDetail;
use Terminalbd\CrmBundle\Entity\ExpenseConveyanceDetails;
use Terminalbd\CrmBundle\Entity\ExpenseParticular;
use Terminalbd\CrmBundle\Entity\FarmerComplain;
use Terminalbd\CrmBundle\Entity\FarmerComplainDetails;
use Terminalbd\CrmBundle\Entity\FcrDetails;
use Terminalbd\CrmBundle\Entity\FcrDifferentCompanies;
use Terminalbd\CrmBundle\Entity\FishLifeCycleCulture;
use Terminalbd\CrmBundle\Entity\FishLifeCycleCultureDetails;
use Terminalbd\CrmBundle\Entity\FishLifeCycleNursing;
use Terminalbd\CrmBundle\Entity\LabService;
use Terminalbd\CrmBundle\Entity\LayerLifeCycle;
use Terminalbd\CrmBundle\Entity\LayerLifeCycleDetails;
use Terminalbd\CrmBundle\Entity\NewFarmerIntroduce\FarmerIntroduceDetails;
use Terminalbd\CrmBundle\Entity\Setting;
use Terminalbd\CrmBundle\Entity\SonaliStandard;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Terminalbd\CrmBundle\Repository\FcrDifferentCompaniesRepository;

/**
 * Class ApiController
 * @package Terminalbd\CrmBundle\Controller
 * @Route("/crm/api")
 */
class ApiController extends AbstractController
{

    /**
     * @Route("/login", methods={"POST","GET"}, options={"expose"=true})
     * @param Request $request
     * @param SmsSender $smsSender
     * @param ParameterBagInterface $parameterBag
     * @return JsonResponse|Response
     */
    public function login(Request $request, SmsSender $smsSender, ParameterBagInterface $parameterBag)
    {
        set_time_limit(0);
        ignore_user_abort(true);
        if ($request->getMethod() == 'POST' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {
            $userId = trim($request->request->get('user_id'));
            $findUser = $this->getDoctrine()->getRepository(User::class)->findOneBy(['isDelete' => '0', 'userId' => $userId]);
            if ($findUser) {
                if ($findUser->isEnabled()) {
                    if (!$findUser->getMobile()) {
                        $response = new Response();
                        $response->headers->set('Content-Type', 'application/json');
                        $response->setContent(json_encode([
                            'message' => 'Mobile number does not found!',
                            'status' => '404',
                        ]));
                        $response->setStatusCode(Response::HTTP_NOT_FOUND);
                        return $response;
                    }
                    $userMobile = str_replace('-', '', $findUser->getMobile());

                    $otp = (string)mt_rand(1000, 9999);
                    $message = 'Your OTP is ' . $otp . '.';
                    $smsResponse = $smsSender->sendSms($message, $userMobile);

//                $smsResponse = json_decode($smsResponse, true);

//                if ($smsResponse['message'] === 'Success'){
                    if ($findUser->getUserGroup()->getSlug() == 'administrator') {
                        $roles = $findUser->getUserGroup()->getSlug();
                    } else {
                        $roles = $findUser->getServiceMode() ? $findUser->getServiceMode()->getSlug() : '';
                    }
//                    $rolesSeparated = implode(",", $roles);
                    $upozilas = [];
                    foreach ($findUser->getUpozila() as $location) {
                        $upozilas[] = $location->getId();
                    }

                    $findUser->setOtp($otp);
                    $this->getDoctrine()->getManager()->flush();

                    $locations = implode(",", $upozilas);
                    $data = [
                        'userId' => $findUser->getId(),
                        'username' => $findUser->getUsername(),
                        'name' => $findUser->getName(),
                        'email' => $findUser->getEmail(),
                        'roles' => $roles,
                        'designation' => $findUser->getDesignation() ? $findUser->getDesignation()->getName() : '',
                        'lineManager' => $findUser->getLineManager() ? $findUser->getLineManager()->getId() : '',
                        'locations' => $locations,
                        'upozilas' => $upozilas,
                        'status' => '200',
                        'otp' => $otp,
                    ];
                    $response = new Response();
                    $response->headers->set('Content-Type', 'application/json');
                    $response->setContent(json_encode($data));
                    $response->setStatusCode(Response::HTTP_OK);
                    return $response;
                } else {
                    $response = new Response();
                    $response->headers->set('Content-Type', 'application/json');
                    $response->setContent(json_encode([
                        'message' => 'This employee is disabled!',
                        'status' => '401',
                    ]));
                    $response->setStatusCode(Response::HTTP_UNAUTHORIZED);
                    return $response;
                }
            } else {
                $response = new Response();
                $response->headers->set('Content-Type', 'application/json');
                $response->setContent(json_encode([
                    'message' => 'Unregistered employee!',
                    'status' => '401',
                ]));
                $response->setStatusCode(Response::HTTP_UNAUTHORIZED);
                return $response;
            }
        }

        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent(json_encode([
            'message' => 'Invalid request!',
            'status' => '405',
        ]));
        $response->setStatusCode(Response::HTTP_UNAUTHORIZED);
        return $response;
    }

    public function checkDuplicateUserAction(Request $request)
    {
        $username = $request->request->get('username');
        $user = $this->getDoctrine()->getManager()->getRepository("UserBundle:User")
            ->checkExistingUser($username);
        if ($user == false) {
            $data = array(
                'status' => 'valid'
            );
        } else {
            $data = array(
                'status' => 'invalid'
            );
        }
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent(json_encode($data));
        $response->setStatusCode(Response::HTTP_OK);
        return $response;


    }

    public function editUserAction(Request $request)
    {
        $username = $request->request->get('user_id');
        $user = $this->getDoctrine()->getManager()->getRepository("UserBundle:User")
            ->findOneBy(array('id' => $username));
        $data = array(
            'user_id' => $username,
            'username' => $user->getUsername(),
            'name' => $user->getName(),
            'role' => $user->getAndroidRole()
        );
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent(json_encode($data));
        $response->setStatusCode(Response::HTTP_OK);
        return $response;
    }

    public function updateUserAction(Request $request)
    {

        $formData = $request->request->all();
        $username = $formData['user_id'];
        $userExist = $this->getDoctrine()->getRepository('UserBundle:User')->find($username);
        if ($this->checkApiValidation($request) == 'invalid') {

            return new Response('Unauthorized access.', 401);

        } elseif ($userExist) {

            $this->getDoctrine()->getRepository('UserBundle:User')->androidUserUpdate($userExist, $formData);
            $data = array('status' => 'success');
            $response = new Response();
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent(json_encode($data));
            $response->setStatusCode(Response::HTTP_OK);
            return $response;

        } else {

            return new Response('Unauthorized access.', 401);
        }
    }

    public function forgetPasswordAction(Request $request)
    {

        $username = $request->request->get('username');
        $user = $this->getDoctrine()->getManager()->getRepository("UserBundle:User")
            ->checkLoginUser($username);
        if (empty($user)) {
            return new Response('Unauthorized access.', 401);
        } else {
            $data = array(
                'licenseKey' => $user->getGlobalOption()->getUniqueCode(),
                'username' => $user->getUsername(),
                'name' => $user->getName(),
                'status' => 'success'
            );
            $response = new Response();
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent(json_encode($data));
            $response->setStatusCode(Response::HTTP_OK);
            return $response;
        }

    }

    public function resetPasswordAction(Request $request)
    {

        if ($this->checkApiValidation($request) == 'invalid') {

            return new Response('Unauthorized access.', 401);

        } else {
            $entity = $this->checkApiValidation($request);
            $username = $request->request->get('username');
            $password = $request->request->get('password');
            $user = $this->getDoctrine()->getManager()->getRepository("UserBundle:User")
                ->findOneBy(array('username' => $username, 'enabled' => 1));
            if (empty($user)) {
                return new Response('Unauthorized access.', 401);
            }

            $user->setPlainPassword($password);
            $this->get('fos_user.user_manager')->updateUser($user);
            $data = array(
                'username' => $user->getUsername(),
                'name' => $user->getProfile()->getName(),
                'status' => 'success'
            );
            $response = new Response();
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent(json_encode($data));
            $response->setStatusCode(Response::HTTP_OK);
            return $response;
        }
    }


    /**
     * @Route("/initial-master-data-import-api", name="initial_master_data_import_api" , methods={"POST","GET"}, options={"expose"=true})
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @return JsonResponse|Response
     */
    public function initialMasterDataImportApi(Request $request, ParameterBagInterface $parameterBag)
    {
        set_time_limit(0);
        ignore_user_abort(true);

        if ($request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {
            $locations = isset($_REQUEST['locations']) ? $_REQUEST['locations'] : "";
            //$terminal = $this->getUser()->getTerminal()->getId();
            $entities = [];

            $agent = $this->getDoctrine()->getRepository(Api::class)->apiAgent(1, $locations);

            $entities['agent'] = $agent;

            $customer = $this->getDoctrine()->getRepository(Api::class)->customerApi(1, 'farmer', $locations);

            $entities['customer'] = $customer;

            $username = isset($_REQUEST['username']) ? $_REQUEST['username'] : "";
            //$terminal = $this->getUser()->getTerminal()->getId();
//            $entities['employee'] = $this->getDoctrine()->getRepository(Api::class)->employeeApi(1, $username);
            $entities['employee'] = [];

            $entities['md_layer_standard'] = $this->getDoctrine()->getRepository(Api::class)->apiLayer(1);

            $entities['md_sonali_standard'] = $this->getDoctrine()->getRepository(Api::class)->apiSonali(1);

            $entities['md_broiler_standard'] = $this->getDoctrine()->getRepository(Api::class)->apiBroiler(1);

            $entities['md_setting_lifecycle'] = $this->getDoctrine()->getRepository(Api::class)->apiLifeCycleSetting(1);

            $entities['agent_location'] = $agent;

            $entities['customer_location'] = $customer;

            $userId = isset($_REQUEST['user_id']) && $_REQUEST['user_id'] != '' ? $_REQUEST['user_id'] : "";

            $entities['crm_setting'] = $this->getDoctrine()->getRepository(Api::class)->allCrmSetting();

            $entities['visit_area'] = $this->getDoctrine()->getRepository(Api::class)->usercrmvisitingarea($userId);

            $entities['purpose'] = $this->getDoctrine()->getRepository(Api::class)->dailyActiviesPurpose(1);

            $entities['purpose_sales_and_marketing'] = $this->getDoctrine()->getRepository(Api::class)->dailyActivitiesPurposeForSalesAndMarketing();

            $entities['agent_report_type'] = $this->getDoctrine()->getRepository(Api::class)->agentPurposeForReport();

            $employee = $this->getDoctrine()->getRepository(User::class)->find($userId);

            $expenseChartArray = [];
            $crm_expense_chart = $this->employeeExpenseChart($employee ? $employee->getId() : '');
            $crm_expense_chart_detail = $crm_expense_chart && isset($crm_expense_chart['details']) ? $crm_expense_chart['details'] : [];
            unset($crm_expense_chart['details']);
            if ($crm_expense_chart && sizeof($crm_expense_chart) > 0) {
                $expenseChartArray[] = $crm_expense_chart;
            }
            $entities['crm_expense_chart'] = $expenseChartArray;
            $entities['crm_expense_chart_detail'] = $crm_expense_chart_detail;

            $entities['farmer'] = $this->getDoctrine()->getRepository(Api::class)->searchfarmer($employee);

            $entities['farmer_type'] = $this->getDoctrine()->getRepository(Api::class)->newfarmertype(1);

            $entities['farm_type'] = $this->getDoctrine()->getRepository(Api::class)->selectFarmType();

            $entities['farm_type_marketing'] = $this->getDoctrine()->getRepository(Api::class)->selectFarmTypeForMarketing();

            $entities['farm_report_type'] = $this->getDoctrine()->getRepository(Api::class)->farmSelectReport(1);

            $entities['farm_report_type_marketing'] = $this->getDoctrine()->getRepository(Api::class)->farmSelectReportForMarketing();

            $entities['report_age_week'] = $this->getDoctrine()->getRepository(Api::class)->ageweek(1);

            $entities['report_breed_type'] = $this->getDoctrine()->getRepository(Api::class)->breedType(1);

            $entities['report_color'] = $this->getDoctrine()->getRepository(Api::class)->color(1);

            $entities['report_feed_mill'] = $this->getDoctrine()->getRepository(Api::class)->feedMill(1);

            $entities['report_feed_type'] = $this->getDoctrine()->getRepository(Api::class)->feedType(1);

            $entities['report_product_name_type'] = $this->getDoctrine()->getRepository(Api::class)->productName();

            $entities['feed'] = $this->getDoctrine()->getRepository(Api::class)->feed(1);

            $entities['report_hatchery'] = $this->getDoctrine()->getRepository(Api::class)->hatchery(1);

            $entities['disease'] = $this->getDoctrine()->getRepository(Api::class)->disease(1);

            $entities['product'] = $this->getDoctrine()->getRepository(Api::class)->product(1);

            $entities['product_for_chick'] = $this->getDoctrine()->getRepository(Api::class)->productForBoilerChick();

            $entities['product_for_layer_chick'] = $this->getDoctrine()->getRepository(Api::class)->productForLayerChick();

            $entities['species'] = $this->getDoctrine()->getRepository(Api::class)->mainculturespecies();

            $entities['crm_vehicle'] = $this->getDoctrine()->getRepository(Api::class)->vehicle(1);

            $entities['agent_purpose'] = $this->getDoctrine()->getRepository(Api::class)->agentPurpose();

            $locations = isset($_REQUEST['locations']) ? $_REQUEST['locations'] : "";

            $entities['sub_agent'] = $this->getDoctrine()->getRepository(Api::class)->agentApi(1, 'sub-agent', $locations);

            $entities['sub_agent_purpose'] = $this->getDoctrine()->getRepository(Api::class)->subAgentPurpose();

            $entities['other_agent'] = $this->getDoctrine()->getRepository(Api::class)->agentApi(1, 'other-agent', $locations);

            $entities['other_agent_purpose'] = $this->getDoctrine()->getRepository(Api::class)->otherAgentPurpose();

            $entities['crm_complain_doc'] = $this->getDoctrine()->getRepository(Api::class)->getComplainType('COMPLAIN_DOC');

            $entities['crm_complain_feed'] = $this->getDoctrine()->getRepository(Api::class)->getComplainType('COMPLAIN_FEED');

            $entities['crm_complain_hatchery'] = $this->getDoctrine()->getRepository(Api::class)->getNourishHatchery('HATCHERY_NOURISH');

            $entities['crm_complain_transport'] = $this->getDoctrine()->getRepository(Api::class)->getTransport('TRANSPORT');

            $entities['fish_feed_type'] = $this->getDoctrine()->getRepository(Api::class)->fishFeedType();

            $entities['fish_species'] = $this->getDoctrine()->getRepository(Api::class)->fishSpeciesName();

            $entities['main_culture_species'] = $this->getDoctrine()->getRepository(Api::class)->mainculturespecies();

            $entities['fish_sale_price'] = $this->getDoctrine()->getRepository(Api::class)->fishSalesPrice(1);

            $labs = [];
//            $employeeId = isset($_REQUEST['employee_id']) && $_REQUEST['employee_id']!='' ? $_REQUEST['employee_id'] : "";
            if ($employee) {
//                $findEmployee = $this->getDoctrine()->getRepository(User::class)->find($data['employee_id']);
                if ($employee && $employee->getLabs() != '') {
                    $labs = $this->getDoctrine()->getRepository(Api::class)->labName($employee->getLabs());
                }
            }
            $entities['lab'] = $labs;

            $entities['lab_service'] = $this->getDoctrine()->getRepository(Api::class)->labServiceName();

            $regions = $this->getDoctrine()->getRepository(Location::class)->findBy(['level' => 3]);
            $regionArraydata = [];
            foreach ($regions as $region) {
                $regionArraydata[] = [
                    'id' => $region->getId(),
                    'name' => $region->getName(),
                ];
            }

            $entities['region'] = $regionArraydata;

            $entities['chick_type'] = $this->getDoctrine()->getRepository(Setting::class)->getActiveSettingByType('CHICK_TYPE');

            $entities['poultry_meat_egg_type'] = $this->getDoctrine()->getRepository(Setting::class)->getActiveSettingByType('MEAT_EGG_TYPE');

            $entities['visit_working_mode'] = $this->getDoctrine()->getRepository(Setting::class)->getActiveSettingByType('WORKING_MODE');

            $entities['fcr_hatchery_company'] = $this->getDoctrine()->getRepository(Api::class)->fcrHatcheryCompany();

            $entities['breed'] = $this->getDoctrine()->getRepository(Setting::class)->getActiveSettingByType('BREED_NAME');

            $trainingMaterilArray = [];
            if ($employee) {
                if ($employee->getServiceMode() && $employee->getServiceMode()->getSlug() != 'sales-marketing') {
                    $serviceModeExplode = explode('-', $employee->getServiceMode()->getSlug());
                    $lastElement = end($serviceModeExplode);
                    $breedName = $this->getDoctrine()->getRepository(Setting::class)->findOneBy(['settingType' => 'BREED_NAME', 'slug' => $lastElement . '-breed', 'status' => 1]);
                    $materials = $this->getDoctrine()->getRepository(Setting::class)->findBy(['settingType' => 'TRAINING_MATERIAL', 'parent' => $breedName]);

                    foreach ($materials as $material) {
                        $trainingMaterilArray[] = [
                            'id' => $material->getId(),
                            'name' => $material->getName(),
                        ];
                    }
                }

            }
            $entities['training_material'] = $trainingMaterilArray;

            $entities['challenge_name_type'] = $this->getDoctrine()->getRepository(Api::class)->challengeName();

            $entities['fish_feed_type_for_life_cycle'] = $this->getDoctrine()->getRepository(Api::class)->fishFeedTypeForLifeCycle();

            $entities['area'] = $this->getDoctrine()->getRepository(Api::class)->area();

            $entities['fcr_different_feed_company_for_broiler'] = $this->getDoctrine()->getRepository(Api::class)->fcrDifferentFeedCompanyForBroiler();

            $entities['fcr_different_feed_company_for_sonali'] = $this->getDoctrine()->getRepository(Api::class)->fcrDifferentFeedCompanyForSonali();

            $type_of_vehicle = [];
            if ($employee->getExpenseChart() && $employee->getExpenseChart()->getTypeOfVehicle() && $employee->getExpenseChart()->getTypeOfVehicle() == 'car') {
                $type_of_vehicle =
                    [
                        [
                            'id' => 1,
                            'slug' => 'office-car',
                            'name' => 'Office Car',
                        ],
                        [
                            'id' => 2,
                            'slug' => 'car',
                            'name' => 'Car',
                        ],
                        [
                            'id' => 3,
                            'slug' => 'local-conveyance',
                            'name' => 'Local Conveyance',
                        ]
                    ];
            } elseif ($employee->getExpenseChart() && $employee->getExpenseChart()->getTypeOfVehicle() && $employee->getExpenseChart()->getTypeOfVehicle() == 'motorcycle') {
                $type_of_vehicle =
                    [
                        [
                            'id' => 1,
                            'slug' => 'office-car',
                            'name' => 'Office Car',
                        ],
                        [
                            'id' => 2,
                            'slug' => 'motorcycle',
                            'name' => 'Motorcycle',
                        ],
                        [
                            'id' => 3,
                            'slug' => 'local-conveyance',
                            'name' => 'Local Conveyance',
                        ]
                    ];
            } else {
                $type_of_vehicle =
                    [
                        [
                            'id' => 1,
                            'slug' => 'office-car',
                            'name' => 'Office Car',
                        ],
                        [
                            'id' => 2,
                            'slug' => 'local-conveyance',
                            'name' => 'Local Conveyance',
                        ]
                    ];
            }

            $entities['type_of_vehicle'] = $type_of_vehicle;


            $arrayData = [];

            if ($userId) {
                $newDate = date('Y-F', strtotime('-1 month'));
                $explodeDate = explode('-', $newDate);
                $year = $explodeDate[0];
                $month = $explodeDate[1];
                $arrayData = $this->getDoctrine()->getRepository(CompanyWiseFeedSale::class)->getCompanyWiseFeedSaleDataForApiImport($userId, $year, $month);
            }

            $entities['crm_company_wise_feed_sale'] = $arrayData;

            $appVersion = $this->getDoctrine()->getRepository(AppVersions::class)->getActiveAppVersion();
            $entities['app_version'] = $appVersion;


            $response = new Response();
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent(json_encode($entities));
            $response->setStatusCode(Response::HTTP_OK);
            return $response;
        }
        return new JsonResponse([
            'status' => 404,
            'message' => 'Not Found!'
        ]);
    }

    /**
     * @Route("/farmer-data-import-api", name="farmer_data_import_api" , methods={"GET"}, options={"expose"=true})
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @return JsonResponse|Response
     */
    public function farmerDataImportApi(Request $request, ParameterBagInterface $parameterBag)
    {
        set_time_limit(0);
        ignore_user_abort(true);

        if ($request->getMethod() == 'GET' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {
            $locations = isset($_REQUEST['locations']) ? $_REQUEST['locations'] : "";
            //$terminal = $this->getUser()->getTerminal()->getId();
            $entities = [];

            $customer = $this->getDoctrine()->getRepository(Api::class)->customerApi(1, 'farmer', $locations);

            $entities['customer'] = $customer;

            $entities['customer_location'] = $customer;

            $userId = isset($_REQUEST['user_id']) && $_REQUEST['user_id'] != '' ? $_REQUEST['user_id'] : "";

            $employee = $this->getDoctrine()->getRepository(User::class)->find($userId);

            $entities['farmer'] = $this->getDoctrine()->getRepository(Api::class)->searchfarmer($employee);

            $response = new Response();
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent(json_encode($entities));
            $response->setStatusCode(Response::HTTP_OK);
            return $response;
        }
        return new JsonResponse([
            'status' => 404,
            'message' => 'Not Found!'
        ]);
    }


    /**
     * @Route("/life-cycle-data-import-api", name="initial_life_cycle_data_import_api" , methods={"POST","GET"}, options={"expose"=true})
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @return JsonResponse|Response
     */
    public function lifeCycleDataImportApi(Request $request, ParameterBagInterface $parameterBag)
    {
        set_time_limit(0);
        ignore_user_abort(true);

        if ($request->getMethod() == 'GET' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {

            $entities = [];
            $employee_id = $request->query->get('employee_id') && $request->query->get('employee_id') != '' ? $request->query->get('employee_id') : "";

            if ($employee_id && $employee_id != '') {
                $employee = $this->getDoctrine()->getRepository(User::class)->find($employee_id);

                if ($employee) {

                    if ($employee->getServiceMode()) {
                        if ($employee->getServiceMode()->getSlug() == 'sales-service-poultry') {

                            $entities['crm_broiler_life_cycle'] = $this->getChickLifeCycleInprogressData($employee_id);

                            $entities['crm_broiler_life_cycle_details'] = $this->getChickLifeCycleDetailsInprogressData($employee_id);

                            $entities['crm_layer_life_cycle'] = $this->getLayerLifeCycleInprogressData($employee_id);

                            $entities['crm_layer_life_cycle_details'] = $this->getLayerLifeCycleDetailsInprogressData($employee_id);


                        } elseif ($employee->getServiceMode()->getSlug() == 'sales-service-cattle') {

                            $entities['crm_cattle_life_cycle'] = $this->getCattleLifeCycleInprogressData($employee_id);

                            $entities['crm_cattle_life_cycle_details'] = $this->getCattleLifeCycleDetailsInprogressData($employee_id);

                        } elseif ($employee->getServiceMode()->getSlug() == 'sales-service-fish') {

                            $entities['crm_fish_life_cycle_culture'] = $this->getFishLifeCycleDataByEmployeeId($employee_id);

                            $entities['crm_fish_life_cycle_nursing'] = $this->getFishLifeCycleNursingDataByEmployeeId($employee_id);

                        } else {
                            $entities['crm_broiler_life_cycle'] = [];
                            $entities['crm_broiler_life_cycle_details'] = [];
                            $entities['crm_layer_life_cycle'] = [];
                            $entities['crm_layer_life_cycle_details'] = [];

                            $entities['crm_cattle_life_cycle'] = [];
                            $entities['crm_cattle_life_cycle_details'] = [];

                            $entities['crm_fish_life_cycle_culture'] = [];
                        }
                    } else {
                        $entities['crm_broiler_life_cycle'] = [];
                        $entities['crm_broiler_life_cycle_details'] = [];
                        $entities['crm_layer_life_cycle'] = [];
                        $entities['crm_layer_life_cycle_details'] = [];

                        $entities['crm_cattle_life_cycle'] = [];
                        $entities['crm_cattle_life_cycle_details'] = [];

                        $entities['crm_fish_life_cycle_culture'] = [];
                    }
                }

            }


            $response = new Response();
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent(json_encode($entities));
            $response->setStatusCode(Response::HTTP_OK);
            return $response;
        }
        return new JsonResponse([
            'status' => 404,
            'message' => 'Not Found!'
        ]);
    }


    /**
     * @Route("/agent", name="crm_api_agent" , methods={"POST","GET"}, options={"expose"=true})
     */
    public function apiAgent()
    {
        set_time_limit(0);
        ignore_user_abort(true);
        $locations = isset($_REQUEST['locations']) ? $_REQUEST['locations'] : "";
        //$terminal = $this->getUser()->getTerminal()->getId();
        $entities = $this->getDoctrine()->getRepository(Api::class)->apiAgent(1, $locations);

        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent(json_encode($entities));
        $response->setStatusCode(Response::HTTP_OK);
        return $response;
    }

    /**
     * @Route("/nourish-agent", name="crm_api_rourish_agent" , methods={"POST","GET"}, options={"expose"=true})
     */
    public function apiNourishAgent()
    {
        set_time_limit(0);
        ignore_user_abort(true);
        $locations = isset($_REQUEST['locations']) ? $_REQUEST['locations'] : "";
        //$terminal = $this->getUser()->getTerminal()->getId();
        $entities = $this->getDoctrine()->getRepository(Api::class)->apiOnlyNourishAgent(1, $locations);

        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent(json_encode($entities));
        $response->setStatusCode(Response::HTTP_OK);
        return $response;
    }

    /**
     * @Route("/customer", name="customerApi")
     */
    public function customerApi()
    {
        set_time_limit(0);
        ignore_user_abort(true);
        //$terminal = $this->getUser()->getTerminal()->getId();
        $locations = isset($_REQUEST['locations']) ? $_REQUEST['locations'] : "";
        $entities = $this->getDoctrine()->getRepository(Api::class)->customerApi(1, 'farmer', $locations);
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent(json_encode($entities));
        $response->setStatusCode(Response::HTTP_OK);
        return $response;
    }

    /**
     * @Route("/sub-agent", name="sub_agent_api")
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @return JsonResponse|Response
     */
    public function subAgentApi(Request $request, ParameterBagInterface $parameterBag)
    {
        set_time_limit(0);
        ignore_user_abort(true);
        if ($request->getMethod() == 'GET' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {
            $locations = $request->query->get('locations');
            $entities = $this->getDoctrine()->getRepository(Api::class)->agentApi(1, 'sub-agent', $locations);
            $response = new Response();
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent(json_encode($entities));
            $response->setStatusCode(Response::HTTP_OK);
            return $response;
        }
        return new JsonResponse([
            'status' => 404,
            'message' => 'Not Found!'
        ]);

    }

    /**
     * @Route("/other-agent", name="other_agent_api")
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @return JsonResponse|Response
     */
    public function otherAgent(Request $request, ParameterBagInterface $parameterBag)
    {
        set_time_limit(0);
        ignore_user_abort(true);
        if ($request->getMethod() == 'GET' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {
            //$terminal = $this->getUser()->getTerminal()->getId();
            $locations = $request->query->get('locations');
            $entities = $this->getDoctrine()->getRepository(Api::class)->agentApi(1, 'other-agent', $locations);
            $response = new Response();
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent(json_encode($entities));
            $response->setStatusCode(Response::HTTP_OK);
            return $response;
        }
        return new JsonResponse([
            'status' => 404,
            'message' => 'Not Found!'
        ]);
    }

    /**
     * @Route("/crmvisit", methods={"POST"}, name="crmvisit")
     */
    public function crmVisit(Request $request)
    {
        set_time_limit(0);
        ignore_user_abort(true);
        $username = isset($_REQUEST['username']) ? $_REQUEST['username'] : "";
        //$terminal = $this->getUser()->getTerminal()->getId();
        $entities = $this->getDoctrine()->getRepository(Api::class)->crmVisit(1, $username);

        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent(json_encode($entities));
        $response->setStatusCode(Response::HTTP_OK);
        return $response;
    }

    /**
     * @Route("/employee", methods={"POST"}, name="employeeApi")
     * @param Request $request
     * @return Response
     */
    public function employeeApi(Request $request)
    {
        set_time_limit(0);
        ignore_user_abort(true);
        $username = isset($_REQUEST['username']) ? $_REQUEST['username'] : "";
        //$terminal = $this->getUser()->getTerminal()->getId();
        $entities = $this->getDoctrine()->getRepository(Api::class)->employeeApi(1, $username);

        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent(json_encode($entities));
        $response->setStatusCode(Response::HTTP_OK);
        return $response;
    }

    /**
     * @Route("/broiler/standard", name="crm_api_brolier")
     */
    public function apiBroiler()
    {
        set_time_limit(0);
        ignore_user_abort(true);
        //$terminal = $this->getUser()->getTerminal()->getId();
        $entities = $this->getDoctrine()->getRepository(Api::class)->apiBroiler(1);

        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent(json_encode($entities));
        $response->setStatusCode(Response::HTTP_OK);
        return $response;
    }

    /**
     * @Route("/sonali/standard", name="crm_api_Sonali")
     */
    public function apiSonali()
    {
        set_time_limit(0);
        ignore_user_abort(true);
        //$terminal = $this->getUser()->getTerminal()->getId();
        $entities = $this->getDoctrine()->getRepository(Api::class)->apiSonali(1);

        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent(json_encode($entities));
        $response->setStatusCode(Response::HTTP_OK);
        return $response;
    }

    /**
     * @Route("/setting/life-cycle", name="crm_api_apiLifeCycleSetting")
     */
    public function apiLifeCycleSetting()
    {
        set_time_limit(0);
        ignore_user_abort(true);
        //$terminal = $this->getUser()->getTerminal()->getId();
        $entities = $this->getDoctrine()->getRepository(Api::class)->apiLifeCycleSetting(1);

        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent(json_encode($entities));
        $response->setStatusCode(Response::HTTP_OK);
        return $response;
    }

    /**
     * @Route("/setting", name="apiSetting")
     */
    public function apiSetting()
    {
        set_time_limit(0);
        ignore_user_abort(true);
        //$terminal = $this->getUser()->getTerminal()->getId();
        $entities = $this->getDoctrine()->getRepository(Api::class)->apiSetting(1);

        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent(json_encode($entities));
        $response->setStatusCode(Response::HTTP_OK);
        return $response;
    }

    /**
     * @Route("/layer/standard", name="apiLayer")
     */
    public function apiLayer()
    {
        set_time_limit(0);
        ignore_user_abort(true);
        //$terminal = $this->getUser()->getTerminal()->getId();
        $entities = $this->getDoctrine()->getRepository(Api::class)->apiLayer(1);

        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent(json_encode($entities));
        $response->setStatusCode(Response::HTTP_OK);
        return $response;
    }


    /**
     * @Route("/report/farmer-introduce-report", methods={"POST"}, name="farmerintroducereport")
     * @param Request $request
     * @return Response
     */
    public function farmerIntroduceReport(Request $request)
    {
        $breedName = $request->request->get('breed_name');
        $employeeName = $request->request->get('employee');

        $employee = $this->getDoctrine()->getRepository(User::class)->findOneBy(array('name' => $employeeName));
        //$agentName = $request->request->get('agent_name');

        set_time_limit(0);
        ignore_user_abort(true);
        // $terminal = $this->getUser()->getTerminal()->getId();
        $entities = $this->getDoctrine()->getRepository(Api::class)->farmerIntroduceReport(1, $breedName, $employee);

        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent(json_encode($entities));
        $response->setStatusCode(Response::HTTP_OK);
        return $response;
    }

    /**
     * @Route("/report/farmer-touch-report", methods={"POST"}, name="farmertouchreport")
     * @param Request $request
     * @return Response
     */
    public function farmerTouchReport(Request $request)
    {
        $startDate = $request->request->get('startDate');
        $endDate = $request->request->get('endDate');
        $reportSlug = $request->request->get('report');
        $employeeName = $request->request->get('employee');

        $report = $this->getDoctrine()->getRepository(Setting::class)->findOneBy(array('slug' => $reportSlug));

        $employee = $this->getDoctrine()->getRepository(User::class)->findOneBy(array('name' => $employeeName));

        set_time_limit(0);
        ignore_user_abort(true);
        $entities = $this->getDoctrine()->getRepository(Api::class)->farmerTouchReport(1, $startDate, $endDate, $report, $employee);

        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent(json_encode($entities));
        $response->setStatusCode(Response::HTTP_OK);
        return $response;
    }

    /**
     * @Route("/report/farmer-training-report", methods={"POST"}, name="farmer-training-report")
     * @param Request $request
     * @return Response
     */
    public function farmerTrainingReport(Request $request)
    {
        $breedName = $request->request->get('breed_name');
        $employeeName = $request->request->get('employee');

        $employee = $this->getDoctrine()->getRepository(User::class)->findOneBy(array('name' => $employeeName));
        //$agentName = $request->request->get('agent_name');
        set_time_limit(0);
        ignore_user_abort(true);
        // $terminal = $this->getUser()->getTerminal()->getId();
        $entities = $this->getDoctrine()->getRepository(Api::class)->farmerTrainingReport(1, $breedName, $employee);

        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent(json_encode($entities));
        $response->setStatusCode(Response::HTTP_OK);
        return $response;
    }


    /**
     * @Route("/report/poultry", methods={"POST"}, name="reportPoultry")
     * @param Request $request
     * @return Response
     */
    public function poultryLifeCylceReport(Request $request)
    {
        set_time_limit(0);
        ignore_user_abort(true);
        $startDate = $request->request->get('startDate');
        $endDate = $request->request->get('endDate');
        $reportSlug = $request->request->get('report');
        $customerName = $request->request->get('customer');

        $report = $this->getDoctrine()->getRepository(Setting::class)->findOneBy(array('slug' => $reportSlug));

        $customer = $this->getDoctrine()->getRepository(CrmCustomer::class)->findOneBy(array('name' => $customerName));


        //$terminal = $this->getUser()->getTerminal()->getId();
        $entities = $this->getDoctrine()->getRepository(Api::class)->poultryLifeCylceReport(1, $startDate, $endDate, $reportSlug, $customerName);
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent(json_encode($entities));
        $response->setStatusCode(Response::HTTP_OK);
        return $response;
    }

    /**
     * @Route("/cattle/crm-visit", methods={"POST"}, name="cattle-crm-visit")
     * @param Request $request
     * @return Response
     */
    public function farmCattleVisit(Request $request)
    {
        $startDate = $request->request->get('startDate');
        $endDate = $request->request->get('endDate');
        $employeeName = $request->request->get('employee');

        set_time_limit(0);
        ignore_user_abort(true);

        $employee = $this->getDoctrine()->getRepository(User::class)->findOneBy(array('name' => $employeeName));

        $entities = $this->getDoctrine()->getRepository(Api::class)->farmVisitCattle(1, $startDate, $endDate, $employee);

        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent(json_encode($entities));
        $response->setStatusCode(Response::HTTP_OK);
        return $response;
    }

    /**
     * @Route("/chick/fcr", methods={"POST"}, name="chickfcr")
     * @param Request $request
     * @return Response
     */
    public function frcReportPoulty(Request $request)
    {
        set_time_limit(0);
        ignore_user_abort(true);

        $startDate = $request->request->get('startDate');
        $endDate = $request->request->get('endDate');
        $reportSlug = $request->request->get('report');
        $employeeName = $request->request->get('employee');

        $report = $this->getDoctrine()->getRepository(Setting::class)->findOneBy(array('slug' => $reportSlug));

        $employee = $this->getDoctrine()->getRepository(User::class)->findOneBy(array('name' => $employeeName));

        $entities = $this->getDoctrine()->getRepository(Api::class)->frcReportPoulty(1, $startDate, $endDate, $reportSlug, $employeeName);

        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent(json_encode($entities));
        $response->setStatusCode(Response::HTTP_OK);
        return $response;
    }

    /**
     * @Route("/fcr/before/{type}/new", methods={"GET", "POST"}, name="api_fcr_before_new", options={"expose"=true})
     * @param Request $request
     * @param $type
     * @return Response
     * @throws Exception
     */
    // type = sonali or boiler
    public function apiFcrBeforeNew(Request $request, $type): Response
    {
        $data = $request->request->all();
        $slug = 'fcr-before-sale-' . $type;
        $report = $this->getDoctrine()->getRepository(Setting::class)->findOneBy(['slug' => $slug, 'settingType' => 'FARMER_REPORT', 'status' => 1]);

        $hatchery = $this->getDoctrine()->getRepository(Setting::class)->findOneBy(['id' => $data['hatchery_id'], 'status' => 1]);

        $breed = $this->getDoctrine()->getRepository(Setting::class)->findOneBy(['id' => $data['breed_id'], 'status' => 1]);

        $feed = $this->getDoctrine()->getRepository(Setting::class)->findOneBy(['id' => $data['feed_id'], 'status' => 1]);

        $feedMill = $this->getDoctrine()->getRepository(Setting::class)->findOneBy(['id' => $data['feed_Mill'], 'status' => 1]);

        $feedType = $this->getDoctrine()->getRepository(Setting::class)->findOneBy(['id' => $data['feedType'], 'status' => 1]);

        $employee = $this->getDoctrine()->getRepository(User::class)->find($data['user_id']);

        $customer = $this->getDoctrine()->getRepository(CrmCustomer::class)->find($data['customer_id']);

        $entity = new FcrDetails();
        $reportingDate = date('Y-m-d', strtotime('now'));
        $hatchingDate = date('Y-m-d', strtotime('now'));
        $proDate = date('Y-m-d', strtotime('now'));
        $entity->setReportingMonth(new \DateTime($reportingDate));
        $entity->setHatchingDate(new \DateTime($hatchingDate));
        $entity->setProDate(new \DateTime($proDate));
        $entity->setFcrOfFeed(strtoupper('before'));
        $entity->setCustomer($customer);
        $entity->setReport($report);
        $entity->setAgent($customer->getAgent());
        $entity->setEmployee($employee);
        $entity->setTotalBirds($data['totalBirds']);
        $entity->setAgeDay($data['age']);
        $entity->setMortalityPes($data['mortality_pcs']);
        $entity->setHatchery($hatchery ? $hatchery : null);
        $entity->setBreed($breed ? $breed : null);
        $entity->setFeed($feed ? $feed : null);
        $entity->setFeedMill($feedMill ? $feedMill : null);
        $entity->setFeedType($feedType ? $feedType : null);
        $entity->setBatchNo($data['batch_no']);
        $entity->setRemarks($data['remarks']);

        if ($report->getSlug() == 'fcr-before-sale-sonali') {

            /* @var SonaliStandard $sonaliStandard */
            $sonaliStandard = $this->getDoctrine()->getRepository(SonaliStandard::class)->findOneBy(array('age' => $entity->getAgeDay()));
            if ($sonaliStandard) {
                $entity->setWeightStandard($sonaliStandard->getTargetBodyWeight());
                $entity->setFeedConsumptionStandard($sonaliStandard->getCumulativeFeedIntake());
            }
        }
        if ($report->getSlug() == 'fcr-before-sale-boiler') {

            /* @var BroilerStandard $broilerStandard */
            $broilerStandard = $this->getDoctrine()->getRepository(BroilerStandard::class)->findOneBy(array('age' => $entity->getAgeDay()));
            if ($broilerStandard) {
                $entity->setWeightStandard($broilerStandard->getTargetBodyWeight());
                $entity->setFeedConsumptionStandard($broilerStandard->getTargetFeedConsumption());
            }
        }
        $entity->setMortalityPercent($entity->calculateMortalityPercent());
        $entity->setFeedConsumptionPerBird($entity->calculatePerBird());
        $entity->setFcrWithoutMortality($entity->calculateWithoutMortality());
        $entity->setFcrWithMortality($entity->calculateWithMortality());


        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();

        return new JsonResponse('success');
    }

    /**
     * @Route("/crmvisitingarea", methods={"GET","POST"}, name="crmVisitingArea")
     * @param Request $request
     * @return Response
     */
    public function crmVisitingArea(Request $request)
    {

        $terminal = 1;

        $formData = $_REQUEST;
        $userId = $request->request->get('user_id');

        $user = $this->getDoctrine()->getRepository(User::class)->find($userId);

        if (!empty($user)) {

            /* @var $user User */

            $upozilas = [];
            foreach ($user->getUpozila() as $location):
                $upozilas[] = $location->getId();
                $upozilaName[] = $location->getName();
            endforeach;
            $locations = implode(",", $upozilas);
            $data = array(
                'upozilas' => $upozilas,
                'upozilaName' => $upozilaName
            );
            $response = new Response();
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent(json_encode($data));
            $response->setStatusCode(Response::HTTP_OK);
            return $response;
        }
    }

    /**
     * @Route("/crmvisitnew", methods={"GET","POST"}, name="crmVisitNew")
     * @param Request $request
     * @return JsonResponse
     */
    public function new(Request $request)
    {
        $data = $request->request->all();

        $employee = $this->getDoctrine()->getRepository(User::class)->find($data['user_id']);

        $locations = $this->getDoctrine()->getRepository(Location::class)->find($data['id']);

        $entity = new CrmVisit();
        $entity->setEmployee($employee);
        $entity->setLocation($locations);
        $entity->setWorkingDuration($data['workingduration']);
        $entity->setWorkingDurationTo($data['workingdurationto']);
        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();

        return new JsonResponse('success');
    }

    /**
     * @Route("/dailyActiviesPurpose", methods={"GET"}, name="dailyActiviesPurpose")
     */
    public function dailyActiviesPurpose()
    {

        set_time_limit(0);
        ignore_user_abort(true);

        $entities = $this->getDoctrine()->getRepository(Api::class)->dailyActiviesPurpose(1);
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent(json_encode($entities));
        $response->setStatusCode(Response::HTTP_OK);
        return $response;
    }

    /**
     * @Route("/dailyActivitiesPurposeForSalesAndMarketing", methods={"GET"}, name="dailyActivitiesPurposeForSalesAndMarketing")
     */
    public function dailyActivitiesPurposeForSalesAndMarketing(Request $request, ParameterBagInterface $parameterBag)
    {

        set_time_limit(0);
        ignore_user_abort(true);
        if ($request->getMethod() == 'GET' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {
            $entities = $this->getDoctrine()->getRepository(Api::class)->dailyActivitiesPurposeForSalesAndMarketing();
            $response = new Response();
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent(json_encode($entities));
            $response->setStatusCode(Response::HTTP_OK);
            return $response;
        }
        return new JsonResponse([
            'status' => 404,
            'message' => 'Not Found!'
        ]);
    }

    /**
     * @Route("/vehicle", methods={"GET"}, name="vehicle")
     */
    public function vehicle()
    {

        set_time_limit(0);
        ignore_user_abort(true);

        $entities = $this->getDoctrine()->getRepository(Api::class)->vehicle(1);
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent(json_encode($entities));
        $response->setStatusCode(Response::HTTP_OK);
        return $response;
    }

    /**
     * @Route("/farmerSelectPurpose", methods={"GET","POST"}, name="farmerSelectPurpose")
     * @param Request $request
     * @return Response
     */
    public function farmerSelectPurpose(Request $request)
    {
        set_time_limit(0);
        ignore_user_abort(true);

        $employeeId = $request->request->get('user_id');

        $employee = $this->getDoctrine()->getRepository(User::class)->findOneBy(array('id' => $employeeId));

        $entities = $this->getDoctrine()->getRepository(Api::class)->farmerSelectPurpose(1, $employee);

        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent(json_encode($entities));
        $response->setStatusCode(Response::HTTP_OK);
        return $response;
    }

    /**
     * @Route("/selectFarmType", methods={"GET"}, name="selectFarmType")
     */
    public function selectFarmType()
    {

        set_time_limit(0);
        ignore_user_abort(true);

        $entities = $this->getDoctrine()->getRepository(Api::class)->selectFarmType();
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent(json_encode($entities));
        $response->setStatusCode(Response::HTTP_OK);
        return $response;
    }


    /**
     * @Route("/farmSelectReport", methods={"GET"}, name="farmSelectReport")
     */
    public function farmSelectReport()
    {
        set_time_limit(0);
        ignore_user_abort(true);

        $entities = $this->getDoctrine()->getRepository(Api::class)->farmSelectReport(1);

        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent(json_encode($entities));
        $response->setStatusCode(Response::HTTP_OK);
        return $response;
    }

    /**
     * @Route("/searchfarmer", methods={"POST"}, name="searchfarmer")
     */
    public function searchfarmer(Request $request)
    {
        set_time_limit(0);
        ignore_user_abort(true);

        $data = $request->request->all();

        $employee = $this->getDoctrine()->getRepository(User::class)->find($data['user_id']);

        $entities = $this->getDoctrine()->getRepository(Api::class)->searchfarmer($employee);

        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent(json_encode($entities));
        $response->setStatusCode(Response::HTTP_OK);
        return $response;

    }

    /**
     * @Route("/newfarmertype", methods={"GET"}, name="newfarmertype")
     */
    public function newFarmType()
    {

        set_time_limit(0);
        ignore_user_abort(true);

        $entities = $this->getDoctrine()->getRepository(Api::class)->newfarmertype(1);
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent(json_encode($entities));
        $response->setStatusCode(Response::HTTP_OK);
        return $response;
    }

    /**
     * @Route("/ageweek", methods={"GET"}, name="ageweek")
     */
    public function ageweek()
    {

        set_time_limit(0);
        ignore_user_abort(true);

        $entities = $this->getDoctrine()->getRepository(Api::class)->ageweek(1);
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent(json_encode($entities));
        $response->setStatusCode(Response::HTTP_OK);
        return $response;
    }

    /**
     * @Route("/feedType", methods={"GET"}, name="feedType")
     */
    public function feedType()
    {

        set_time_limit(0);
        ignore_user_abort(true);

        $entities = $this->getDoctrine()->getRepository(Api::class)->feedType(1);
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent(json_encode($entities));
        $response->setStatusCode(Response::HTTP_OK);
        return $response;
    }

    /**
     * @Route("/fishFeedType", methods={"GET"}, name="fishFeedType")
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @return Response
     */
    public function fishFeedType(Request $request, ParameterBagInterface $parameterBag)
    {
        set_time_limit(0);
        ignore_user_abort(true);
        if ($request->getMethod() == 'GET' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {
            $entities = $this->getDoctrine()->getRepository(Api::class)->fishFeedType();
            $response = new Response();
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent(json_encode($entities));
            $response->setStatusCode(Response::HTTP_OK);
            return $response;
        }
        return new JsonResponse([
            'status' => 404,
            'message' => 'Not Found!'
        ]);
    }

    /**
     * @Route("/fishFeedTypeForLifeCycle", methods={"GET"}, name="fishFeedTypeForLifeCycle")
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @return Response
     */
    public function fishFeedTypeForLifeCycle(Request $request, ParameterBagInterface $parameterBag)
    {
        set_time_limit(0);
        ignore_user_abort(true);
        if ($request->getMethod() == 'GET' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {
            $entities = $this->getDoctrine()->getRepository(Api::class)->fishFeedTypeForLifeCycle();
            $response = new Response();
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent(json_encode($entities));
            $response->setStatusCode(Response::HTTP_OK);
            return $response;
        }
        return new JsonResponse([
            'status' => 404,
            'message' => 'Not Found!'
        ]);
    }

    /**
     * @Route("/fishSpeciesName", methods={"GET"}, name="fishSpeciesName")
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @return JsonResponse|Response
     */
    public function fishSpeciesName(Request $request, ParameterBagInterface $parameterBag)
    {

        set_time_limit(0);
        ignore_user_abort(true);
        if ($request->getMethod() == 'GET' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {
            $entities = $this->getDoctrine()->getRepository(Api::class)->fishSpeciesName();
            $response = new Response();
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent(json_encode($entities));
            $response->setStatusCode(Response::HTTP_OK);
            return $response;
        }
        return new JsonResponse([
            'status' => 404,
            'message' => 'Not Found!'
        ]);


    }


    /**
     * @Route("/hatchery", methods={"GET"}, name="hatchery")
     */
    public function hatchery()
    {

        set_time_limit(0);
        ignore_user_abort(true);

        $entities = $this->getDoctrine()->getRepository(Api::class)->hatchery(1);
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent(json_encode($entities));
        $response->setStatusCode(Response::HTTP_OK);
        return $response;
    }

    /**
     * @Route("/feedMill", methods={"GET"}, name="feedMill")
     */
    public function feedMill()
    {

        set_time_limit(0);
        ignore_user_abort(true);

        $entities = $this->getDoctrine()->getRepository(Api::class)->feedMill(1);
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent(json_encode($entities));
        $response->setStatusCode(Response::HTTP_OK);
        return $response;
    }

    /**
     * @Route("/breedType", methods={"GET"}, name="breedType")
     */
    public function breedType()
    {

        set_time_limit(0);
        ignore_user_abort(true);

        $entities = $this->getDoctrine()->getRepository(Api::class)->breedType(1);
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent(json_encode($entities));
        $response->setStatusCode(Response::HTTP_OK);
        return $response;
    }

    /**
     * @Route("/color", methods={"GET"}, name="color")
     */
    public function color()
    {

        set_time_limit(0);
        ignore_user_abort(true);

        $entities = $this->getDoctrine()->getRepository(Api::class)->color(1);
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent(json_encode($entities));
        $response->setStatusCode(Response::HTTP_OK);
        return $response;
    }

    /**
     * @Route("/feed", methods={"GET"}, name="feed")
     */
    public function feed()
    {

        set_time_limit(0);
        ignore_user_abort(true);

        $entities = $this->getDoctrine()->getRepository(Api::class)->feed(1);
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent(json_encode($entities));
        $response->setStatusCode(Response::HTTP_OK);
        return $response;
    }

    /**
     * @Route("/disease", methods={"GET"}, name="disease")
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @return JsonResponse|Response
     */
    public function disease(Request $request, ParameterBagInterface $parameterBag)
    {

        set_time_limit(0);
        ignore_user_abort(true);
//        if ($request->getMethod() == 'GET' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')){
        if ($request->getMethod() == 'GET') {
            $entities = $this->getDoctrine()->getRepository(Api::class)->disease(1);
            $response = new Response();
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent(json_encode($entities));
            $response->setStatusCode(Response::HTTP_OK);
            return $response;
        }
        return new JsonResponse([
            'status' => 404,
            'message' => 'Not Found!'
        ]);


    }

    /**
     * @Route("/product", methods={"GET"}, name="product")
     */
    public function product()
    {

        set_time_limit(0);
        ignore_user_abort(true);

        $entities = $this->getDoctrine()->getRepository(Api::class)->product(1);
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent(json_encode($entities));
        $response->setStatusCode(Response::HTTP_OK);
        return $response;
    }

    /**
     * @Route("/product-for-boiler-chick", methods={"GET"}, name="product_for_boiler_chick")
     */
    public function productForBoilerChick(Request $request, ParameterBagInterface $parameterBag)
    {

        set_time_limit(0);
        ignore_user_abort(true);

        if ($request->getMethod() == 'GET' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {
            $entities = $this->getDoctrine()->getRepository(Api::class)->productForBoilerChick();
            $response = new Response();
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent(json_encode($entities));
            $response->setStatusCode(Response::HTTP_OK);
            return $response;
        }
        return new JsonResponse([
            'status' => 404,
            'message' => 'Not Found!'
        ]);
    }


    /**
     * @Route("/product-for-layer-chick", methods={"GET"}, name="product_for_layer_chick")
     */
    public function productForLayerChick(Request $request, ParameterBagInterface $parameterBag)
    {

        set_time_limit(0);
        ignore_user_abort(true);

        if ($request->getMethod() == 'GET' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {
            $entities = $this->getDoctrine()->getRepository(Api::class)->productForLayerChick();
            $response = new Response();
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent(json_encode($entities));
            $response->setStatusCode(Response::HTTP_OK);
            return $response;
        }
        return new JsonResponse([
            'status' => 404,
            'message' => 'Not Found!'
        ]);
    }

    /**
     * @Route("/mainculturespecies", methods={"GET"}, name="mainculturespecies")
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @return Response
     */
    public function mainCultureSpecies(Request $request, ParameterBagInterface $parameterBag)
    {

        set_time_limit(0);
        ignore_user_abort(true);
        if ($request->getMethod() == 'GET') {
            $entities = $this->getDoctrine()->getRepository(Api::class)->mainculturespecies();
            $response = new Response();
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent(json_encode($entities));
            $response->setStatusCode(Response::HTTP_OK);
            return $response;
        }
        return new JsonResponse([
            'status' => 404,
            'message' => 'Not Found!'
        ]);

    }

    /**
     * @Route("/usercrmvisitingarea", methods={"POST"}, name="usercrmvisitingarea")
     */
    public function usercrmvisitingarea(Request $request)
    {

        set_time_limit(0);
        ignore_user_abort(true);

        $employeeId = $request->request->get('user_id');

        $entities = $this->getDoctrine()->getRepository(Api::class)->usercrmvisitingarea($employeeId);
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent(json_encode($entities));
        $response->setStatusCode(Response::HTTP_OK);
        return $response;
    }

    /**
     * @Route("/dairyBreedType", methods={"GET"}, name="dairyBreedType")
     */
    public function dairyBreedType()
    {

        set_time_limit(0);
        ignore_user_abort(true);

        $entities = $this->getDoctrine()->getRepository(Api::class)->dairyBreedType(1);
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent(json_encode($entities));
        $response->setStatusCode(Response::HTTP_OK);
        return $response;
    }

    /**
     * @Route("/fatteningBreedType", methods={"GET"}, name="fatteningBreedType")
     */
    public function fatteningBreedType()
    {

        set_time_limit(0);
        ignore_user_abort(true);

        $entities = $this->getDoctrine()->getRepository(Api::class)->fatteningBreedType(1);
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent(json_encode($entities));
        $response->setStatusCode(Response::HTTP_OK);
        return $response;
    }


    /**
     * @Route("/store-json-data", methods={"POST"}, name="store_json_data")
     * @param Request $request
     * @return JsonResponse
     */
    public function storeAllJsonDataFromApi(Request $request)
    {
        set_time_limit(0);
        ignore_user_abort(true);

        $data = $request->request->all();

        if ($data) {
            if (!in_array($data['process'], ['crm_fish_life_cycle_detail_species'])) {
                $em = $this->getDoctrine()->getManager();
                $findEmployee = $this->getDoctrine()->getRepository(User::class)->find($data['employee_id']);
                $findParent = $this->getDoctrine()->getRepository(Api::class)->findOneBy(['batchNo' => $data['batch_id'], 'employee' => $findEmployee]);
                if (!$findParent) {
                    $api = new Api();
                    $api->setBatchNo($data['batch_id'] ?: null);
                    $api->setEmployee($findEmployee);
                    $api->setStatus(0);
                    $api->setCreatedAt(new \DateTime('now'));

                    $apiDetails = new ApiDetails();
                    $apiDetails->setBatch($api);
                    $apiDetails->setProcess($data['process']);
                    $apiDetails->setJsonData($data['json_body']);
                    $apiDetails->setStatus(0);
                    $api->addApiDetails($apiDetails);
                    $em->persist($api);
                    $em->flush();

                } else {
                    $apiDetails = new ApiDetails();
                    $apiDetails->setBatch($findParent);
                    $apiDetails->setProcess($data['process']);
                    $apiDetails->setJsonData($data['json_body']);
                    $apiDetails->setStatus(0);
                    $em->persist($apiDetails);
                    $em->flush();
                }
                return new JsonResponse([
                    'status' => 200,
                ]);
            }
            return new JsonResponse([
                'status' => 200,
            ]);
        } else {
            return new JsonResponse([
                'status' => false,
            ]);
        }
    }

    /**
     * @Route("/store-api-json-data", methods={"POST"}, name="store_api_json_data")
     * @param Request $request
     * @return JsonResponse
     */
    public function storeApiJsonData(Request $request)
    {
        set_time_limit(0);
        ignore_user_abort(true);

        $data = $request->request->all();

        if ($data) {
            $appVersion = $this->getDoctrine()->getRepository(AppVersions::class)->getActiveAppVersion();

            $em = $this->getDoctrine()->getManager();
            $findEmployee = $this->getDoctrine()->getRepository(User::class)->find($data['employee_id']);

            $existingApiCheck = $this->getDoctrine()->getRepository(Api::class)->findOneBy(['employee' => $findEmployee, 'batchNo' => $data['batch_id']]);

            if ($existingApiCheck) {
                return new JsonResponse([
                    'status' => 200,
                    'app_version' => $appVersion
                ]);
            }

            $api = new Api();
            $api->setBatchNo($data['batch_id'] ?: null);
            $api->setEmployee($findEmployee);
            $api->setStatus(0);
            $api->setCreatedAt(new \DateTime('now'));

            $jsonBody = json_decode($data['json_body'], true);

            $api->setApiDetailItemCount(count($jsonBody));

            foreach ($jsonBody as $process => $item) {
                $apiDetails = new ApiDetails();
                $apiDetails->setBatch($api);
                $apiDetails->setProcess($process);
                $apiDetails->setJsonData($item);
                $apiDetails->setStatus(0);
                $api->addApiDetails($apiDetails);
            }

            $em->persist($api);
            $em->flush();

            return new JsonResponse([
                'status' => 200,
                'app_version' => $appVersion
            ]);
        } else {
            return new JsonResponse([
                'status' => false,
            ]);
        }
    }

    /**
     * @Route("/list", name="api_response_list")
     */
    /*    public function apiResponseList()
        {
            $list = $this->getDoctrine()->getRepository(Api::class)->getData();
            return $this->render('@TerminalbdCrm/api/api-response-list.html.twig', [
                'list' => $list,
            ]);
        }*/

    /**
     * @Route("/{id}/insert-data", name="insert_json_data")
     * @param ApiDetails $apiDetails
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function insertDataIntoCorrespondingTable(ApiDetails $apiDetails)
    {
        /*       set_time_limit(0);
               ignore_user_abort(true);

               if ($apiDetails->isStatus() == 0){
                   $jsonToArray = json_decode($apiDetails->getJsonData(), true);
                   if ($apiDetails->getProcess() == 'crm_visit'){
                       foreach($jsonToArray as $data){
                           $this->getDoctrine()->getRepository(CrmVisit::class)->insertDataFromApi($data);
                       }
                   }elseif ($apiDetails->getProcess() == 'farmer_report'){
                       foreach($jsonToArray as $data){
                           if ($data['crm_visit_id'] !== null){
                               $findVisit = $this->getDoctrine()->getRepository(CrmVisit::class)->findOneBy(['appId' => $data['crm_visit_id']]);
                               if ($findVisit){
                                   $this->getDoctrine()->getRepository(CrmVisitDetails::class)->insertDataFromApi($data, $findVisit->getId());
                               }
                           }
                       }
                   }elseif ($apiDetails->getProcess() == 'layer_performance_report'){
                       foreach( $jsonToArray as $data){
                           $this->getDoctrine()->getRepository(LayerPerformanceDetails::class)->insertDataFromApi($data);
                       }
                   }elseif ($apiDetails->getProcess() == 'crm_visit_details'){
                       dd($jsonToArray);
                   }
                   $apiDetails->setStatus(1);
                   $em = $this->getDoctrine()->getManager();
                   $em->persist($apiDetails);
                   $em->flush();
                   $this->addFlash('success', 'Data has been migrated!');
                   return $this->redirectToRoute('api_response_list');
               }else{
                   $this->addFlash('error', 'Somthing Wrong!');
                   return $this->redirectToRoute('api_response_list');
               }*/
        return new Response(false);
    }

    /**
     * @Route("/companySpeciesWiseAvarageFCRBefore", methods={"GET"}, name="companySpeciesWiseAvarageFCRBefore")
     */

    public function companySpeciesWiseAvarageFCRBefore()
    {
        set_time_limit(0);
        ignore_user_abort(true);

        $entities = $this->getDoctrine()->getRepository(Api::class)->companySpeciesWiseAvarageFCRBefore(1);
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent(json_encode($entities));
        $response->setStatusCode(Response::HTTP_OK);
        return $response;
    }


    /**
     * @Route("/fishSalesPrice", methods={"GET"}, name="fishSalesPrice")
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @return JsonResponse|Response
     */

    public function fishSalesPrice(Request $request, ParameterBagInterface $parameterBag)
    {
        set_time_limit(0);
        ignore_user_abort(true);
        if ($request->getMethod() == 'GET' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {
            $entities = $this->getDoctrine()->getRepository(Api::class)->fishSalesPrice(1);
            $response = new Response();
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent(json_encode($entities));
            $response->setStatusCode(Response::HTTP_OK);
            return $response;
        }
        return new JsonResponse([
            'status' => 404,
            'message' => 'Not Found!'
        ]);


    }

    /**
     * @Route("/companyWiseFeedSaleFish", methods={"GET"}, name="companyWiseFeedSaleFish")
     */

    public function companyWiseFeedSaleFish()
    {
        set_time_limit(0);
        ignore_user_abort(true);

        $entities = $this->getDoctrine()->getRepository(Api::class)->companyWiseFeedSaleFish(1);
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent(json_encode($entities));
        $response->setStatusCode(Response::HTTP_OK);
        return $response;
    }

    /**
     * @Route("/company", methods={"GET"}, name="company")
     */
    public function company()
    {
        set_time_limit(0);
        ignore_user_abort(true);

        $entities = $this->getDoctrine()->getRepository(Api::class)->company(1);
        $response = new Response();
        $response->headers->set("Content-Type", "application/json");
        $response->setContent(json_encode($entities));
        $response->setStatusCode(Response::HTTP_OK);
        return $response;
    }

    /**
     * @Route("/competitorsCompany", methods={"GET"}, name="competitorsCompany")
     */
    public function competitorsCompany()
    {
        set_time_limit(0);
        ignore_user_abort(true);

        $entities = $this->getDoctrine()->getRepository(Api::class)->competitorsCompany(1);
        $response = new Response();
        $response->headers->set("Content-Type", "application/json");
        $response->setContent(json_encode($entities));
        $response->setStatusCode(Response::HTTP_OK);
        return $response;
    }

    /**
     * @Route("/complain", name="complain_api")
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @return JsonResponse
     */
    public function complain(Request $request, ParameterBagInterface $parameterBag)
    {
        set_time_limit(0);
        ignore_user_abort(true);

        if ($request->getMethod() == 'POST' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {
            $data = $request->request->all();
            $findEmployee = $this->getDoctrine()->getRepository(User::class)->find($data['employee_id']);
            $findAgent = $this->getDoctrine()->getRepository(Agent::class)->find($data['agent_id']);
            $findFarmer = $this->getDoctrine()->getRepository(CrmCustomer::class)->find($data['farmer_id']);
            $complainType = null;
            if (isset($data['complain_type_id']) && $data['complain_type_id'] != '') {
                $complainType = $this->getDoctrine()->getRepository(Setting::class)->find($data['complain_type_id']);
            }
            if ($findEmployee && $findAgent && $findFarmer) {
                $em = $this->getDoctrine()->getManager();

                $complain = new FarmerComplain();
                $complain->setEmployee($findEmployee);
                $complain->setAgent($findAgent);
                $complain->setFarmer($findFarmer);

                $em->persist($complain);
                $em->flush();

                $comments = json_decode($data['comments'], true);
                foreach ($comments as $comment) {
                    $fileName = '';
                    if (preg_match('/^data:image\/(\w+);base64,/', $comment['attachment'], $type)) {
                        $extension = $type[1];
                        $attachment = substr($comment['attachment'], strpos($comment['attachment'], ',') + 1);
                        $attachment = str_replace(' ', '+', $attachment);
                        $attachment = base64_decode($attachment);
                        $fileName = $data['farmer_id'] . '_' . $comment['comment'] . '_' . date('d-m-Y') . '_' . time() . '.' . $extension;

                        file_put_contents($parameterBag->get('projectRoot') . '/public/uploads/crm/visit/complain/' . $fileName, $attachment);
                    }

                    $details = new FarmerComplainDetails();
                    $details->setComplain($complain);
                    $details->setComment($comment['comment']);
                    $details->setAttachment($fileName);
                    if ($complainType) {
                        $details->setComplainType($complainType);
                    }
                    $em->persist($details);
                    $em->flush();

                }

                return new JsonResponse([
                    'status' => 200,
                    'message' => 'success'
                ]);
            } else {
                return new JsonResponse([
                    'status' => 500,
                    'message' => 'Server Error!'
                ]);
            }
        }

        return new JsonResponse([
            'status' => 500,
            'message' => 'Server Error!'
        ]);

    }

    /**
     * @Route("/agent-purpose", name="agent_purpose_api")
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @return JsonResponse|Response
     */
    public function agentPurposeApi(Request $request, ParameterBagInterface $parameterBag)
    {
        set_time_limit(0);
        ignore_user_abort(true);

        if ($request->getMethod() == 'GET' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {
//            $records = $this->getDoctrine()->getRepository(Setting::class)->findBy(array('settingType' => 'AGENT_PURPOSE','status'=>1));
            $entities = $this->getDoctrine()->getRepository(Api::class)->agentPurpose();
            /*$data = [];
            foreach ($records as $key => $record) {
                $data[$key]['id'] = $record->getId();
                $data[$key]['name'] = $record->getName();
            }*/
            $response = new Response();
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent(json_encode($entities));
            $response->setStatusCode(Response::HTTP_OK);
            return $response;
        }
        return new JsonResponse([
            'status' => 404,
            'message' => 'Not Found!'
        ]);

    }

    /**
     * @Route("/sub-agent-purpose", name="sub-agent_purpose_api")
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @return JsonResponse|Response
     */
    public function subAgentPurposeApi(Request $request, ParameterBagInterface $parameterBag)
    {
        set_time_limit(0);
        ignore_user_abort(true);

        if ($request->getMethod() == 'GET' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {
            $entities = $this->getDoctrine()->getRepository(Api::class)->subAgentPurpose();
            /*$records = $this->getDoctrine()->getRepository(Setting::class)->findBy(array('settingType' => 'SUB_AGENT_PURPOSE'));
            $data = [];
            foreach ($records as $key => $record) {
                $data[$key]['id'] = $record->getId();
                $data[$key]['name'] = $record->getName();
            }*/
            $response = new Response();
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent(json_encode($entities));
            $response->setStatusCode(Response::HTTP_OK);
            return $response;
        }
        return new JsonResponse([
            'status' => 404,
            'message' => 'Not Found!'
        ]);

    }

    /**
     * @Route("/other-agent-purpose", name="other-agent_purpose_api")
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @return JsonResponse|Response
     */
    public function otherAgentPurposeApi(Request $request, ParameterBagInterface $parameterBag)
    {
        set_time_limit(0);
        ignore_user_abort(true);

        if ($request->getMethod() == 'GET' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {
            $entities = $this->getDoctrine()->getRepository(Api::class)->otherAgentPurpose();
            /*$records = $this->getDoctrine()->getRepository(Setting::class)->findBy(array('settingType' => 'OTHER_AGENT_PURPOSE'));
            $data = [];
            foreach ($records as $key => $record) {
                $data[$key]['id'] = $record->getId();
                $data[$key]['name'] = $record->getName();
            }*/
            $response = new Response();
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent(json_encode($entities));
            $response->setStatusCode(Response::HTTP_OK);
            return $response;
        }
        return new JsonResponse([
            'status' => 404,
            'message' => 'Not Found!'
        ]);
    }

    /**
     * @Route("/create-sub-agent", name="create_sub_agent_api")
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @return Response
     */
    public function createSubAgent(Request $request, ParameterBagInterface $parameterBag)
    {
        set_time_limit(0);
        ignore_user_abort(true);

        if ($request->getMethod() == 'POST' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {
            $entity = new Agent();
            $allRequestData = $request->request->all();

            $group = $this->getDoctrine()->getRepository(\App\Entity\Core\Setting::class)->findOneBy(array('slug' => 'sub-agent'));
            $location = $this->getDoctrine()->getRepository(Location::class)->find($allRequestData['location']);
            $entity->setName($allRequestData['name']);
            $entity->setAddress($allRequestData['address']);
            $entity->setMobile($allRequestData['mobile']);
            $entity->setAgentGroup($group);
            $entity->setUpozila($location);
            $entity->setDistrict($location->getParent());
            if ($allRequestData['agent']) {
                $agent = $this->getDoctrine()->getRepository(Agent::class)->find($allRequestData['agent']);
                $entity->setParent($agent);
            }
            $em = $this->getDoctrine()->getManager();

            try {
                $em->persist($entity);
                $em->flush();

                return new JsonResponse([
                    'status' => 200,
                    'message' => 'Success'
                ]);
            } catch (Exception $e) {
                $response = new Response();
                $response->headers->set('Content-Type', 'application/json');
                $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
                $response->setContent($e->getMessage());
                return $response;
            }
        }

        return new JsonResponse([
            'status' => 500,
            'message' => 'Server Error!'
        ]);
    }


    /**
     * @Route("/create-other-agent", name="create_other_agent_api")
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @return Response
     */
    public function createOtherAgent(Request $request, ParameterBagInterface $parameterBag)
    {
        set_time_limit(0);
        ignore_user_abort(true);

        if ($request->getMethod() == 'POST' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {
            $entity = new Agent();
            $allRequestData = $request->request->all();
            $group = $this->getDoctrine()->getRepository(\App\Entity\Core\Setting::class)->findOneBy(array('slug' => 'other-agent'));
            $location = $this->getDoctrine()->getRepository(Location::class)->find($allRequestData['location']);
            $entity->setName($allRequestData['name']);
            $entity->setAddress($allRequestData['address']);
            $entity->setMobile($allRequestData['mobile']);
            $entity->setAgentGroup($group);
            $entity->setUpozila($location);
            $entity->setDistrict($location->getParent());
            $entity->setCreated(new \DateTime('now'));
            $em = $this->getDoctrine()->getManager();

            try {
                $em->persist($entity);
                $em->flush();

                return new JsonResponse([
                    'status' => 200,
                    'message' => 'Success'
                ]);
            } catch (Exception $e) {
                $response = new Response();
                $response->headers->set('Content-Type', 'application/json');
                $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
                $response->setContent($e->getMessage());
                return $response;
            }
        }

        return new JsonResponse([
            'status' => 500,
            'message' => 'Server Error!'
        ]);
    }

    /**
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @Route("/chick-life-cycle", name="chick_life_cycle_api")
     * @return JsonResponse|Response
     */
    public function getChickLifeCycle(Request $request, ParameterBagInterface $parameterBag)
    {
        set_time_limit(0);
        ignore_user_abort(true);

        if ($request->getMethod() == 'POST' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {
            $parameters = $request->request->all();
            $records = $this->getDoctrine()->getRepository(Api::class)->getLifeCycleData($parameters);

            $response = new Response();
            $response->headers->set('Content-Type', 'application/json');
            if ($records) {
                $response->setContent(json_encode($records));
            } else {
                $response->setContent(json_encode([
                    'status' => 404,
                    'message' => 'Not found!'
                ]));
            }
            $response->setStatusCode(Response::HTTP_OK);
            return $response;
        }
        return new JsonResponse([
            'status' => 404,
            'message' => 'Not Found!'
        ]);
    }

    /**
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @Route("/layer-life-cycle", name="layer_life_cycle_api")
     * @return JsonResponse|Response
     */
    public function getLayerLifeCycle(Request $request, ParameterBagInterface $parameterBag)
    {
        set_time_limit(0);
        ignore_user_abort(true);

        if ($request->getMethod() == 'POST' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {
            $parameters = $request->request->all();
            $records = $this->getDoctrine()->getRepository(Api::class)->getLayerLifeCycleData($parameters);

            $response = new Response();
            $response->headers->set('Content-Type', 'application/json');
            if ($records) {
                $response->setContent(json_encode($records));
            } else {
                $response->setContent(json_encode([
                    'status' => 404,
                    'message' => 'Not found!'
                ]));
            }
            $response->setStatusCode(Response::HTTP_OK);
            return $response;
        }
        return new JsonResponse([
            'status' => 404,
            'message' => 'Not Found!'
        ]);
    }

    /**
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @Route("/cattle-life-cycle", name="cattle_life_cycle_api")
     * @return JsonResponse|Response
     */
    public function getCattleLifeCycle(Request $request, ParameterBagInterface $parameterBag)
    {
        set_time_limit(0);
        ignore_user_abort(true);

        if ($request->getMethod() == 'POST' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {
            $parameters = $request->request->all();
            $records = $this->getDoctrine()->getRepository(Api::class)->getCattleLifeCycleData($parameters);

            $response = new Response();
            $response->headers->set('Content-Type', 'application/json');
            if ($records) {
                $response->setContent(json_encode($records));
            } else {
                $response->setContent(json_encode([
                    'status' => 404,
                    'message' => 'Not found!'
                ]));
            }
            $response->setStatusCode(Response::HTTP_OK);
            return $response;
        }
        return new JsonResponse([
            'status' => 404,
            'message' => 'Not Found!'
        ]);
    }

    /**
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @return Response
     * @Route("/app/version", name="app_version")
     */
    public function appVersion(Request $request, ParameterBagInterface $parameterBag)
    {
        set_time_limit(0);
        ignore_user_abort(true);
        $response = new Response();

        if ($request->getMethod() == 'GET' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {
            $version = $this->getDoctrine()->getRepository(Api::class)->getCurrentVersion();
            $response->headers->set('Content-Type', 'application/json');
            if ($version) {
                $response->setContent(json_encode($version));
                $response->setStatusCode(Response::HTTP_OK);
                return $response;
            }
        }
        $response->setContent(json_encode([
            'status' => 404,
            'message' => 'Not found!'
        ]));
        return $response;

    }

    /**
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @return Response
     * @Route("/complain-doc", name="complain_doc_api")
     */
    public function complainDocApi(Request $request, ParameterBagInterface $parameterBag)
    {
        set_time_limit(0);
        ignore_user_abort(true);
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');

        if ($request->getMethod() == 'GET' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {
            $complainDoc = $this->getDoctrine()->getRepository(Api::class)->getComplainType('COMPLAIN_DOC');
            if ($complainDoc) {
                $response->setContent(json_encode($complainDoc));
                $response->setStatusCode(Response::HTTP_OK);
                return $response;
            }
            $response->setContent(json_encode([
                'status' => 404,
                'message' => 'Not found!'
            ]));
            return $response;

        }
        $response->setContent(json_encode([
            'status' => 401,
            'message' => 'Invalid Request!'
        ]));
        return $response;
    }

    /**
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @return Response
     * @Route("/complain-feed", name="complain_feed_api")
     */
    public function complainFeedApi(Request $request, ParameterBagInterface $parameterBag)
    {
        set_time_limit(0);
        ignore_user_abort(true);
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');

        if ($request->getMethod() == 'GET' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {
            $complainFeed = $this->getDoctrine()->getRepository(Api::class)->getComplainType('COMPLAIN_FEED');
            if ($complainFeed) {
                $response->setContent(json_encode($complainFeed));
                $response->setStatusCode(Response::HTTP_OK);
                return $response;
            }
            $response->setContent(json_encode([
                'status' => 404,
                'message' => 'Not found!'
            ]));
            return $response;

        }
        $response->setContent(json_encode([
            'status' => 401,
            'message' => 'Invalid Request!'
        ]));
        return $response;
    }

    /**
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @return Response
     * @Route("/transport", name="transport_api")
     */
    public function transportApi(Request $request, ParameterBagInterface $parameterBag)
    {
        set_time_limit(0);
        ignore_user_abort(true);
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');

        if ($request->getMethod() == 'GET' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {
            $transports = $this->getDoctrine()->getRepository(Api::class)->getTransport('TRANSPORT');
            if ($transports) {
                $response->setContent(json_encode($transports));
                $response->setStatusCode(Response::HTTP_OK);
                return $response;
            }
            $response->setContent(json_encode([
                'status' => 404,
                'message' => 'Not found!'
            ]));
            return $response;

        }
        $response->setContent(json_encode([
            'status' => 401,
            'message' => 'Invalid Request!'
        ]));
        return $response;
    }

    /**
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @return Response
     * @Route("/hatchery-nourish", name="hatchery_nourish_api")
     */
    public function nourishHatcheryApi(Request $request, ParameterBagInterface $parameterBag)
    {
        set_time_limit(0);
        ignore_user_abort(true);
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');

        if ($request->getMethod() == 'GET' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {
            $hatcheries = $this->getDoctrine()->getRepository(Api::class)->getNourishHatchery('HATCHERY_NOURISH');
            if ($hatcheries) {
                $response->setContent(json_encode($hatcheries));
                $response->setStatusCode(Response::HTTP_OK);
                return $response;
            }
            $response->setContent(json_encode([
                'status' => 404,
                'message' => 'Not found!'
            ]));
            return $response;

        }
        $response->setContent(json_encode([
            'status' => 401,
            'message' => 'Invalid Request!'
        ]));
        return $response;
    }


    /**
     * @Route("/labName", methods={"GET"}, name="lab_name")
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @return JsonResponse|Response
     */
    public function labName(Request $request, ParameterBagInterface $parameterBag)
    {
        set_time_limit(0);
        ignore_user_abort(true);
        if ($request->getMethod() == 'GET' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {
            $entities = [];
            $data = $request->query->all();
            if (isset($data['employee_id']) && $data['employee_id'] != '') {
                $findEmployee = $this->getDoctrine()->getRepository(User::class)->find($data['employee_id']);
                if ($findEmployee && $findEmployee->getLabs() != '') {
                    $entities = $this->getDoctrine()->getRepository(Api::class)->labName($findEmployee->getLabs());
                }
            }

            $response = new Response();
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent(json_encode($entities));
            $response->setStatusCode(Response::HTTP_OK);
            return $response;
        }
        return new JsonResponse([
            'status' => 404,
            'message' => 'Not Found!'
        ]);
    }

    /**
     * @Route("/labServiceName", methods={"GET"}, name="lab_service_name")
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @return JsonResponse|Response
     */
    public function labServiceName(Request $request, ParameterBagInterface $parameterBag)
    {
        set_time_limit(0);
        ignore_user_abort(true);
        if ($request->getMethod() == 'GET' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {
            $entities = $this->getDoctrine()->getRepository(Api::class)->labServiceName();
            $response = new Response();
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent(json_encode($entities));
            $response->setStatusCode(Response::HTTP_OK);
            return $response;
        }
        return new JsonResponse([
            'status' => 404,
            'message' => 'Not Found!'
        ]);


    }

    /**
     * @Route("/chick-life-cycle-in-progress", name="chick_life_cycle_in_progress")
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @return JsonResponse|Response
     */
    public function chickLifeCycleInProgress(Request $request, ParameterBagInterface $parameterBag)
    {
        set_time_limit(0);
        ignore_user_abort(true);
        if ($request->getMethod() == 'POST' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {
            $parameters = $request->request->all();
            $arrayData = [];
            if (isset($parameters['employee_id']) && $parameters['employee_id'] != "") {
                $arrayData = $this->getChickLifeCycleInprogressData($parameters['employee_id']);
            }

            $response = new Response();
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent(json_encode($arrayData));
            $response->setStatusCode(Response::HTTP_OK);
            return $response;
        }
        return new JsonResponse([
            'status' => 404,
            'message' => 'Not Found!'
        ]);


    }

    private function getChickLifeCycleInprogressData($employeeId)
    {
        $arrayData = [];
        if ($employeeId) {
            $employee = $this->getDoctrine()->getRepository(User::class)->find($employeeId);

            $entities = $this->getDoctrine()->getRepository(ChickLifeCycle::class)->findBy(['employee' => $employee, 'lifeCycleState' => 'IN_PROGRESS'], ['id' => 'ASC']);

            if ($entities) {
                /** @var ChickLifeCycle $entity */
                foreach ($entities as $entity) {
                    $arrayData[] = [
                        "id" => $entity->getAppId() ? (int)$entity->getAppId() : $entity->getId(),
                        "hatching_date" => $entity->getHatchingDate() ? $entity->getHatchingDate()->format('Y-m-d') : "",
                        "remarks" => $entity->getRemarks() ? $entity->getRemarks() : null,
                        "reporting_date" => $entity->getReportingDate() ? $entity->getReportingDate()->format('Y-m-d') : "",
                        "customer_id" => $entity->getCustomer() ? $entity->getCustomer()->getId() : null,
                        "agent_id" => $entity->getAgent() ? $entity->getAgent()->getId() : null,
                        "employee_id" => $entity->getEmployee() ? $entity->getEmployee()->getId() : null,
                        "report_id" => $entity->getReport() ? $entity->getReport()->getId() : null,
                        "life_cycle_state" => $entity->getLifeCycleState() ? $entity->getLifeCycleState() : "",
                        "created_at" => $entity->getCreatedAt() ? $entity->getCreatedAt()->format('Y-m-d H:i:s') : null,
                        "hatchery_id" => $entity->getHatchery() ? $entity->getHatchery()->getId() : null,
                        "breed_id" => $entity->getBreed() ? $entity->getBreed()->getId() : null,
                        "feed_id" => $entity->getFeed() ? $entity->getFeed()->getId() : null,
                        "feed_mill_id" => $entity->getFeedMill() ? $entity->getFeedMill()->getId() : null,
                        "total_birds" => $entity->getTotalBirds() ? (string)$entity->getTotalBirds() : "0",
                        "hatchery_name" => $entity->getHatchery() ? $entity->getHatchery()->getName() : "",
                        "breed_name" => $entity->getBreed() ? $entity->getBreed()->getName() : "",
                        "feed_name" => $entity->getFeed() ? $entity->getFeed()->getName() : "",
                        "feed_mill_name" => $entity->getFeedMill() ? $entity->getFeedMill()->getName() : "",
                        "crm_visit_id" => null,
                        "is_sync" => 1,
                        "visit_details_id" => null,
                        "web_life_cycle_id" => $entity->getId(),
                        "farm_number" => $entity->getFarmNumber() ? $entity->getFarmNumber() : 1,
                        "agent_name" => $entity->getAgent() ? $entity->getAgent()->getName() : "",
                        "customer_name" => $entity->getCustomer() ? $entity->getCustomer()->getName() : "",
                        "address" => $entity->getAgent() ? $entity->getAgent()->getUpozila()->getName() : "",
                        "report_type_name" => $entity->getReport() ? $entity->getReport()->getName() : null,
                    ];
                }
            }
        }

        return $arrayData;
    }

    /**
     * @Route("/chick-life-cycle-details-in-progress", name="chick_life_cycle_details_in_progress")
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @return JsonResponse|Response
     */
    public function chickLifeCycleDetailsInProgress(Request $request, ParameterBagInterface $parameterBag)
    {
        set_time_limit(0);
        ignore_user_abort(true);
        if ($request->getMethod() == 'POST' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {
            $parameters = $request->request->all();
            $arrayData = [];
            if (isset($parameters['employee_id']) && $parameters['employee_id'] != "") {

                $arrayData = $this->getChickLifeCycleDetailsInprogressData($parameters['employee_id']);

            }

            $response = new Response();
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent(json_encode($arrayData));
            $response->setStatusCode(Response::HTTP_OK);
            return $response;

        }
        return new JsonResponse([
            'status' => 404,
            'message' => 'Not Found!'
        ]);


    }

    private function getChickLifeCycleDetailsInprogressData($employeeId)
    {
        $arrayData = [];
        if ($employeeId) {
            $employee = $this->getDoctrine()->getRepository(User::class)->find($employeeId);

            $entities = $this->getDoctrine()->getRepository(ChickLifeCycle::class)->findBy(['employee' => $employee, 'lifeCycleState' => 'IN_PROGRESS'], ['id' => 'ASC']);

            if ($entities) {
                /** @var ChickLifeCycle $entity */
                foreach ($entities as $entity) {
                    if ($entity->getCrmChickLifeCycleDetails()) {
                        /* @var ChickLifeCycleDetails $lifeCycleDetail */
                        foreach ($entity->getCrmChickLifeCycleDetails() as $lifeCycleDetail) {
                            if ($lifeCycleDetail->getReportingDate() != "") {
                                $lifeCycleDate = $entity->getCreatedAt() ? $entity->getCreatedAt()->format('Y-m-d H:i:s') : null;
                                $arrayData[] = [
                                    "id" => $lifeCycleDetail->getAppId() ? (int)$lifeCycleDetail->getAppId() : $lifeCycleDetail->getId(),
                                    "crm_chick_life_cycle_id" => $entity->getAppId() ? (int)$entity->getAppId() : $entity->getId(),
                                    "visiting_week" => $lifeCycleDetail->getVisitingWeek() ? (string)$lifeCycleDetail->getVisitingWeek() : "",
                                    "age_days" => $lifeCycleDetail->getAgeDays() ? (string)$lifeCycleDetail->getAgeDays() : "0",
                                    "mortality_pes" => $lifeCycleDetail->getMortalityPes() ? (string)$lifeCycleDetail->getMortalityPes() : "0",
                                    "mortality_percent" => $lifeCycleDetail->calculateMortalityPercent() > 0 ? (string)number_format($lifeCycleDetail->calculateMortalityPercent(), 2, '.', '') : "0",
                                    "weight_standard" => $lifeCycleDetail->getWeightStandard() ? (string)$lifeCycleDetail->getWeightStandard() : "0",
                                    "weight_achieved" => $lifeCycleDetail->getWeightAchieved() ? (string)$lifeCycleDetail->getWeightAchieved() : "0",
                                    "feed_total_kg" => $lifeCycleDetail->getFeedTotalKg() ? (string)$lifeCycleDetail->getFeedTotalKg() : "0",
                                    "per_bird" => $lifeCycleDetail->getPerBird() ? (string)$lifeCycleDetail->getPerBird() : "0",
                                    "feed_standard" => $lifeCycleDetail->getFeedStandard() ? (string)$lifeCycleDetail->getFeedStandard() : "0",
                                    "without_mortality" => $lifeCycleDetail->calculateWithoutMortality() > 0 ? (string)number_format($lifeCycleDetail->calculateWithoutMortality(), 5, '.', '') : "0",
                                    "with_mortality" => $lifeCycleDetail->calculateWithMortality() > 0 ? (string)number_format($lifeCycleDetail->calculateWithMortality(), 5, '.', '') : "0",
                                    "pro_date" => $lifeCycleDetail->getProDate() ? $lifeCycleDetail->getProDate()->format('Y-m-d') : "",
                                    "batch_no" => $lifeCycleDetail->getBatchNo(),
                                    "remarks" => $lifeCycleDetail->getRemarks(),
                                    "created_at" => $lifeCycleDetail->getCreatedAt() && $lifeCycleDetail->getCreatedAt()->format('Y-m-d H:i:s') != '-0001-11-30 00:00:00' ? $lifeCycleDetail->getCreatedAt()->format('Y-m-d H:i:s') : $lifeCycleDate,
                                    "updated_at" => $lifeCycleDetail->getUpdatedAt() ? $lifeCycleDetail->getUpdatedAt()->format('Y-m-d H:i:s') : "",
                                    "feed_type_id" => $lifeCycleDetail->getFeedType() ? $lifeCycleDetail->getFeedType()->getId() : null,
                                    "reporting_date" => $lifeCycleDetail->getReportingDate() ? $lifeCycleDetail->getReportingDate()->format('Y-m-d') : "",
                                    "crm_visit_id" => null,
                                    "is_sync" => 1,
                                    "customer_id" => $entity->getCustomer() ? $entity->getCustomer()->getId() : null,
                                    "employee_id" => $entity->getEmployee() ? $entity->getEmployee()->getId() : null,
                                    "report_id" => $entity->getReport() ? $entity->getReport()->getId() : null,
                                    'visit_details_id' => null,
                                    'life_cycle_state' => $entity->getLifeCycleState(),
                                    "web_life_cycle_details_id" => $lifeCycleDetail->getId(),
                                    "web_life_cycle_id" => $entity->getId(),
                                    "farm_number" => $entity->getFarmNumber() ? $entity->getFarmNumber() : 1,
                                ];
                            }
                        }
                    }
                }
            }
        }

        return $arrayData;

    }

    /**
     * @Route("/layer-life-cycle-in-progress", name="layer_life_cycle_in_progress")
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @return JsonResponse|Response
     */
    public function layerLifeCycleInProgress(Request $request, ParameterBagInterface $parameterBag)
    {
        set_time_limit(0);
        ignore_user_abort(true);
        if ($request->getMethod() == 'POST' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {
            $parameters = $request->request->all();

            $arrayData = [];
            if (isset($parameters['employee_id']) && $parameters['employee_id'] != "") {
                $arrayData = $this->getLayerLifeCycleInprogressData($parameters['employee_id']);
            }

            $response = new Response();
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent(json_encode($arrayData));
            $response->setStatusCode(Response::HTTP_OK);
            return $response;


            /*$entities = $this->getDoctrine()->getRepository(Api::class)->layerLifeCycleInProgress($parameters);
            $response = new Response();
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent(json_encode($entities));
            $response->setStatusCode(Response::HTTP_OK);
            return $response;*/
        }
        return new JsonResponse([
            'status' => 404,
            'message' => 'Not Found!'
        ]);


    }

    private function getLayerLifeCycleInprogressData($employeeId)
    {

        $arrayData = [];
        if ($employeeId) {
            $employee = $this->getDoctrine()->getRepository(User::class)->find($employeeId);

            $entities = $this->getDoctrine()->getRepository(LayerLifeCycle::class)->findBy(['employee' => $employee, 'lifeCycleState' => 'IN_PROGRESS'], ['id' => 'ASC']);

            if ($entities) {
                /** @var LayerLifeCycle $entity */
                foreach ($entities as $entity) {
                    $arrayData[] = [
                        "id" => $entity->getAppId() ? (int)$entity->getAppId() : $entity->getId(),
                        "total_birds" => $entity->getTotalBirds() ? (string)$entity->getTotalBirds() : "0",
                        "hatchery_date" => $entity->getHatcheryDate() ? $entity->getHatcheryDate()->format('Y-m-d') : null,
                        "created" => $entity->getCreated() ? $entity->getCreated()->format('Y-m-d H:i:s') : null,
                        "updated" => $entity->getUpdated() ? $entity->getUpdated()->format('Y-m-d H:i:s') : null,
                        "customer_id" => $entity->getCustomer() ? $entity->getCustomer()->getId() : null,
                        "agent_id" => $entity->getAgent() ? $entity->getAgent()->getId() : null,
                        "employee_id" => $entity->getEmployee() ? $entity->getEmployee()->getId() : null,
                        "report_id" => $entity->getReport() ? $entity->getReport()->getId() : null,
                        "life_cycle_state" => $entity->getLifeCycleState(),
                        "hatchery_id" => $entity->getHatchery() ? $entity->getHatchery()->getId() : null,
                        "breed_id" => $entity->getBreed() ? $entity->getBreed()->getId() : null,
                        "feed_id" => $entity->getFeed() ? $entity->getFeed()->getId() : null,
                        "feed_mill_id" => $entity->getFeedMill() ? $entity->getFeedMill()->getId() : null,
                        "hatchery_name" => $entity->getHatchery() ? $entity->getHatchery()->getName() : "",
                        "breed_name" => $entity->getBreed() ? $entity->getBreed()->getName() : "",
                        "feed_name" => $entity->getFeed() ? $entity->getFeed()->getName() : "",
                        "feed_mill_name" => $entity->getFeedMill() ? $entity->getFeedMill()->getName() : "",
                        "crm_visit_id" => null,
                        "is_sync" => 1,
                        "visit_details_id" => null,
                        "web_life_cycle_id" => $entity->getId(),
                        "farm_number" => $entity->getFarmNumber() ? $entity->getFarmNumber() : 1,
                        "agent_name" => $entity->getAgent() ? $entity->getAgent()->getName() : "",
                        "customer_name" => $entity->getCustomer() ? $entity->getCustomer()->getName() : "",
                        "address" => $entity->getAgent() ? $entity->getAgent()->getUpozila()->getName() : "",
                        "report_type_name" => $entity->getReport() ? $entity->getReport()->getName() : null,
                        "reporting_date" => $entity->getReportingDate() ? $entity->getReportingDate()->format('Y-m-d') : $entity->getCreated()->format('Y-m-d'),
                    ];
                }
            }
        }

        return $arrayData;

    }


    /**
     * @Route("/layer-life-cycle-details-in-progress", name="layer_life_cycle_details_in_progress")
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @return JsonResponse|Response
     */
    public function layerLifeCycleDetailsInProgress(Request $request, ParameterBagInterface $parameterBag)
    {
        set_time_limit(0);
        ignore_user_abort(true);
        if ($request->getMethod() == 'POST' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {
            $parameters = $request->request->all();
            $arrayData = [];
            if (isset($parameters['employee_id']) && $parameters['employee_id'] != "") {

                $arrayData = $this->getLayerLifeCycleDetailsInprogressData($parameters['employee_id']);
            }

            $response = new Response();
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent(json_encode($arrayData));
            $response->setStatusCode(Response::HTTP_OK);
            return $response;

        }
        return new JsonResponse([
            'status' => 404,
            'message' => 'Not Found!'
        ]);


    }

    private function getLayerLifeCycleDetailsInprogressData($employeeId)
    {
        $arrayData = [];
        if ($employeeId) {
            $employee = $this->getDoctrine()->getRepository(User::class)->find($employeeId);

            $entities = $this->getDoctrine()->getRepository(LayerLifeCycle::class)->findBy(['employee' => $employee, 'lifeCycleState' => 'IN_PROGRESS'], ['id' => 'ASC']);

            if ($entities) {
                /** @var LayerLifeCycle $entity */
                foreach ($entities as $entity) {
                    if ($entity->getCrmLayerLifeCycleDetails()) {
                        /* @var LayerLifeCycleDetails $lifeCycleDetail */
                        foreach ($entity->getCrmLayerLifeCycleDetails() as $lifeCycleDetail) {
                            if ($lifeCycleDetail->getVisitingDate() != "" && $lifeCycleDetail->getIsVisited() == 1) {
                                $arrayData[] = [
                                    "id" => $lifeCycleDetail->getAppId() ? (int)$lifeCycleDetail->getAppId() : $lifeCycleDetail->getId(),
                                    "crm_layer_life_cycle_id" => $entity->getAppId() ? (int)$entity->getAppId() : $entity->getId(),
                                    "visiting_date" => $lifeCycleDetail->getVisitingDate() ? $lifeCycleDetail->getVisitingDate()->format('Y-m-d') : null,
                                    "age_week" => $lifeCycleDetail->getAgeWeek() ? (string)$lifeCycleDetail->getAgeWeekOnlyNumber() : "",
                                    "dead_bird" => $lifeCycleDetail->getDeadBird() ? (string)$lifeCycleDetail->getDeadBird() : "0",
                                    "avg_weight" => $lifeCycleDetail->getAvgWeight() ? (string)$lifeCycleDetail->getAvgWeight() : "0",
                                    "target_weight" => $lifeCycleDetail->getTargetWeight() ? (string)$lifeCycleDetail->getTargetWeight() : "0",
                                    "uniformity" => $lifeCycleDetail->getUniformity() ? (string)$lifeCycleDetail->getUniformity() : "0",
                                    "feed_per_bird" => $lifeCycleDetail->getFeedPerBird() ? (string)$lifeCycleDetail->getFeedPerBird() : "0",
                                    "target_feed_per_bird" => $lifeCycleDetail->getTargetFeedPerBird() ? (string)$lifeCycleDetail->getTargetFeedPerBird() : "0",
                                    "total_eggs" => $lifeCycleDetail->getTotalEggs() ? (string)$lifeCycleDetail->getTotalEggs() : "0",
                                    "target_egg_production" => $lifeCycleDetail->getTargetEggProduction() ? (string)$lifeCycleDetail->getTargetEggProduction() : "0",
                                    "egg_weight_actual" => $lifeCycleDetail->getEggWeightActual() ? (string)$lifeCycleDetail->getEggWeightActual() : "0",
                                    "egg_weight_standard" => $lifeCycleDetail->getEggWeightStandard() ? (string)$lifeCycleDetail->getEggWeightStandard() : "0",
                                    "production_date" => $lifeCycleDetail->getProductionDate() ? $lifeCycleDetail->getProductionDate()->format('Y-m-d') : "",
                                    "batch_no" => $lifeCycleDetail->getBatchNo() ? $lifeCycleDetail->getBatchNo() : "",
                                    "medicine" => $lifeCycleDetail->getMedicine() ? $lifeCycleDetail->getMedicine() : "",
                                    "remarks" => $lifeCycleDetail->getRemarks() ? $lifeCycleDetail->getRemarks() : "",
                                    "created" => $lifeCycleDetail->getCreated() ? $lifeCycleDetail->getCreated()->format('Y-m-d H:i:s') : null,
                                    "updated" => $lifeCycleDetail->getUpdated() ? $lifeCycleDetail->getUpdated()->format('Y-m-d H:i:s') : null,
                                    "feed_mill_id" => $lifeCycleDetail->getFeedMill() ? $lifeCycleDetail->getFeedMill()->getId() : null,
                                    "feed_type_id" => $lifeCycleDetail->getFeedType() ? $lifeCycleDetail->getFeedType()->getId() : null,
                                    "crm_visit_id" => null,
                                    "is_sync" => 1,
                                    "customer_id" => $entity->getCustomer() ? $entity->getCustomer()->getId() : null,
                                    "employee_id" => $entity->getEmployee() ? $entity->getEmployee()->getId() : null,
                                    "report_id" => $entity->getReport() ? $entity->getReport()->getId() : null,
                                    "visit_details_id" => null,
                                    'life_cycle_state' => $entity->getLifeCycleState(),
                                    "web_life_cycle_details_id" => $lifeCycleDetail->getId(),
                                    "web_life_cycle_id" => $entity->getId(),
                                    "farm_number" => $entity->getFarmNumber() ? $entity->getFarmNumber() : 1,
                                ];
                            }

                        }
                    }
                }
            }
        }
        return $arrayData;
    }

    /**
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @return JsonResponse|Response
     * @Route("/employee-location", name="employee_location")
     */
    public function employeeLocation(Request $request, ParameterBagInterface $parameterBag)
    {
        set_time_limit(0);
        ignore_user_abort(true);
        if ($request->getMethod() == 'POST' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {
            $lineManager = (int)$request->request->get('line_manager_id');
            $lineManager = $this->getDoctrine()->getRepository(User::class)->find($lineManager);

            if (!$lineManager) {
                $response = new Response();
                $response->headers->set('Content-Type', 'application/json');
                $response->setContent(json_encode(['message' => 'Unregistered user', 'status' => Response::HTTP_NOT_FOUND]));
                $response->setStatusCode(Response::HTTP_NOT_FOUND);
                return $response;
            }

            if (in_array('ROLE_CRM_POULTRY_ADMIN', (array)$lineManager->getRoles()) || in_array('ROLE_CRM_CATTLE_ADMIN', (array)$lineManager->getRoles()) || in_array('ROLE_CRM_AQUA_ADMIN', (array)$lineManager->getRoles()) || in_array('ROLE_CRM_SALES_MARKETING_ADMIN', (array)$lineManager->getRoles())) {
                $entities = $this->getDoctrine()->getRepository(Api::class)->getEmployeeLocation($lineManager, []);
                $response = new Response();
                $response->headers->set('Content-Type', 'application/json');
                $response->setContent(json_encode($entities));
                $response->setStatusCode(Response::HTTP_OK);
                return $response;
            }

            $userType = $this->getDoctrine()->getRepository(\App\Entity\Core\Setting::class)->findOneBy(['slug' => 'employee']);
            $allUser = $this->getDoctrine()->getRepository(User::class)->findBy(['enabled' => 1, 'userGroup' => $userType]); //136

            $users = [];
            foreach ($allUser as $user) {
                if ($user->getLineManager() && $user->isEnabled()) {
                    $users[] = [
                        'id' => $user->getId(),
                        'lineManager' => $user->getLineManager()->getId()
                    ];
                }
            }

            $members = $this->getMemberTree($users, $lineManager->getId());
            $ids = [];
            foreach ($members as $key => $item) {
                if (isset($item['ids'])) {
                    array_push($members[$key]['ids'], $item['id']);
                } else {
                    $members[$key]['ids'][] = $item['id'];

                }
                foreach ($members[$key]['ids'] as $id) {
                    if (!in_array($id, $ids)) {
                        $ids[] = $id;
                    }
                }
            }

            $entities = $this->getDoctrine()->getRepository(Api::class)->getEmployeeLocation($lineManager, $ids);

            $response = new Response();
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent(json_encode($entities));
            $response->setStatusCode(Response::HTTP_OK);
            return $response;
        }
        return new JsonResponse([
            'status' => 404,
            'message' => 'Not Found!'
        ]);
    }

    private function getMemberTree(array $allEmployee, $lineManagerId)
    {
        $members = array();

        foreach ($allEmployee as $employee) {

            if ($employee['lineManager']) {
                if ($employee['lineManager'] == $lineManagerId) {

                    $member = $this->getMemberTree($allEmployee, $employee['id']);
                    if ($member) {
                        foreach ($member as $child) {
                            $employee['ids'][] = $child['id'];
                        }
                    }
                    $members[] = $employee;
                }
            }
        }
        return $members;

    }
    /*    private function getMemberTree($membersId, $lineManager)
        {
            $members = $this->getDoctrine()->getRepository(Api::class)->getMembers($lineManager);
            if ($members){
                foreach ($members as $member) {
                    array_push($membersId, $member['id']);
                    $child = $this->getMemberTree($membersId, $member['id']);
                    if ($child){
                        $membersId[] = $child;
                    }

                }
            }
            return $membersId;

        }*/

    /**
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @return JsonResponse|Response
     * @Route("/employee-location-update", name="employee_location_update")
     */
    public function setEmployeeLocation(Request $request, ParameterBagInterface $parameterBag)
    {
        set_time_limit(0);
        ignore_user_abort(true);
        if ($request->getMethod() == 'POST' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {
            $parameters = $request->request->all();

            if ($parameters['employee_id']) {
                $findUser = $this->getDoctrine()->getRepository(User::class)->find($parameters['employee_id']);
                if ($findUser) {
                    $findUser->setLatitude($parameters['latitude'] ?: null);
                    $findUser->setLongitude($parameters['longitude'] ?: null);
                    $this->getDoctrine()->getManager()->flush();

                    return new JsonResponse([
                        'status' => 200,
                        'message' => 'success'
                    ]);
                }
            }
        }
        return new JsonResponse([
            'status' => 404,
            'message' => 'Not Found!'
        ]);
    }

    /**
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @return JsonResponse
     * @Route("/regions", name="regions")
     */
    public function regions(Request $request, ParameterBagInterface $parameterBag)
    {
        set_time_limit(0);
        ignore_user_abort(true);
        if ($request->getMethod() == 'GET' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {
            $regions = $this->getDoctrine()->getRepository(Location::class)->findBy(['level' => 3]);
            $data = [];
            foreach ($regions as $region) {
                $data[] = [
                    'id' => $region->getId(),
                    'name' => $region->getName(),
                ];
            }
            return new JsonResponse($data);
        }
        return new JsonResponse([
            'status' => 404,
            'message' => 'Not Found!'
        ]);

    }

    /**
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @return JsonResponse
     * @Route("/chick-type", name="chick_type")
     */
    public function chickType(Request $request, ParameterBagInterface $parameterBag)
    {
        set_time_limit(0);
        ignore_user_abort(true);
        if ($request->getMethod() == 'GET' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {
            $chickTypes = $this->getDoctrine()->getRepository(Setting::class)->findBy(array('settingType' => 'CHICK_TYPE', 'status' => 1));

            $data = [];
            foreach ($chickTypes as $chickType) {
                $data[] = [
                    'id' => $chickType->getId(),
                    'name' => $chickType->getName(),
                ];
            }
            return new JsonResponse($data);
        }
        return new JsonResponse([
            'status' => 404,
            'message' => 'Not Found!'
        ]);

    }

    /**
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @return JsonResponse
     * @Route("/poultry-meat-egg-type", name="poultry_meat_egg_type")
     */
    public function poultryMeatEggType(Request $request, ParameterBagInterface $parameterBag)
    {
        set_time_limit(0);
        ignore_user_abort(true);
        if ($request->getMethod() == 'GET' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {
            $breedTypes = $this->getDoctrine()->getRepository(Setting::class)->findBy(['settingType' => 'MEAT_EGG_TYPE', 'status' => 1]);

            $data = [];
            foreach ($breedTypes as $breedType) {
                $data[] = [
                    'id' => $breedType->getId(),
                    'name' => $breedType->getName(),
                ];
            }
            return new JsonResponse($data);
        }
        return new JsonResponse([
            'status' => 404,
            'message' => 'Not Found!'
        ]);

    }

    /**
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @return JsonResponse
     * @Route("/complain-type", name="complain_type")
     */
    public function complainType(Request $request, ParameterBagInterface $parameterBag)
    {
        set_time_limit(0);
        ignore_user_abort(true);
        if ($request->getMethod() == 'GET' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {
            $types = $this->getDoctrine()->getRepository(Setting::class)->findBy(['settingType' => 'COMPLAIN_TYPE', 'status' => 1]);

            $data = [];
            foreach ($types as $type) {
                $data[] = [
                    'id' => $type->getId(),
                    'name' => $type->getName(),
                ];
            }
            return new JsonResponse($data);
        }
        return new JsonResponse([
            'status' => 404,
            'message' => 'Not Found!'
        ]);

    }


    /**
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @return JsonResponse
     * @Route("/fcr-hatchery-company", name="fcr_hatchery_company")
     */
    public function fcrHatcheryCompany(Request $request, ParameterBagInterface $parameterBag)
    {
        set_time_limit(0);
        ignore_user_abort(true);
        if ($request->getMethod() == 'GET' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {
            $fcrCompanies = $this->getDoctrine()->getRepository(Api::class)->fcrHatcheryCompany();

            return new JsonResponse($fcrCompanies);
        }
        return new JsonResponse([
            'status' => 404,
            'message' => 'Not Found!'
        ]);

    }

    /**
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @return JsonResponse
     * @Route("/visit-working-mode", name="visit_working_mode")
     */
    public function visitWorkingMode(Request $request, ParameterBagInterface $parameterBag)
    {
        set_time_limit(0);
        ignore_user_abort(true);
        if ($request->getMethod() == 'GET' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {
            $workingModes = $this->getDoctrine()->getRepository(Setting::class)->findBy(['status' => 1, 'settingType' => 'WORKING_MODE'],['sortOrder' => 'ASC']);

            $data = [];
            foreach ($workingModes as $workingMode) {
                $data[] = [
                    'id' => $workingMode->getId(),
                    'name' => $workingMode->getName(),
                ];
            }
            return new JsonResponse($data);
        }
        return new JsonResponse([
            'status' => 404,
            'message' => 'Not Found!'
        ]);

    }

    /**
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @return JsonResponse
     * @Route("/create-farmer", name="create_farmer")
     */
    public function createFarmer(Request $request, ParameterBagInterface $parameterBag)
    {
        set_time_limit(0);
        ignore_user_abort(true);
        if ($request->getMethod() == 'POST' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {
            $parameters = $request->request->all();

            $agent = null;
            $subAgent = null;
            $otherAgent = null;
            $location = null;

            if (isset($parameters['agentId']) && !empty($parameters['agentId'])) {
                $agent = $this->getDoctrine()->getRepository(Agent::class)->find($parameters['agentId']);
            }
            if (isset($parameters['subAgentId']) && !empty($parameters['subAgentId'])) {
                $subAgent = $this->getDoctrine()->getRepository(Agent::class)->find($parameters['subAgentId']);
            }
            if (isset($parameters['otherAgentId']) && !empty($parameters['otherAgentId'])) {
                $otherAgent = $this->getDoctrine()->getRepository(Agent::class)->find($parameters['otherAgentId']);
            }
            if (isset($parameters['locationId']) && !empty($parameters['locationId'])) {
                $location = $this->getDoctrine()->getRepository(Location::class)->find($parameters['locationId']);
            }
            if (
                (isset($parameters['name']) && !empty($parameters['name'])) &&
                (isset($parameters['mobile']) && !empty($parameters['mobile'])) &&
                ($agent || $subAgent || $otherAgent) &&
                (isset($parameters['feedId']) && !empty($parameters['feedId'])) &&
                (isset($parameters['farmerType']) && !empty($parameters['farmerType'])) &&
                (isset($parameters['employeeId']) && !empty($parameters['employeeId']))
            ) {

                $farmerType = $this->getDoctrine()->getRepository(Setting::class)->find($parameters['farmerType']);
                $feed = $this->getDoctrine()->getRepository(Setting::class)->find($parameters['feedId']);
                $employee = $this->getDoctrine()->getRepository(User::class)->find($parameters['employeeId']);
                $farmerGroup = $this->getDoctrine()->getRepository(Setting::class)->findOneBy(['slug' => 'farmer']);

//                duplicate check
                $existingFarmerCheck = $this->getDoctrine()->getRepository(CrmCustomer::class)->duplicateCustomerCheckByMobileAndType($parameters['mobile'], $parameters['farmerType']);

                if ($existingFarmerCheck) {
                    return new JsonResponse(['statusCode' => '409', 'message' => 'This Farmar Already Exist.']);
                } else {
                    // Add new Farmer
                    $newFarmer = new CrmCustomer();
                    $newFarmer->setName($parameters['name']);
                    $newFarmer->setMobile($parameters['mobile']);
                    $newFarmer->setAddress(isset($parameters['address']) ? $parameters['address'] : null);
                    $newFarmer->setAgent($agent ?: ($subAgent ?: ($otherAgent ?: null)));
                    $newFarmer->setOtherAgent($otherAgent);
                    $newFarmer->setLocation($location);
                    $newFarmer->setCustomerGroup($farmerGroup);
                    $newFarmer->setCreated(new \DateTime('now'));
                    $this->getDoctrine()->getManager()->persist($newFarmer);
                    $this->getDoctrine()->getManager()->flush();

                    $message = 'Customer is created successfully.';

                    // Introduce new Farmer
                    $introduceFarmer = new FarmerIntroduceDetails();
                    $introduceFarmer->setAgent($agent ?: ($subAgent ?: ($otherAgent ?: null)));
                    $introduceFarmer->setCustomer($newFarmer);
                    $introduceFarmer->setSubAgent($subAgent);
                    $introduceFarmer->setEmployee($employee);
                    $introduceFarmer->setOtherAgent($otherAgent);
                    $introduceFarmer->setFarmerType($farmerType);
                    if ($feed->getName() != 'Nourish') {
                        $introduceFarmer->setOtherFeed($feed);
                    }
                    $introduceFarmer->setFeed($feed);
                    $introduceFarmer->setCultureSpeciesItemAndQty(isset($parameters['cultureSpeciesItemAndQty']) ? $parameters['cultureSpeciesItemAndQty'] : null);
                    $introduceFarmer->setCreatedAt(new \DateTime('now'));

                    if ((isset($parameters['agentId']) && !empty($parameters['agentId'])) && ($feed && $feed->getName() == 'Nourish')) {
                        $introduceFarmer->setIntroduceDate(new \DateTime('now'));
                        $message = 'Customer is created and introduced successfully.';
                    } elseif ((isset($parameters['subAgentId']) && !empty($parameters['subAgentId'])) && ($feed && $feed->getName() == 'Nourish')) {
                        $introduceFarmer->setIntroduceDate(new \DateTime('now'));
                        $message = 'Customer is created and introduced successfully.';
                    } else {
                        $introduceFarmer->setIntroduceDate(null);
                    }

                    $this->getDoctrine()->getManager()->persist($introduceFarmer);
                    $this->getDoctrine()->getManager()->flush();

                    return new JsonResponse([
                        'statusCode' => 201,
                        'message' => $message
                    ]);
                }
            } else {
                return new JsonResponse([
                    'statusCode' => 422,
                    'message' => 'invalid data format'
                ]);
            }
        }
        return new JsonResponse([
            'status' => 500,
            'message' => 'Server Error!'
        ]);

    }


    /**
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @return JsonResponse
     * @Route("/training-material", name="training_material")
     */
    public function trainingMaterial(Request $request, ParameterBagInterface $parameterBag)
    {
        set_time_limit(0);
        ignore_user_abort(true);
        if ($request->getMethod() == 'POST' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {

            $user = $this->getDoctrine()->getRepository(User::class)->find($request->request->get('user_id'));

            if ($user) {
                if ($user->getServiceMode() && $user->getServiceMode()->getSlug() != 'sales-marketing') {
                    $serviceModeExplode = explode('-', $user->getServiceMode()->getSlug());
                    $lastElement = end($serviceModeExplode);
                    $breedName = $this->getDoctrine()->getRepository(Setting::class)->findOneBy(['settingType' => 'BREED_NAME', 'slug' => $lastElement . '-breed', 'status' => 1]);
                    $materials = $this->getDoctrine()->getRepository(Setting::class)->findBy(['settingType' => 'TRAINING_MATERIAL', 'parent' => $breedName]);
                    $data = [];
                    foreach ($materials as $material) {
                        $data[] = [
                            'id' => $material->getId(),
                            'name' => $material->getName(),
                        ];
                    }
                    return new JsonResponse($data);
                } else {
                    return new JsonResponse([
                        'status' => 404,
                        'message' => 'Not Found!'
                    ]);
                }

            } else {
                return new JsonResponse([
                    'status' => 404,
                    'message' => 'Not Found!'
                ]);
            }
        }
        return new JsonResponse([
            'status' => 404,
            'message' => 'Not Found!'
        ]);

    }

    /**
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @return JsonResponse
     * @Route("/breed-name", name="breed_name")
     */
    public function breedName(Request $request, ParameterBagInterface $parameterBag)
    {
        set_time_limit(0);
        ignore_user_abort(true);
        if ($request->getMethod() == 'GET' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {
            $breedNames = $this->getDoctrine()->getRepository(Setting::class)->findBy(['settingType' => 'BREED_NAME', 'status' => 1]);

            $data = [];
            foreach ($breedNames as $breedName) {
                $data[] = [
                    'id' => $breedName->getId(),
                    'name' => $breedName->getName(),
                ];
            }
            return new JsonResponse($data);
        }
        return new JsonResponse([
            'status' => 404,
            'message' => 'Not Found!'
        ]);

    }


    /**
     * @Route("/agent-purpose-for-report", name="agent_purpose_for_report_api")
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @return JsonResponse|Response
     */
    public function agentPurposeForReportApi(Request $request, ParameterBagInterface $parameterBag)
    {
        set_time_limit(0);
        ignore_user_abort(true);

        if ($request->getMethod() == 'GET' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {
            $entities = $this->getDoctrine()->getRepository(Api::class)->agentPurposeForReport();

            $response = new Response();
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent(json_encode($entities));
            $response->setStatusCode(Response::HTTP_OK);
            return $response;
        }
        return new JsonResponse([
            'status' => 404,
            'message' => 'Not Found!'
        ]);

    }


    /**
     * @Route("/selectFarmTypeForMarketing", methods={"GET"}, name="selectFarmTypeForMarketing")
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @return JsonResponse|Response
     */
    public function selectFarmTypeForMarketing(Request $request, ParameterBagInterface $parameterBag)
    {

        set_time_limit(0);
        ignore_user_abort(true);
        if ($request->getMethod() == 'GET' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {

            $entities = $this->getDoctrine()->getRepository(Api::class)->selectFarmTypeForMarketing();
            $response = new Response();
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent(json_encode($entities));
            $response->setStatusCode(Response::HTTP_OK);
            return $response;
        }
        return new JsonResponse([
            'status' => 404,
            'message' => 'Not Found!'
        ]);
    }


    /**
     * @Route("/farmSelectReportForMarketing", methods={"GET"}, name="farmSelectReportMarketing")
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @return JsonResponse|Response
     */
    public function farmSelectReportForMarketing(Request $request, ParameterBagInterface $parameterBag)
    {
        set_time_limit(0);
        ignore_user_abort(true);
        if ($request->getMethod() == 'GET' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {

            $entities = $this->getDoctrine()->getRepository(Api::class)->farmSelectReportForMarketing();

            $response = new Response();
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent(json_encode($entities));
            $response->setStatusCode(Response::HTTP_OK);
            return $response;
        }
        return new JsonResponse([
            'status' => 404,
            'message' => 'Not Found!'
        ]);
    }


    /**
     * @Route("/productName", methods={"GET"}, name="productName")
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @return Response
     */
    public function productName(Request $request, ParameterBagInterface $parameterBag)
    {
        set_time_limit(0);
        ignore_user_abort(true);
        if ($request->getMethod() == 'GET' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {
            $entities = $this->getDoctrine()->getRepository(Api::class)->productName();
            $response = new Response();
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent(json_encode($entities));
            $response->setStatusCode(Response::HTTP_OK);
            return $response;
        }
        return new JsonResponse([
            'status' => 404,
            'message' => 'Not Found!'
        ]);
    }

    /**
     * @Route("/challengeName", methods={"GET"}, name="challengeName")
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @return Response
     */
    public function challengeName(Request $request, ParameterBagInterface $parameterBag)
    {
        set_time_limit(0);
        ignore_user_abort(true);
        if ($request->getMethod() == 'GET' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {
            $entities = $this->getDoctrine()->getRepository(Api::class)->challengeName();
            $response = new Response();
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent(json_encode($entities));
            $response->setStatusCode(Response::HTTP_OK);
            return $response;
        }
        return new JsonResponse([
            'status' => 404,
            'message' => 'Not Found!'
        ]);
    }

    /**
     * @Route("/area", methods={"GET"}, name="area")
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @return Response
     */
    public function area(Request $request, ParameterBagInterface $parameterBag)
    {
        set_time_limit(0);
        ignore_user_abort(true);
        if ($request->getMethod() == 'GET' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {
            $entities = $this->getDoctrine()->getRepository(Api::class)->area();
            $response = new Response();
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent(json_encode($entities));
            $response->setStatusCode(Response::HTTP_OK);
            return $response;
        }
        return new JsonResponse([
            'status' => 404,
            'message' => 'Not Found!'
        ]);
    }

    /**
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @return JsonResponse
     * @Route("/fcr-different-feed-company-for-broiler", name="fcr_different_feed_company_for_broiler")
     */
    public function fcrDifferentFeedCompanyForBroiler(Request $request, ParameterBagInterface $parameterBag)
    {
        set_time_limit(0);
        ignore_user_abort(true);
        if ($request->getMethod() == 'GET' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {
            $fcrCompanies = $this->getDoctrine()->getRepository(Api::class)->fcrDifferentFeedCompanyForBroiler();

            return new JsonResponse($fcrCompanies);
        }
        return new JsonResponse([
            'status' => 404,
            'message' => 'Not Found!'
        ]);

    }

    /**
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @return JsonResponse
     * @Route("/fcr-different-feed-company-for-sonali", name="fcr_different_feed_company_for_sonali")
     */
    public function fcrDifferentFeedCompanyForSonali(Request $request, ParameterBagInterface $parameterBag)
    {
        set_time_limit(0);
        ignore_user_abort(true);
        if ($request->getMethod() == 'GET' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {
            $fcrCompanies = $this->getDoctrine()->getRepository(Api::class)->fcrDifferentFeedCompanyForSonali();

            return new JsonResponse($fcrCompanies);
        }
        return new JsonResponse([
            'status' => 404,
            'message' => 'Not Found!'
        ]);

    }


    /**
     * @Route("/fish-life-cycle-culture-in-progress", name="fish_life_cycle_culture_in_progress")
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @return JsonResponse|Response
     */
    public function fishLifeCycleCultureInProgress(Request $request, ParameterBagInterface $parameterBag)
    {
        set_time_limit(0);
        ignore_user_abort(true);
        if ($request->getMethod() == 'POST' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {
            $parameters = $request->request->all();
            $arrayData = [];
            if (isset($parameters['employee_id']) && $parameters['employee_id'] != "") {

                $arrayData = $this->getFishLifeCycleDataByEmployeeId($parameters['employee_id']);

            }

            $response = new Response();
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent(json_encode($arrayData));
            $response->setStatusCode(Response::HTTP_OK);
            return $response;
        }
        return new JsonResponse([
            'status' => 404,
            'message' => 'Not Found!'
        ]);


    }

    private function getFishLifeCycleDataByEmployeeId($employeeId)
    {
        $arrayData = [];
        if ($employeeId) {
            $employee = $this->getDoctrine()->getRepository(User::class)->find($employeeId);

            $entities = $this->getDoctrine()->getRepository(FishLifeCycleCulture::class)->findBy(['employee' => $employee, 'status' => 'IN_PROGRESS'], ['id' => 'ASC']);

            if ($entities) {
                /** @var FishLifeCycleCulture $entity */
                foreach ($entities as $entity) {
                    $detailData = [];
                    if ($entity->getFishLifeCycleCultureDetails()) {
                        /* @var FishLifeCycleCultureDetails $fishLifeCycleCultureDetail */
                        foreach ($entity->getFishLifeCycleCultureDetails() as $fishLifeCycleCultureDetail) {
                            $detailData[] = [
                                "id" => $fishLifeCycleCultureDetail->getAppId() ? (int)$fishLifeCycleCultureDetail->getAppId() : "",
                                "sampling_date" => $fishLifeCycleCultureDetail->getSamplingDate()->format('Y-m-d'),
                                "avg_present_weight_gm" => $fishLifeCycleCultureDetail->getAveragePresentWeight() ? (string)$fishLifeCycleCultureDetail->getAveragePresentWeight() : "0",
                                "cur_survival_rate" => $fishLifeCycleCultureDetail->getSrPercentage() ? (string)$fishLifeCycleCultureDetail->getSrPercentage() : "0",
                                "cur_feed_consumption" => (string)$fishLifeCycleCultureDetail->getCurrentFeedConsumptionKg(),
                                "farmer_comment" => (string)$fishLifeCycleCultureDetail->getFarmerRemarks() ? $fishLifeCycleCultureDetail->getFarmerRemarks() : "",
                                "visitor_comment" => (string)$fishLifeCycleCultureDetail->getEmployeeRemarks() ? $fishLifeCycleCultureDetail->getEmployeeRemarks() : "",
                                "pond_number" => $entity->getPondNumber() ? (string)$entity->getPondNumber() : (string)1,
                                "cur_fcr" => (string)$fishLifeCycleCultureDetail->getCurrentFcr(),
                                "web_life_cycle_details_id" => (string)$fishLifeCycleCultureDetail->getId(),
                                "created_at" => $fishLifeCycleCultureDetail->getCreatedAt()->format('Y-m-d H:i:s')
                            ];
                        }
                    }
                    $customerGroup = $entity->getCustomer()->getFarmerIntroduce() && $entity->getCustomer()->getFarmerIntroduce()->getFarmerType() ? '(' . $entity->getCustomer()->getFarmerIntroduce()->getFarmerType()->getName() . ')' : null;
                    $arrayData[] = [
                        "id" => $entity->getAppId(),
                        "report_id" => (string)$entity->getAppReportId(),
                        "report_type_id" => $entity->getReport() ? (string)$entity->getReport()->getId() : "",
                        "agent_id" => $entity->getAgent() ? (string)$entity->getAgent()->getId() : "",
                        "agent_name" => $entity->getAgent() ? $entity->getAgent()->getName() : "",
                        "customer_id" => $entity->getCustomer() ? (string)$entity->getCustomer()->getId() : "",
                        "customer_name" => $entity->getCustomer() ? $entity->getCustomer()->getName() . ' ' . $customerGroup : "",
                        "pond_number" => (string)$entity->getPondNumber(),
                        "reporting_date" => $entity->getCreatedAt()->format('Y-m-d H:i:s'),
                        "address" => $entity->getAgent() && $entity->getAgent()->getUpozila() ? $entity->getAgent()->getUpozila()->getName() : "",
                        "feed_company_id" => $entity->getFeed() ? (string)$entity->getFeed()->getId() : "",
                        "feed_company_name" => $entity->getFeed() ? (string)$entity->getFeed()->getName() : "",
                        "hatchery_id" => $entity->getHatchery() ? (string)$entity->getHatchery()->getId() : "",
                        "hatchery_name" => $entity->getHatchery() ? $entity->getHatchery()->getName() : "",
                        "feed_item_name" => $entity->getFeedItemName() ? $entity->getFeedItemName() : "",
                        "culture_species_main_id" => $entity->getMainCultureSpecies() ? (string)$entity->getMainCultureSpecies()->getId() : "",
                        "culture_species_main_name" => $entity->getMainCultureSpecies() ? (string)$entity->getMainCultureSpecies()->getName() : "",
                        "culture_species_optional_id" => $entity->getOtherCultureSpecies() ? (string)$entity->getOtherCultureSpecies()->getId() : "",
                        "culture_species_optional_name" => $entity->getOtherCultureSpecies() ? $entity->getOtherCultureSpecies()->getName() : "",
                        "feed_type_id" => $entity->getFeedType() ? (string)$entity->getFeedType()->getId() : "",
                        "feed_type_name" => $entity->getFeedType() ? $entity->getFeedType()->getName() : "",
                        "stocking_date" => $entity->getStockingDate()->format('Y-m-d'),
                        "culture_area_decimal" => $entity->getCultureAreaDecimal() ? (string)$entity->getCultureAreaDecimal() : "0",
                        "no_of_initial_fish_pcs" => $entity->getNoOfInitialFish() ? (string)$entity->getNoOfInitialFish() : "0",
                        "density_per_decimal_pcs" => $entity->getStockingDensity() ? (string)$entity->getStockingDensity() : "0",
                        "avg_initial_weight_gm" => $entity->getAverageInitialWeightGm() ? (string)$entity->getAverageInitialWeightGm() : "0",
                        "total_initial_weight_kg" => $entity->getTotalInitialWeightKg() ? (string)$entity->getTotalInitialWeightKg() : "0",
                        "culture_species_desc" => $entity->getSpeciesDescription() ? (string)$entity->getSpeciesDescription() : "",
                        "life_cycle_details" => sizeof($detailData) > 0 ? json_encode($detailData) : "",
                        "status" => $entity->getStatus(),
                        "feed_item_name_other" => $entity->getFeedItemNameOther() ? $entity->getFeedItemNameOther() : "",
                        "web_life_cycle_id" => (string)$entity->getId(),
                        "employee_id" => $entity->getEmployee() ? (string)$entity->getEmployee()->getId() : "",
                        "report_type_name" => $entity->getReport() ? $entity->getReport()->getName() : null,
                    ];
                }
            }
        }

        return $arrayData;
    }

    private function getFishLifeCycleNursingDataByEmployeeId($employeeId)
    {
        $arrayData = [];
        if ($employeeId) {
            $employee = $this->getDoctrine()->getRepository(User::class)->find($employeeId);

            $entities = $this->getDoctrine()->getRepository(FishLifeCycleNursing::class)->findBy(['employee' => $employee, 'status' => 'IN_PROGRESS'], ['id' => 'ASC']);

            if ($entities) {
                /** @var FishLifeCycleNursing $entity */
                foreach ($entities as $entity) {
                    $detailData = [];
                    if ($entity->getFishLifeCycleNursingDetails()) {
                        /* @var FishLifeCycleCultureDetails $fishLifeCycleNursingDetail */
                        foreach ($entity->getFishLifeCycleNursingDetails() as $fishLifeCycleNursingDetail) {
                            $detailData[] = [
                                "id" => $fishLifeCycleNursingDetail->getAppId() ? (int)$fishLifeCycleNursingDetail->getAppId() : "",
                                "sampling_date" => $fishLifeCycleNursingDetail->getSamplingDate()->format('Y-m-d'),
                                "avg_present_weight_gm" => $fishLifeCycleNursingDetail->getAveragePresentWeight() ? (string)$fishLifeCycleNursingDetail->getAveragePresentWeight() : "0",
                                "cur_survival_rate" => $fishLifeCycleNursingDetail->getSrPercentage() ? (string)$fishLifeCycleNursingDetail->getSrPercentage() : "0",
                                "cur_feed_consumption" => (string)$fishLifeCycleNursingDetail->getCurrentFeedConsumptionKg(),
                                "farmer_comment" => (string)$fishLifeCycleNursingDetail->getFarmerRemarks() ? $fishLifeCycleNursingDetail->getFarmerRemarks() : "",
                                "visitor_comment" => (string)$fishLifeCycleNursingDetail->getEmployeeRemarks() ? $fishLifeCycleNursingDetail->getEmployeeRemarks() : "",
                                "pond_number" => $entity->getPondNumber() ? (string)$entity->getPondNumber() : (string)1,
                                "cur_fcr" => (string)$fishLifeCycleNursingDetail->getCurrentFcr(),
                                "web_life_cycle_details_id" => (string)$fishLifeCycleNursingDetail->getId(),
                                "created_at" => $fishLifeCycleNursingDetail->getCreatedAt()->format('Y-m-d H:i:s')
                            ];
                        }
                    }
                    $customerGroup = $entity->getCustomer()->getFarmerIntroduce() && $entity->getCustomer()->getFarmerIntroduce()->getFarmerType() ? '(' . $entity->getCustomer()->getFarmerIntroduce()->getFarmerType()->getName() . ')' : null;
                    $arrayData[] = [
                        "id" => $entity->getAppId(),
                        "report_id" => (string)$entity->getAppReportId(),
                        "report_type_id" => $entity->getReport() ? (string)$entity->getReport()->getId() : "",
                        "agent_id" => $entity->getAgent() ? (string)$entity->getAgent()->getId() : "",
                        "agent_name" => $entity->getAgent() ? $entity->getAgent()->getName() : "",
                        "customer_id" => $entity->getCustomer() ? (string)$entity->getCustomer()->getId() : "",
                        "customer_name" => $entity->getCustomer() ? $entity->getCustomer()->getName() . ' ' . $customerGroup : "",
                        "pond_number" => (string)$entity->getPondNumber(),
                        "reporting_date" => $entity->getCreatedAt()->format('Y-m-d H:i:s'),
                        "address" => $entity->getAgent() && $entity->getAgent()->getUpozila() ? $entity->getAgent()->getUpozila()->getName() : "",
                        "feed_company_id" => $entity->getFeed() ? (string)$entity->getFeed()->getId() : "",
                        "feed_company_name" => $entity->getFeed() ? (string)$entity->getFeed()->getName() : "",
                        "hatchery_id" => $entity->getHatchery() ? (string)$entity->getHatchery()->getId() : "",
                        "hatchery_name" => $entity->getHatchery() ? $entity->getHatchery()->getName() : "",
                        "feed_item_name" => $entity->getFeedItemName() ? $entity->getFeedItemName() : "",
                        "nursing_species_main_id" => $entity->getMainCultureSpecies() ? (string)$entity->getMainCultureSpecies()->getId() : "",
                        "nursing_species_main_name" => $entity->getMainCultureSpecies() ? (string)$entity->getMainCultureSpecies()->getName() : "",
                        "nursing_species_optional_id" => $entity->getOtherCultureSpecies() ? (string)$entity->getOtherCultureSpecies()->getId() : "",
                        "nursing_species_optional_name" => $entity->getOtherCultureSpecies() ? $entity->getOtherCultureSpecies()->getName() : "",
                        "feed_type_id" => $entity->getFeedType() ? (string)$entity->getFeedType()->getId() : "",
                        "feed_type_name" => $entity->getFeedType() ? $entity->getFeedType()->getName() : "",
                        "stocking_date" => $entity->getStockingDate()->format('Y-m-d'),
                        "nursing_area_decimal" => $entity->getCultureAreaDecimal() ? (string)$entity->getCultureAreaDecimal() : "0",
                        "no_of_initial_fish_pcs" => $entity->getNoOfInitialFish() ? (string)$entity->getNoOfInitialFish() : "0",
                        "density_per_decimal_pcs" => $entity->getStockingDensity() ? (string)$entity->getStockingDensity() : "0",
                        "avg_initial_weight_gm" => $entity->getAverageInitialWeightGm() ? (string)$entity->getAverageInitialWeightGm() : "0",
                        "total_initial_weight_kg" => $entity->getTotalInitialWeightKg() ? (string)$entity->getTotalInitialWeightKg() : "0",
                        "nursing_species_desc" => $entity->getSpeciesDescription() ? (string)$entity->getSpeciesDescription() : "",
                        "life_cycle_details" => sizeof($detailData) > 0 ? json_encode($detailData) : "",
                        "status" => $entity->getStatus(),
                        "feed_item_name_other" => $entity->getFeedItemNameOther() ? $entity->getFeedItemNameOther() : "",
                        "web_life_cycle_id" => (string)$entity->getId(),
                        "employee_id" => $entity->getEmployee() ? (string)$entity->getEmployee()->getId() : "",
                        "report_type_name" => $entity->getReport() ? $entity->getReport()->getName() : null,
                    ];
                }
            }
        }

        return $arrayData;
    }


    /**
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @return JsonResponse
     * @Route("/fcr-different-company-data", name="fcr_different_feed_company_data")
     */
    public function fcrDifferentFeedCompanyData(Request $request, ParameterBagInterface $parameterBag)
    {
        set_time_limit(0);
        ignore_user_abort(true);

        if ($request->getMethod() == 'POST' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {
            $parameters = $request->request->all();
            $fcrCompanies = [];
            if (isset($parameters['employee_id']) && $parameters['employee_id'] != "") {
                $employeeId = $parameters['employee_id'];
                $year = isset($parameters['year']) && $parameters['year'] != "" ? $parameters['year'] : date('Y');
                $fcrCompanies = $this->getDoctrine()->getRepository(FcrDifferentCompanies::class)->getFcrDifferentCompaniesApi($employeeId, $year);
            }

            $response = new Response();
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent(json_encode($fcrCompanies));
            $response->setStatusCode(Response::HTTP_OK);
            return $response;
        }
        return new JsonResponse([
            'status' => 404,
            'message' => 'Not Found!'
        ]);

    }


    /**
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @return JsonResponse
     * @Route("/lab-service-data", name="lab_service_data")
     */
    public function labServiceDataForApiImport(Request $request, ParameterBagInterface $parameterBag)
    {
        set_time_limit(0);
        ignore_user_abort(true);

        if ($request->getMethod() == 'POST' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {
            $parameters = $request->request->all();
            $labServicesData = [];
            if (isset($parameters['employee_id']) && $parameters['employee_id'] != "") {
                $employeeId = $parameters['employee_id'];
                $year = isset($parameters['year']) && $parameters['year'] != "" ? $parameters['year'] : date('Y');
                $labServicesData = $this->getDoctrine()->getRepository(LabService::class)->getLabServicesDataForApiImport($employeeId, $year);
            }

            $response = new Response();
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent(json_encode($labServicesData));
            $response->setStatusCode(Response::HTTP_OK);
            return $response;
        }
        return new JsonResponse([
            'status' => 404,
            'message' => 'Not Found!'
        ]);

    }


    /**
     * @Route("/cattle-life-cycle-in-progress", name="cattle_life_cycle_in_progress")
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @return JsonResponse|Response
     */
    public function cattleLifeCycleInProgress(Request $request, ParameterBagInterface $parameterBag)
    {
        set_time_limit(0);
        ignore_user_abort(true);
        if ($request->getMethod() == 'POST' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {
            $parameters = $request->request->all();
            $arrayData = [];
            if (isset($parameters['employee_id']) && $parameters['employee_id'] != "") {

                $arrayData = $this->getCattleLifeCycleInprogressData($parameters['employee_id']);

            }

            $response = new Response();
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent(json_encode($arrayData));
            $response->setStatusCode(Response::HTTP_OK);
            return $response;
        }
        return new JsonResponse([
            'status' => 404,
            'message' => 'Not Found!'
        ]);


    }

    private function getCattleLifeCycleInprogressData($employeeId)
    {

        $arrayData = [];

        if ($employeeId) {
            $employee = $this->getDoctrine()->getRepository(User::class)->find($employeeId);

            $entities = $this->getDoctrine()->getRepository(CattleLifeCycle::class)->findBy(['employee' => $employee, 'lifeCycleState' => 'IN_PROGRESS'], ['id' => 'ASC']);

            if ($entities) {
                /** @var CattleLifeCycle $entity */
                foreach ($entities as $entity) {
                    $arrayData[] = [
                        "id" => $entity->getAppId() ? (int)$entity->getAppId() : $entity->getId(),
                        "customer_id" => $entity->getCustomer() ? $entity->getCustomer()->getId() : null,
                        "report_id" => $entity->getReport() ? $entity->getReport()->getId() : null,
                        "agent_id" => $entity->getAgent() ? $entity->getAgent()->getId() : null,
                        "employee_id" => $entity->getEmployee() ? $entity->getEmployee()->getId() : null,
                        "reporting_date" => $entity->getReportingDate() ? (string)$entity->getReportingDate()->format('Y-m-d') : "",
                        "breed_type" => $entity->getBreedType() ? $entity->getBreedType()->getId() : null,
                        "life_cycle_state" => $entity->getLifeCycleState() ? $entity->getLifeCycleState() : "",
                        "remarks" => $entity->getRemarks() ? $entity->getRemarks() : "",
                        "created_at" => $entity->getCreatedAt() ? $entity->getCreatedAt()->format('Y-m-d H:i:s') : null,
                        "feed_type" => $entity->getFeedType() ? $entity->getFeedType()->getId() : null,
                        "feedName" => $entity->getFeedType() ? $entity->getFeedType()->getName() : null,
                        "breedName" => $entity->getBreedType() ? $entity->getBreedType()->getName() : null,
                        "crm_visit_id" => null,
                        "is_sync" => 1,
                        "visit_details_id" => null,
                        "agent_name" => $entity->getAgent() ? $entity->getAgent()->getName() : "",
                        "customer_name" => $entity->getCustomer() ? $entity->getCustomer()->getName() : "",
                        "address" => $entity->getAgent() && $entity->getAgent()->getUpozila() ? $entity->getAgent()->getUpozila()->getName() : "",
                        "web_life_cycle_id" => $entity->getId(),
                        "farm_number" => $entity->getFarmNumber() ? $entity->getFarmNumber() : 1,
                        "report_type_name" => $entity->getReport() ? $entity->getReport()->getName() : null,
                        "feed_company_id" => $entity->getFeed() ? $entity->getFeed()->getId() : null,
                        "feed_company_name" => $entity->getFeed() ? $entity->getFeed()->getName() : null,
                        "feed_type_id" => $entity->getFeedType() ? $entity->getFeedType()->getId() : null,
                        "feed_type_name" => $entity->getFeedType() ? $entity->getFeedType()->getName() : null,
                        "feed_mill_id" => $entity->getFeedMill() ? $entity->getFeedMill()->getId() : null,
                        "feed_mill_name" => $entity->getFeedMill() ? $entity->getFeedMill()->getName() : null,
                        "production_date" => $entity->getProductionDate() ? (string)$entity->getProductionDate()->format('Y-m-d') : "",
                        "batch_no" => $entity->getBatchNo() ? $entity->getBatchNo() : ""
                    ];
                }
            }
        }

        return $arrayData;
    }

    /**
     * @Route("/cattle-life-cycle-details-in-progress", name="cattle_life_cycle_details_in_progress")
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @return JsonResponse|Response
     */
    public function cattleLifeCycleDetailsInProgress(Request $request, ParameterBagInterface $parameterBag)
    {
        set_time_limit(0);
        ignore_user_abort(true);
        if ($request->getMethod() == 'POST' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {
            $parameters = $request->request->all();
            $arrayData = [];
            if (isset($parameters['employee_id']) && $parameters['employee_id'] != "") {

                $arrayData = $this->getCattleLifeCycleDetailsInprogressData($parameters['employee_id']);

            }

            $response = new Response();
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent(json_encode($arrayData));
            $response->setStatusCode(Response::HTTP_OK);
            return $response;

        }
        return new JsonResponse([
            'status' => 404,
            'message' => 'Not Found!'
        ]);


    }

    private function getCattleLifeCycleDetailsInprogressData($employeeId)
    {
        $arrayData = [];
        if ($employeeId) {
            $employee = $this->getDoctrine()->getRepository(User::class)->find($employeeId);

            $entities = $this->getDoctrine()->getRepository(CattleLifeCycle::class)->findBy(['employee' => $employee, 'lifeCycleState' => 'IN_PROGRESS'], ['id' => 'ASC']);

            if ($entities) {
                /** @var CattleLifeCycle $entity */
                foreach ($entities as $entity) {
                    if ($entity->getCrmCattleLifeCycleDetails()) {
                        /* @var CattleLifeCycleDetails $lifeCycleDetail */
                        foreach ($entity->getCrmCattleLifeCycleDetails() as $lifeCycleDetail) {
                            $arrayData[] = [
                                "id" => $lifeCycleDetail->getAppId() ? (int)$lifeCycleDetail->getAppId() : $lifeCycleDetail->getId(),
                                "crm_cattle_life_cycle_id" => $entity->getAppId() ? (int)$entity->getAppId() : $entity->getId(),
                                "visiting_date" => $lifeCycleDetail->getVisitingDate() ? $lifeCycleDetail->getVisitingDate()->format('Y-m-d') : "",
                                "age_of_cattle_month" => $lifeCycleDetail->getAgeOfCattleMonth() ? (string)$lifeCycleDetail->getAgeOfCattleMonth() : "0",
                                "previous_body_weight" => $lifeCycleDetail->getPreviousBodyWeight() ? (string)$lifeCycleDetail->getPreviousBodyWeight() : "0",
                                "present_body_weight" => $lifeCycleDetail->getPresentBodyWeight() ? (string)$lifeCycleDetail->getPresentBodyWeight() : "0",
                                "body_weight_difference" => $lifeCycleDetail->getBodyWeightDifference() ? (string)$lifeCycleDetail->getBodyWeightDifference() : "0",
                                "duration_of_bwt_difference" => $lifeCycleDetail->getDurationOfBwtDifference() ? (string)$lifeCycleDetail->getDurationOfBwtDifference() : "0",
                                "lactation_no" => $lifeCycleDetail->getLactationNo() ? (string)$lifeCycleDetail->getLactationNo() : "0",
                                "age_of_lactation" => $lifeCycleDetail->getAgeOfLactation() ? (string)$lifeCycleDetail->getAgeOfLactation() : "0",
                                "average_weight_per_day" => $lifeCycleDetail->getAverageWeightPerDay() ? (string)$lifeCycleDetail->getAverageWeightPerDay() : "0",
                                "average_weight_per_kg_consumption_feed" => $lifeCycleDetail->getAverageWeightPerKgConsumptionFeed() ? (string)$lifeCycleDetail->getAverageWeightPerKgConsumptionFeed() : "0",
                                "average_weight_per_kg_dm" => $lifeCycleDetail->getAverageWeightPerKgDm() ? (string)$lifeCycleDetail->getAverageWeightPerKgDm() : "0",
                                "milk_fat_percentage" => $lifeCycleDetail->getMilkFatPercentage() ? (string)$lifeCycleDetail->getMilkFatPercentage() : "0",
                                "consumption_feed_intake_ready_feed" => $lifeCycleDetail->getConsumptionFeedIntakeReadyFeed() ? (string)$lifeCycleDetail->getConsumptionFeedIntakeReadyFeed() : "0",
                                "consumption_feed_intake_conventional" => $lifeCycleDetail->getConsumptionFeedIntakeConventional() ? (string)$lifeCycleDetail->getConsumptionFeedIntakeConventional() : "0",
                                "consumption_feed_intake_total" => $lifeCycleDetail->getConsumptionFeedIntakeTotal() ? (string)$lifeCycleDetail->getConsumptionFeedIntakeTotal() : "0",
                                "fodder_green_grass_kg" => $lifeCycleDetail->getFodderGreenGrassKg() ? (string)$lifeCycleDetail->getFodderGreenGrassKg() : "0",
                                "fodder_straw_kg" => $lifeCycleDetail->getFodderStrawKg() ? (string)$lifeCycleDetail->getFodderStrawKg() : "0",
                                "dm_of_fodder_green_grass_kg" => $lifeCycleDetail->getDmOfFodderGreenGrassKg() ? (string)$lifeCycleDetail->getDmOfFodderGreenGrassKg() : "0",
                                "dm_of_fodder_straw_kg" => $lifeCycleDetail->getDmOfFodderStrawKg() ? (string)$lifeCycleDetail->getDmOfFodderStrawKg() : "0",
                                "total_dm_kg" => $lifeCycleDetail->getTotalDmKg() ? (string)$lifeCycleDetail->getTotalDmKg() : "0",
                                "dm_requirement_by_bwt_kg" => $lifeCycleDetail->getDmRequirementByBwtKg() ? (string)$lifeCycleDetail->getDmRequirementByBwtKg() : "0",
                                "remarks" => $lifeCycleDetail->getRemarks() ? (string)$lifeCycleDetail->getRemarks() : "",
                                "created_at" => $lifeCycleDetail->getCreatedAt() ? $lifeCycleDetail->getCreatedAt()->format('Y-m-d H:i:s') : "",
                                "updated_at" => $lifeCycleDetail->getUpdatedAt() ? $lifeCycleDetail->getUpdatedAt()->format('Y-m-d H:i:s') : "",
                                "feedName" => $entity->getFeedType() ? $entity->getFeedType()->getName() : "",
                                "breedName" => $entity->getBreedType() ? $entity->getBreedType()->getName() : "",
                                "crm_visit_id" => null,
                                "is_sync" => 1,
                                "customer_id" => $entity->getCustomer() ? $entity->getCustomer()->getId() : null,
                                "employee_id" => $entity->getEmployee() ? $entity->getEmployee()->getId() : null,
                                "report_id" => $entity->getReport() ? $entity->getReport()->getId() : null,
                                'visit_details_id' => null,
                                'life_cycle_state' => $entity->getLifeCycleState(),
                                "web_life_cycle_id" => $entity->getId(),
                                "farm_number" => $entity->getFarmNumber() ? $entity->getFarmNumber() : 1,
                                "web_life_cycle_details_id" => $lifeCycleDetail->getId(),
                            ];
                        }
                    }
                }
            }
        }

        return $arrayData;
    }


    /**
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @return JsonResponse
     * @Route("/company-wise-feed-sale-data", name="company_wise_feed_sale_data")
     */
    public function companyWiseFeedSaleDataForApiImport(Request $request, ParameterBagInterface $parameterBag)
    {
        set_time_limit(0);
        ignore_user_abort(true);

        if ($request->getMethod() == 'POST' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {
            $parameters = $request->request->all();
            $arrayData = [];
            if (isset($parameters['employee_id']) && $parameters['employee_id'] != "") {
                $employeeId = $parameters['employee_id'];
                $newDate = date('Y-F', strtotime('-1 month'));
                $explodeDate = explode('-', $newDate);
                $year = $explodeDate[0];
                $month = $explodeDate[1];
                $arrayData = $this->getDoctrine()->getRepository(CompanyWiseFeedSale::class)->getCompanyWiseFeedSaleDataForApiImport($employeeId, $year, $month);
            }

            $response = new Response();
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent(json_encode($arrayData));
            $response->setStatusCode(Response::HTTP_OK);
            return $response;
        }
        return new JsonResponse([
            'status' => 404,
            'message' => 'Not Found!'
        ]);

    }


    /**
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @return JsonResponse
     * @Route("/new-farmer-introduce", name="new_farmer_introduce")
     */
    public function newFarmerIntroduce(Request $request, ParameterBagInterface $parameterBag)
    {
        set_time_limit(0);
        ignore_user_abort(true);
        if ($request->getMethod() == 'POST' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {
            $parameters = $request->request->all();

            if (isset($parameters['agent_id']) && $parameters['agent_id'] != "") {

                $findAgent = $this->getDoctrine()->getRepository(Agent::class)->find($parameters['agent_id']);

                if ($findAgent && $findAgent->getAgentGroup() && $findAgent->getAgentGroup()->getSlug() != "other-agent") {

                    $findFarmer = $this->getDoctrine()->getRepository(CrmCustomer::class)->find($parameters['customer_id']);
                    /**
                     * @var FarmerIntroduceDetails $findIntroFarmer
                     */
                    $findIntroFarmer = $this->getDoctrine()->getRepository(FarmerIntroduceDetails::class)->findOneBy(['customer' => $findFarmer]);

                    if ($findIntroFarmer && $findIntroFarmer->getIntroduceDate() === null && $parameters['feed_id'] == 1) {

                        $updateFarmer = "UPDATE `crm_customers` SET `updated`= :updated,`agent_id`= :agent_id WHERE id = :id";
                        $updateFarmerStmt = $this->getDoctrine()->getConnection()->prepare($updateFarmer);
                        $updateFarmerStmt->bindValue('agent_id', $parameters['agent_id']);
                        $updateFarmerStmt->bindValue('id', $parameters['customer_id']);

                        if ($parameters['created_at']) {
                            $updateFarmerStmt->bindValue('updated', (new \DateTime($parameters['created_at']))->format('Y-m-d H:i:s'));
                        } else {
                            $updateFarmerStmt->bindValue('updated', null);
                        }
                        $updateFarmerStmt->execute();

                        $sql = "UPDATE `crm_customer_introduce_details` SET `agent_id`= :agentId,`culture_species_item_and_qty`= :culture_species_item_and_qty,`remarks`= :remarks,`feed_id`= :feed_id,`introduce_date`= :introduce_date,`introduce_by_id`= :introduce_by_id WHERE customer_id = :farmerId";  // every time exits when create new farmer
                        $stmt = $this->getDoctrine()->getConnection()->prepare($sql);
                        $stmt->bindValue('farmerId', $parameters['customer_id']);
                        $stmt->bindValue('agentId', $parameters['agent_id']);
                        $stmt->bindValue('culture_species_item_and_qty', isset($parameters['culture_species_item_and_qty']) && $parameters['culture_species_item_and_qty'] ? $parameters['culture_species_item_and_qty'] : $findIntroFarmer->getCultureSpeciesItemAndQty());
                        $stmt->bindValue('remarks', isset($parameters['remarks']) && $parameters['remarks'] ? $parameters['remarks'] : $findIntroFarmer->getRemarks());
                        $stmt->bindValue('introduce_by_id', $parameters['employee_id']);
                        if (isset($parameters['created_at']) && $parameters['created_at']) {
                            $stmt->bindValue('introduce_date', (new \DateTime($parameters['created_at']))->format('Y-m-d H:i:s'));
                        } else {
                            $stmt->bindValue('introduce_date', (new \DateTime('now'))->format('Y-m-d H:i:s'));
                        }

                        $stmt->bindValue('feed_id', 55);

                        $stmt->execute();

                        return new JsonResponse([
                            'status' => 200,
                            'message' => 'Success'
                        ]);
                    }

                    return new JsonResponse([
                        'status' => 403,
                        'message' => 'This Farmer already introduce.'
                    ]);
                }
                return new JsonResponse([
                    'status' => 403,
                    'message' => 'This Agent is a other agent.'
                ]);
            }
            return new JsonResponse([
                'status' => 404,
                'message' => 'Agent is required.'
            ]);
        }
        return new JsonResponse([
            'status' => 500,
            'message' => 'Server Error!'
        ]);

    }

    /**
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @return JsonResponse
     * @Route("/employee/expense/chart", name="crm_employee_expense_chart")
     */
    public function getEmployeeExpenseChart(Request $request, ParameterBagInterface $parameterBag)
    {
        $arrayData = [];
        set_time_limit(0);
        ignore_user_abort(true);
        if ($request->getMethod() == 'GET' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {

            $parameters = $request->query->all();

            if (isset($parameters['employee_id']) && $parameters['employee_id'] != "") {

                $arrayData = $this->employeeExpenseChart($parameters['employee_id']);

                $response = new Response();
                $response->headers->set('Content-Type', 'application/json');
                $response->setContent(json_encode($arrayData));
                $response->setStatusCode(Response::HTTP_OK);
                return $response;
            }
            return new JsonResponse([
                'status' => 404,
                'message' => 'Employee is required.'
            ]);
        }
        return new JsonResponse([
            'status' => 500,
            'message' => 'Server Error!'
        ]);

    }

    private function employeeExpenseChart($employeeId)
    {
        $arrayData = [];
        if ($employeeId) {
            $employee = $this->getDoctrine()->getRepository(User::class)->find($employeeId);

            $entity = $this->getDoctrine()->getRepository(ExpenseChart::class)->findOneBy(['employee' => $employee], ['id' => 'DESC']);
//dd($entity);
            /* @var ExpenseChart $entity */
            if ($entity) {
                $arrayData = [
                    "id" => $entity->getId(),
                    "employee_id" => $entity->getEmployee() ? $entity->getEmployee()->getId() : null,
                    "typeOfVehicle" => $entity->getTypeOfVehicle() ? $entity->getTypeOfVehicle() : null,
                    "perKmRate" => $entity->getPerKmRate() ? $entity->getPerKmRate() : null,
                ];

                if ($entity->getExpenseChartDetails() && sizeof($entity->getExpenseChartDetails()) > 0) {
                    /* @var ExpenseChartDetail $expenseChartDetail */
                    foreach ($entity->getExpenseChartDetails() as $expenseChartDetail) {
                        $arrayData['details'][] = [
                            'id' => $expenseChartDetail->getId(),
                            'particular_id' => $expenseChartDetail->getParticular() ? $expenseChartDetail->getParticular()->getId() : null,
                            'particular_name' => $expenseChartDetail->getParticular() ? $expenseChartDetail->getParticular()->getName() : null,
                            'particular_slug' => $expenseChartDetail->getParticular() ? $expenseChartDetail->getParticular()->getSlug() : null,
                            'particular_setting_type' => $expenseChartDetail->getParticular() ? $expenseChartDetail->getParticular()->getSettingType() : null,
                            'particular_expense_payment_type' => $expenseChartDetail->getParticular() ? $expenseChartDetail->getParticular()->getExpensePaymentType() : null,
                            'payment_duration' => $expenseChartDetail ? $expenseChartDetail->getPaymentDuration() : null,
                            'amount' => $expenseChartDetail ? $expenseChartDetail->getAmount() : null,
                            'area_id' => $expenseChartDetail->getArea() ? $expenseChartDetail->getArea()->getId() : null,
                            'area_name' => $expenseChartDetail->getArea() ? $expenseChartDetail->getArea()->getName() : null,
                            'area_slug' => $expenseChartDetail->getArea() ? $expenseChartDetail->getArea()->getSlug() : null,
                        ];
                    }
                }

            }
        }

        return $arrayData;
    }

    /**
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @return JsonResponse
     * @Route("/employee/expense/insert/using-app", name="crm_employee_expense_insert_using_app")
     */
    public function expenseInsertUsingApp(Request $request, ParameterBagInterface $parameterBag)
    {

        set_time_limit(0);
        ignore_user_abort(true);
        if ($request->getMethod() == 'POST' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {
            $arrayData = $request->getContent();

            $expenseData = json_decode($arrayData, true);
            $employeeId = isset($expenseData['employee_id']) && $expenseData['employee_id'] != '' ? $expenseData['employee_id'] : null;

            $vistingDate = isset($expenseData['visit_date']) && $expenseData['visit_date'] != "" ? date('Y-m-d', strtotime($expenseData['visit_date'])) : '';
            $employee = $this->getDoctrine()->getRepository(User::class)->find($employeeId);

            if ($employeeId && $vistingDate && $employee) {
                $expenseApiResponse = new ExpenseApiResponse();
                $expenseApiResponse->setEmployee($employee);
                $expenseApiResponse->setJsonResponse($arrayData);
                $this->getDoctrine()->getManager()->persist($expenseApiResponse);

                $visiting_area = isset($expenseData['visiting_area']) && $expenseData['visiting_area'] != '' ? $expenseData['visiting_area'] : '';
                $comment = isset($expenseData['comment']) && $expenseData['comment'] != '' ? $expenseData['comment'] : null;
                $areaId = isset($expenseData['area']) && $expenseData['area'] != '' ? $expenseData['area']['id'] : '';
                $area = null;
                if ($areaId) {
                    $area = $this->getDoctrine()->getRepository(Setting::class)->find($areaId);
                }
                $existingExpense = null;
                $expenseIdForUpdate = isset($expenseData['id']) && $expenseData['id']!=""?(int)$expenseData['id']:'';

                if($expenseIdForUpdate!=''){
                    $existingExpense = $this->getDoctrine()->getRepository(Expense::class)->find($expenseIdForUpdate);
                }elseif ($expenseIdForUpdate=='' && $employeeId!='' && $vistingDate!=''){
                    $dailyExpenseCheck=$this->getDoctrine()->getRepository(Expense::class)->getExpenseByEmployeeAndDate( $employeeId, $vistingDate);
                    if($dailyExpenseCheck && sizeof($dailyExpenseCheck)>0){
                        $existingExpense = $this->getDoctrine()->getRepository(Expense::class)->findOneBy(['employee' => $employee, 'id'=>$dailyExpenseCheck[0]['id']]);
                    }else{
                        $existingExpense = $this->getDoctrine()->getRepository(Expense::class)->findOneBy(['employee' => $employee, 'expenseDate' => new \DateTimeImmutable($vistingDate)]);
                    }
                }

                if ($existingExpense) {
                    $expense = $existingExpense;
                }else{
                    $expense = new Expense();
                }

                    $getLastMileageRecords = $this->getDoctrine()->getRepository(ExpenseConveyanceDetails::class)->getLastMileageByEmployeeDate($employeeId, $vistingDate);

                    $expense->setExpenseDate(new \DateTime($vistingDate));
                    $expense->setEmployee($employee);
                    $expense->setScheduleVisit($visiting_area);
                    $expense->setVisitLocation($visiting_area);
                    $expense->setComments($comment);
                    $expense->setWorkingArea($area);
                    $expense->setStatus(1);
                    $expense->setIsAreaChange(isset($expenseData['change_area']) && $expenseData['change_area']==true?1:0);
                    $expense->setAsPerAttachment(isset($expenseData['as_per_attachment']) ? $expenseData['as_per_attachment']:0);

                    $this->getDoctrine()->getManager()->persist($expense);
                    if(isset($expenseData['particulars']) ){
                        if($expense->getExpenseParticulars() && sizeof($expense->getExpenseParticulars()) >0 ){
                            $this->getDoctrine()->getRepository(ExpenseParticular::class)->deleteAllParticularByExpense($expense);
                        }                        

                        foreach ($expenseData['particulars'] as $particular) {
                            $expensePerticular = new ExpenseParticular();
                            $expensePerticular->setExpense($expense);
                            $expensePerticular->setAmount(isset($particular['amount']) && $particular['amount']!=''?(float)$particular['amount']:0);
                            $expensePerticular->setParticular($this->getDoctrine()->getRepository(Setting::class)->find((int)$particular['particular_id']));
                            $expensePerticular->setExpenseChartDetailId((int)$particular['id']);
                            $this->getDoctrine()->getManager()->persist($expensePerticular);
                        }
                    }

                    if(isset($expenseData['mode_of_transport'])){
                        if($expense->getExpenseConveyanceDetails() && sizeof($expense->getExpenseConveyanceDetails())>0){
                            $this->getDoctrine()->getRepository(ExpenseConveyanceDetails::class)->deleteAllConveyanceDetailByExpense($expense);
                        }

                        foreach ($expenseData['mode_of_transport'] as $transport_type => $value) {
                            $fuel_bill = isset($value['fuel_bill']) && $value['fuel_bill']!=''?(float)$value['fuel_bill']:0;
                            $toll_bill = isset($value['toll_bill']) && $value['toll_bill']!=''?(float)$value['toll_bill']:0;
                            $parking_bill = isset($value['parking_bill']) && $value['parking_bill']!=''?(float)$value['parking_bill']:0;
                            $other_bill = isset($value['other_bill']) && $value['other_bill']!=''?(float)$value['other_bill']:0;
                            $mobil_bill = isset($value['mobil_bill']) && $value['mobil_bill']!=''?(float)$value['mobil_bill']:0;
                            $maintenance_bill = isset($value['maintenance_bill']) && $value['maintenance_bill']!=''?(float)$value['maintenance_bill']:0;
                            $servicing_bill = isset($value['servicing_bill']) && $value['servicing_bill']!=''?(float)$value['servicing_bill']:0;
                            $destination = isset($value['destination'])?$value['destination']:null;

                            $meter_reading_start = isset($value['meter_reading_start']) && $value['meter_reading_start'] !='' ? (float)$value['meter_reading_start'] : 0;
                            $meter_reading_end = isset($value['meter_reading_end']) && $value['meter_reading_end'] !='' ? (float)$value['meter_reading_end'] : 0;
                            $total_reading = isset($value['total_reading']) && $value['total_reading'] !='' ? (float)$value['total_reading'] : 0;

                            $details = isset($value['details'])?$value['details']:null;



                            if($transport_type == 'local-conveyance' || $transport_type=='others') {
                                if(sizeof($value)>0){
                                    foreach ($value as $item) {
                                        $destination = isset($item['destination'])?$item['destination']:null;

                                        $expenseConveyanceDetails = new ExpenseConveyanceDetails();
                                        $expenseConveyanceDetails->setExpense($expense);
                                        $expenseConveyanceDetails->setDestination($destination);
                                        $expenseConveyanceDetails->setDetails($item['details']);
                                        $expenseConveyanceDetails->setAmount((float)$item['amount']);
                                        $expenseConveyanceDetails->setTransportType($transport_type);
                                        $expenseConveyanceDetails->setTotalAmount($expenseConveyanceDetails->calculateTotalAmount());
                                        $this->getDoctrine()->getManager()->persist($expenseConveyanceDetails);
                                    }
                                }
                            }else{
                                $expenseConveyanceDetails = new ExpenseConveyanceDetails();
                                
                                $expenseConveyanceDetails->setExpense($expense);
                                $expenseConveyanceDetails->setAmount(isset($value['amount']) && $value['amount']!=''?(float)$value['amount']:0);
                                $expenseConveyanceDetails->setFuelBill((float)$fuel_bill);
                                $expenseConveyanceDetails->setTollBill((float)$toll_bill);
                                $expenseConveyanceDetails->setMobilBill((float)$mobil_bill);
                                $expenseConveyanceDetails->setMaintenanceBill((float)$maintenance_bill);
                                $expenseConveyanceDetails->setParkingBill((float)$parking_bill);
                                $expenseConveyanceDetails->setServicingBill((float)$servicing_bill);
                                $expenseConveyanceDetails->setTransportType($transport_type);
                                $expenseConveyanceDetails->setOthersBill((float)$other_bill);
                                $expenseConveyanceDetails->setDestination($destination);
                                $expenseConveyanceDetails->setMeterReadingFrom($meter_reading_start);
                                $expenseConveyanceDetails->setMeterReadingTo($meter_reading_end);
                                $expenseConveyanceDetails->setTotalMileage($total_reading);
                                $expenseConveyanceDetails->setDetails($details);
                                $expenseConveyanceDetails->setTotalAmount($expenseConveyanceDetails->calculateTotalAmount());

                                $cumulativeTotalMileageOneHundred = $getLastMileageRecords && isset($getLastMileageRecords['cumulativeTotalMileageOneHundred']) ? $getLastMileageRecords['cumulativeTotalMileageOneHundred'] : 0;
                                $mileageOneHundred = 0;

                                if($total_reading>0){
                                    if(($cumulativeTotalMileageOneHundred + $total_reading) == 0) {
                                        $mileageOneHundred = 0;
                                    }elseif (($cumulativeTotalMileageOneHundred + $total_reading) > 0 && ($cumulativeTotalMileageOneHundred + $total_reading) < 1000) {
                                        $mileageOneHundred = $cumulativeTotalMileageOneHundred + $total_reading;
                                    }elseif (($cumulativeTotalMileageOneHundred + $total_reading) >= 1000) {
                                        $mileageOneHundred = ($cumulativeTotalMileageOneHundred + $total_reading) - 1000;
                                    }
                                    $expenseConveyanceDetails->setCumulativeTotalMileageOneHundred($mileageOneHundred);

                                    $cumulativeTotalMileageTwoHundred = $getLastMileageRecords && isset($getLastMileageRecords['cumulativeTotalMileageTwoHundred']) ? $getLastMileageRecords['cumulativeTotalMileageTwoHundred'] : 0;
                                    $mileageTwoHundred = 0;

                                    if(($cumulativeTotalMileageTwoHundred + $total_reading) == 0) {
                                        $mileageTwoHundred = 0;
                                    }elseif (($cumulativeTotalMileageTwoHundred + $total_reading) > 0 && ($cumulativeTotalMileageTwoHundred + $total_reading) < 2000) {
                                        $mileageTwoHundred = $cumulativeTotalMileageTwoHundred + $total_reading;
                                    }elseif (($cumulativeTotalMileageTwoHundred + $total_reading) >= 2000) {
                                        $mileageTwoHundred = ($cumulativeTotalMileageTwoHundred + $total_reading) - 2000;
                                    }
                                    $expenseConveyanceDetails->setCumulativeTotalMileageTwoHundred($mileageTwoHundred);
                                }

                                $this->getDoctrine()->getManager()->persist($expenseConveyanceDetails);

                            }

                        }
                    }


                    $this->getDoctrine()->getManager()->flush();

                    return new JsonResponse([
                        'status' => 200,
                        'message' => 'Success'
                    ]);

            }
            return new JsonResponse([
                'status' => 404,
                'message' => 'Employee and Date is required.'
            ]);

        }

        return new JsonResponse([
            'status' => 500,
            'message' => 'Oops! somethings wrong.'
        ]);
    }

    /**
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @return JsonResponse
     * @Route("/employee/monthly/expense/data", name="crm_employee_expense_data")
     */
    public function getAllExpenseByEmployeeAndMonth( Request $request, ParameterBagInterface $parameterBag)
    {
        set_time_limit(0);
        ignore_user_abort(true);
        if ($request->getMethod() == 'GET' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {
            $employeeId = $request->query->get('employee_id');
            $month = $request->query->get('month');
            $year = $request->query->get('year');
            $expenses=[];
            if($employeeId && $month && $month!="" && $year){
                $expenses = $this->getDoctrine()->getRepository(Expense::class)->getAllExpenseByEmployeeAndMonth($employeeId, $yearMonth = $year.'-'.$month);
            }elseif($employeeId && $month=="" && $year){
                $employee = $this->getDoctrine()->getRepository(User::class)->find($employeeId);
                $expenses = $this->getEmployeeExpenseMonthly($employee, $year);
            }

            return new JsonResponse([
                'status' => 200,
                'message' => 'Success',
                'data' => $expenses
            ]);


        }

        return new JsonResponse([
            'status' => 500,
            'message' => 'Oops! somethings wrong.'
        ]);

    }

    private function getEmployeeExpenseMonthly(User $employee , $year){
        $entities = $this->getDoctrine()->getRepository(Expense::class)->getExpensesByEmployeeAndYear($employee, $year);

        $expensePaticularTotalAmount = $this->getDoctrine()->getRepository(ExpenseParticular::class)->getTotalAmountExpenseParticularWithoutChartDetail($employee, $year);

        $conveyenceTotalAmount = $this->getDoctrine()->getRepository(ExpenseConveyanceDetails::class)->getTotalAmountMonthlyByEmployeeYear($employee, $year);
        
        $returnArray = [];
        
        if($entities && sizeof($entities)>0){
            foreach ($entities as $entity) {
                $expenseParticulars = isset($expensePaticularTotalAmount[$entity['employeeAutoId']]) && isset($expensePaticularTotalAmount[$entity['employeeAutoId']][$entity['expenseMonthYear']]) ? $expensePaticularTotalAmount[$entity['employeeAutoId']][$entity['expenseMonthYear']]:[];
                $entity['particulars']= $expenseParticulars;

                $conveyenceDetails = isset($conveyenceTotalAmount[$entity['employeeAutoId']]) && isset($conveyenceTotalAmount[$entity['employeeAutoId']][$entity['expenseMonthYear']]) ? $conveyenceTotalAmount[$entity['employeeAutoId']][$entity['expenseMonthYear']]:[];
                $entity['mode_of_transport']= $conveyenceDetails;

                $grandTotalAmount = 0;

                if($expenseParticulars && sizeof($expenseParticulars)>0){
                    $grandTotalAmount += array_sum(array_column($expenseParticulars,'totalAmount'));
                }

                if($conveyenceDetails && sizeof($conveyenceDetails)>0){
                    $grandTotalAmount += array_sum(array_column($conveyenceDetails,'totalAmount'));
                }

                $entity['grand_total_amount'] = $grandTotalAmount;

                $returnArray[] = $entity;
            }
        }
        
        return $returnArray;

    }

    /**
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @return JsonResponse
     * @Route("/employee/last/mileage", name="crm_last_mileage")
     */
    public function getLastMileageRecordByEmployeeAndExpenseDate( Request $request, ParameterBagInterface $parameterBag)
    {
        set_time_limit(0);
        ignore_user_abort(true);
        if ($request->getMethod() == 'GET' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {
            $employeeId = $request->query->get('employee_id');
            $visitDate = $request->query->get('visit_date');
            $getLastMileageRecords=[];
            if($employeeId && $visitDate && $visitDate!=""){
                $employee = $this->getDoctrine()->getRepository(User::class)->find($employeeId);
                $getLastMileageRecord = $this->getDoctrine()->getRepository(ExpenseConveyanceDetails::class)->getLastMileageByEmployeeDate($employeeId, $visitDate);
                if($getLastMileageRecord){
                    $getLastMileageRecords = [
                        'id' => (string)$getLastMileageRecord['id'],
                        'transportType' => $getLastMileageRecord['transportType'],
                        'totalMileage' => (string)$getLastMileageRecord['totalMileage'],
                        'lastMeterStartReading' => (string)$getLastMileageRecord['meterReadingFrom'],
                        'lastMeterEndReading' => (string)$getLastMileageRecord['meterReadingTo'],
                        'cumulativeTotalMileageOneThousand' => (string)$getLastMileageRecord['cumulativeTotalMileageOneHundred'],
                        'cumulativeTotalMileageTwoThousand' =>  (string)$getLastMileageRecord['cumulativeTotalMileageTwoHundred'],
                        'perKmRate' => $employee->getExpenseChart() && $employee->getExpenseChart()->getTypeOfVehicle()=='motorcycle' ? '3.50':'0.00',
                        'mobil_bill' => '500',
                        'maintenance_bill' => '500',
                        'servicing_bill' => '500',
                    ];
                }else{
                    $getLastMileageRecords = [
                        "id"=>"",
                        "transportType"=> "",
                        "totalMileage"=> "0",
                        'lastMeterStartReading' => "0",
                        'lastMeterEndReading' => "0",
                        "cumulativeTotalMileageOneThousand"=> "0",
                        "cumulativeTotalMileageTwoThousand"=> "0",
                        'perKmRate' => $employee->getExpenseChart() && $employee->getExpenseChart()->getTypeOfVehicle()=='motorcycle' ? '3.50':'0.00',
                        'mobil_bill' => '500',
                        'maintenance_bill' => '500',
                        'servicing_bill' => '500',
                    ];
                }
            }

            return new JsonResponse([
                'status' => 200,
                'message' => 'Success',
                'data' => $getLastMileageRecords
            ]);


        }

        return new JsonResponse([
            'status' => 500,
            'message' => 'Oops! somethings wrong.'
        ]);

    }


    /**
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @return JsonResponse
     * @Route("/employee/monthly/tour-plan/insert/using-app", methods={"POST"}, name="crm_employee_monthly_tour_plan_insert_using_app")
     */
    public function createMonthTourPlanUsingApp(Request $request, ParameterBagInterface $parameterBag)
    {

        set_time_limit(0);
        ignore_user_abort(true);
        if ($request->getMethod() == 'POST' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {
            $arrayData = $request->getContent();

            $expenseData = json_decode($arrayData, true);
            $employeeId = isset($expenseData['employee_id']) && $expenseData['employee_id'] != '' ? $expenseData['employee_id'] : '';

            if ($employeeId) {
                $employee = $this->getDoctrine()->getRepository(User::class)->find((int)$employeeId);
                $data = isset($expenseData['data']) && $expenseData['data'] != "" ? $expenseData['data']: [];
                if(sizeof($data)>0){
                    foreach ($data as $visitDate => $item) {
                        $workingMode=null;
                        if(isset($item['workingMode']) && isset($item['workingMode']['id'])){
                            $workingMode = $this->getDoctrine()->getRepository(Setting::class)->find((int)$item['workingMode']['id']);
                        }
                        $visitDate = date('Y-m-d', strtotime($visitDate));
                        $exitingVisitPlan = $this->getDoctrine()->getRepository(CrmVisitPlan::class)->findOneBy(['employee' => $employee, 'visitDate' => new \DateTimeImmutable($visitDate)]);
                        if($exitingVisitPlan){
                            $visitPlan = $exitingVisitPlan;
                        }else{
                            $visitPlan = new CrmVisitPlan();
                        }
                        $visitPlan->setEmployee($employee);
                        $visitPlan->setVisitingArea($item['visitingArea']);
                        $visitPlan->setVisitDate(new \DateTime($visitDate));
                        $visitPlan->setAreaList($item['areaList']);
                        $visitPlan->setAgentList(isset($item['agentList']) && $item['agentList'] ? $item['agentList'] : null);
                        $visitPlan->setAgentInfo(isset($item['agentInfo']) && $item['agentInfo'] ? $item['agentInfo'] : null);
                        $visitPlan->setWorkingMode($workingMode);
                        $this->getDoctrine()->getManager()->persist($visitPlan);
                        $this->getDoctrine()->getManager()->flush();

                    }
                }
                    return new JsonResponse([
                        'status' => 200,
                        'message' => 'Success'
                    ]);
                }
                return new JsonResponse([
                    'status' => 403,
                    'message' => 'This Date Expense already exist!'
                ]);
            }
            return new JsonResponse([
                'status' => 500,
                'message' => 'Employee and Date is required.'
            ]);

        }

    /**
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @return JsonResponse
     * @Route("/employee/monthly/tour-plan/find/using-app", name="crm_employee_monthly_tour_plan_get_using_app")
     */
        public function getMonthlyTourPlanByEmployeeAndDate(Request $request, ParameterBagInterface $parameterBag)
        {
            set_time_limit(0);
            ignore_user_abort(true);
            if ($request->getMethod() == 'GET' && $request->headers->get('X-API-KEY') == $parameterBag->get('crm_api_key')) {
                $employeeId = $request->query->get('employee_id');
                $visitingDate = $request->query->get('visiting_date');
                $type = $request->query->get('type'); // monthly or daily
                if ($employeeId){
                    $visitPlans = $this->getDoctrine()->getRepository(CrmVisitPlan::class)->getMonthlyTourPlanByEmployeeAndDate($employeeId, $visitingDate, $type);
                    $dailyExpenseCheck = null;
                   if($visitingDate && $type=='daily'){
                       $dailyExpenseCheck=$this->getDoctrine()->getRepository(Expense::class)->getExpenseByEmployeeAndDate( $employeeId, $visitingDate);
                   }

                   $lastExpense = $this->getDoctrine()->getRepository(Expense::class)->getLastExpenseByEmployee($employeeId);
                   $lastExpenseDate = $lastExpense && isset($lastExpense['expenseDate']) ?$lastExpense['expenseDate']->format('Y-m-d'):'';
                     $lastVisitPlan = null;
                    $lastNextDate = '';
                   if($lastExpenseDate !=""){
                       $lastNextDate = date('Y-m-d', strtotime($lastExpenseDate. ' + 1 days'));
                       $lastNextdateMonth = date('Y-m', strtotime($lastNextDate));
                       $lastVisitPlan = $this->getDoctrine()->getRepository(CrmVisitPlan::class)->getMonthlyTourPlanByEmployeeAndDate($employeeId, $lastNextDate, 'daily');
                       $lastVisitPlan = $lastVisitPlan && isset($lastVisitPlan['data']) && isset($lastVisitPlan['data'][$lastNextdateMonth]) && isset($lastVisitPlan['data'][$lastNextdateMonth][$lastNextDate]) ? $lastVisitPlan['data'][$lastNextdateMonth][$lastNextDate] : '';
                   }

                    return new JsonResponse([
                        'status' => 200,
                        'message' => 'Success',
                        'dailyExpense' => $dailyExpenseCheck?true:false,
                        'lastExpenseDate'=> $lastExpenseDate ? $lastExpenseDate : date('Y-m-01'),
                        'lastNextExpenseDate' => $lastNextDate ? $lastNextDate : date('Y-m-d'),
                        'lastNextVisitPlan'=> $lastVisitPlan,
                        'data' => $visitPlans,
                    ]);
                }
                return new JsonResponse([
                    'status' => 404,
                    'message' => 'Employee, visiting date and type id required',
                ]);
            }
            return new JsonResponse([
                'status' => 500,
                'message' => 'Oops! somethings wrong.'
            ]);
        }

}