<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\CrmBundle\Controller;

use App\Entity\Core\Agent;
use Doctrine\ORM\EntityRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\DataTransformer\DateIntervalToStringTransformer;
use Symfony\Component\Form\Extension\Core\DataTransformer\DateTimeToStringTransformer;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Terminalbd\CrmBundle\Entity\BroilerStandard;
use Terminalbd\CrmBundle\Entity\FishCompanyAndSpeciesWiseAverageFcr;
use Terminalbd\CrmBundle\Entity\FishCompanyAndSpeciesWiseAverageFcrDetails;
use Terminalbd\CrmBundle\Entity\FishLifeCycle;
use Terminalbd\CrmBundle\Entity\FishLifeCycleDetails;
use Terminalbd\CrmBundle\Entity\CrmCustomer;
use Terminalbd\CrmBundle\Entity\SettingLifeCycle;
use Terminalbd\CrmBundle\Form\CompanySpeciesWiseFcrAfterFormType;
use Terminalbd\CrmBundle\Form\CompanySpeciesWiseFcrFormType;
use Terminalbd\CrmBundle\Form\FishLifeCycleDetailsFormType;
use Terminalbd\CrmBundle\Entity\Setting;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Terminalbd\CrmBundle\Form\DairyLifeCycleDetailsFormType;


/**
 * @Route("/crm/fish/company/species")
 * @Security("is_granted('ROLE_CRM_AQUA_ADMIN') or is_granted('ROLE_CRM_AQUA_USER') or is_granted('ROLE_DEVELOPER')")
 */
class FishCompanyAndSpeciesWiseAverageFcrController extends AbstractController
{

    /**
     * @param Request $request
     * @param CrmCustomer $crmCustomer
     * @param Setting $report
     * @return Response
     * @throws \Exception
     * @ParamConverter("crmCustomer", class="Terminalbd\CrmBundle\Entity\CrmCustomer")
     * @Route("/customer/{id}/report/{report}/new/modal", methods={"GET", "POST"}, name="fish_company_species_wise_fcr_modal", options={"expose"=true})
     */
    public function newModal(Request $request, CrmCustomer $crmCustomer, Setting $report): Response
    {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(CompanySpeciesWiseFcrFormType::class, null, array('report' => $report));
        $returnIds=[];
        $form->handleRequest($request);
        if ($form->isSubmitted()){
            $data = $request->request->get('company_species_wise_fcr_form');
            $reportingDate = isset($data['reporting_date'])? $data['reporting_date'] : date('Y-m-d',strtotime('now'));
            $date=new \DateTime($reportingDate);
            if(isset($data['feed'])&&$data['feed']!=''){

                $feedType = isset($data['feed_type'])?$this->getDoctrine()->getRepository(Setting::class)->find($data['feed_type']):null;
                foreach ($data['feed'] as $feedId){


                    $feed = $this->getDoctrine()->getRepository(Setting::class)->find($feedId);

                    $entity = new FishCompanyAndSpeciesWiseAverageFcr();
                    $existReport = $this->getDoctrine()->getRepository(FishCompanyAndSpeciesWiseAverageFcr::class)->findOneBy(array('reportingMonth'=>$date, 'report'=>$report, 'customer'=>$crmCustomer, 'employee'=>$this->getUser(),'feed'=>$feed,'feedType'=>$feedType));

                    if($existReport){
                        $entity=$existReport;
                    }

                    if($existReport){
                        $entity->setReportingMonth($entity->getReportingMonth());
                    }else{
                        $entity->setReportingMonth(new \DateTime($reportingDate));
                    }
                    $entity->setCustomer($crmCustomer);
                    $entity->setAgent($crmCustomer->getAgent()?$crmCustomer->getAgent():null);
                    $entity->setReport($report);
                    $entity->setEmployee($this->getUser());
                    $entity->setFeed($feed);
                    $entity->setFcrOfFeed('BEFORE');
                    $entity->setFeedType($feedType);
                    $em->persist($entity);

                    $speciesNameByFeedType= $this->getDoctrine()->getRepository(Setting::class)->findBy(array('settingType'=>'SPECIES_NAME','status'=>1,'parent'=>$feedType));
                    if($speciesNameByFeedType){
                        foreach ($speciesNameByFeedType as $item){
                            $fishCompanySpeciesWiseFcrDetails = new FishCompanyAndSpeciesWiseAverageFcrDetails();

                            $existDetails = $this->getDoctrine()->getRepository(FishCompanyAndSpeciesWiseAverageFcrDetails::class)->findOneBy(array('speciesName'=>$item, 'fishCompanyAndSpeciesWiseAverageFcr'=>$entity));
                            if($existDetails){
                                $fishCompanySpeciesWiseFcrDetails=$existDetails;
                            }
                            $fishCompanySpeciesWiseFcrDetails->setSpeciesName($item);
                            $fishCompanySpeciesWiseFcrDetails->setFishCompanyAndSpeciesWiseAverageFcr($entity);
                            $em->persist($fishCompanySpeciesWiseFcrDetails);
                        }
                    }
                    $em->flush();

                    $returnIds[]=$entity->getId();
                }


            }

            $this->addFlash('success', 'post.created_successfully');

            return $this->redirectToRoute('fish_company_species_wise_fcr_details', ['beforeAfter'=>"BEFORE",'feedType'=>$feedType->getId(),'ids'=>$returnIds,'created_date'=>$reportingDate]);

        }

        $companyWiseFcrs = $this->getDoctrine()->getRepository(FishCompanyAndSpeciesWiseAverageFcrDetails::class)->getCompanyAndSpeciesWiseFcrByEmployee($report,'BEFORE', $this->getUser());

        $feedTypes = $this->getDoctrine()->getRepository(Setting::class)->findBy(['settingType'=>'FEED_TYPE', 'parent'=>$report->getParent()]);
        $speciesName = $this->getDoctrine()->getRepository(Setting::class)->getSpeciesNameByFeedTypes($feedTypes);

        return $this->render('@TerminalbdCrm/fishCompanySpeciesWiseFcr/fish-company-species-wise-fcr.html.twig', [
            'customer' => $crmCustomer,
            'report' => $report,
            'form' => $form->createView(),
            'feedTypes' => $feedTypes,
            'mainCultureSpecies' => $speciesName,
            'companyWiseFcrs' => $companyWiseFcrs,
        ]);
    }

    /**
     * @Route("/{beforeAfter}/{feedType}/details", methods={"GET", "POST"}, name="fish_company_species_wise_fcr_details", options={"expose"=true})
     * @param Request $request
     * @param Setting $feedType
     * @param $beforeAfter
     * @return Response
     */
    public function companySpeciesWiseFcrDetails(Request $request, Setting $feedType, $beforeAfter): Response
    {
        $ids= $request->query->get('ids');
        $reportingDate = $request->query->get('created_date');

        $companySpeciesWiseFcrs = $this->getDoctrine()->getRepository(FishCompanyAndSpeciesWiseAverageFcr::class)->getFishCompanySpeciesWiseFcrByReportingDateAndIds($ids, $reportingDate, $this->getUser());

        $returnDetails=[];
        $mainCultureSpecies=null;
        if ($feedType){
            $mainCultureSpecies = $this->getDoctrine()->getRepository(Setting::class)->findBy(array('status'=>1,'settingType'=>'SPECIES_NAME','parent'=>$feedType),['id' => 'ASC']);
        }

        if($companySpeciesWiseFcrs){
            /* @var FishCompanyAndSpeciesWiseAverageFcr $companySpeciesWiseFcr*/
            foreach ($companySpeciesWiseFcrs as $companySpeciesWiseFcr){
                /* @var FishCompanyAndSpeciesWiseAverageFcrDetails $andSpeciesWiseAverageFcrDetail*/
                foreach ($companySpeciesWiseFcr->getFishCompanyAndSpeciesWiseAverageFcrDetails() as $andSpeciesWiseAverageFcrDetail){
                    $returnDetails[$companySpeciesWiseFcr->getId()][$andSpeciesWiseAverageFcrDetail->getSpeciesName()->getId()] = $andSpeciesWiseAverageFcrDetail;
                }

            }
        }

//        $companyWiseFcrs = $this->getDoctrine()->getRepository(FishCompanyAndSpeciesWiseAverageFcrDetails::class)->getCompanyAndSpeciesWiseFcrByEmployee($beforeAfter, $this->getUser());
        
        return $this->render('@TerminalbdCrm/fishCompanySpeciesWiseFcr/fish-company-species-wise-fcr-details-report-modal.html.twig', [
            'companySpeciesWiseFcrs' => $companySpeciesWiseFcrs,
            'companySpeciesDetails' => $returnDetails,
            'mainCultureSpecies' => $mainCultureSpecies,
//            'companyWiseFcrs' => $companyWiseFcrs,
        ]);
    }


    /**
     * @Route("/details/{id}/update", methods={"POST"}, name="fish_company_species_wise_fcr_details_data_update", options={"expose"=true})
     * @param Request $request
     * @param FishCompanyAndSpeciesWiseAverageFcrDetails $entity
     * @return Response
     */

    public function editCompanySpeciesWiseFcrDetails(Request $request, FishCompanyAndSpeciesWiseAverageFcrDetails $entity): Response
    {
        $data = $request->request->all();
        $metaValue = $data['dataMetaValue'];
        $dataMetaKey = $data['dataMetaKey'];


        if($metaValue!=''){

            $set = 'set'.$dataMetaKey;

            $entity->$set($metaValue);

        }

        $entity->setQuantity($entity->calculateFcrQuantity());

        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();

        return new JsonResponse(
            array(
                'success'=>'Success',
                'data'=>$data,
                'id'=>$entity->getId(),
                'quantity'=>number_format($entity->getQuantity(),3,'.',''),
                'status'=>200,
            )
        );

    }

    /**
     * @param FishCompanyAndSpeciesWiseAverageFcr $companySpeciesWiseFcr
     * @return Response
     * @Route("{id}/refresh", methods={"GET"}, name="crm_fish_company_species_wise_fcr_refresh", options={"expose"=true})
     */
    public function fishCompanySpeciesWiseFcrReportRefresh(FishCompanyAndSpeciesWiseAverageFcr $companySpeciesWiseFcr)
    {
        $reportingDate = $companySpeciesWiseFcr->getReportingMonth()->format('Y-m-d');
        $mainCultureSpecies = $this->getDoctrine()->getRepository(Setting::class)->findBy(array('status'=>1,'settingType'=>'SPECIES_NAME','parent'=>$companySpeciesWiseFcr->getFeedType()),['id' => 'ASC']);

        $fcrDetailsMonthWise = $this->getDoctrine()->getRepository(FishCompanyAndSpeciesWiseAverageFcrDetails::class)->getCompanySpeciesWiseFcrDetailsByReportingMonth($companySpeciesWiseFcr->getFcrOfFeed(), $companySpeciesWiseFcr->getFeedType()->getId(), $reportingDate, $this->getUser()->getId());
        $fcrMonthWise = $this->getDoctrine()->getRepository(FishCompanyAndSpeciesWiseAverageFcr::class)->getFishCompanySpeciesWiseFcrByReportingMonth($companySpeciesWiseFcr->getFcrOfFeed(), $companySpeciesWiseFcr->getFeedType(), $reportingDate, $this->getUser());

        return $this->render('@TerminalbdCrm/fishCompanySpeciesWiseFcr/partial/fish-company-species-wise-fcr-details-body.html.twig', [
            'mainCultureSpecies' => $mainCultureSpecies,
            'fcrDetailsMonthWise' => $fcrDetailsMonthWise,
            'fcrMonthWise' => $fcrMonthWise,
        ]);
    }

    /**
     * @Route("/report/{report}/after/new/modal", methods={"GET", "POST"}, name="fish_company_species_wise_fcr_after_modal", options={"expose"=true})
     * @param Request $request
     * @param Setting $report
     * @return Response
     * @throws \Exception
     */
    public function newAfterModal(Request $request, Setting $report): Response
    {
        $em = $this->getDoctrine()->getManager();

        $agentRepo = $this->getDoctrine()->getRepository(Agent::class);

        $form = $this->createForm(CompanySpeciesWiseFcrAfterFormType::class, null, array('report' => $report, 'user' => $this->getUser(),'agentRepo' => $agentRepo));
        $returnIds=[];
        $form->handleRequest($request);
        if ($form->isSubmitted()){
            $data = $request->request->get('company_species_wise_fcr_after_form');
            $reportingDate = isset($data['reporting_date'])? $data['reporting_date'] : date('Y-m-d',strtotime('now'));
            $date=new \DateTime($reportingDate);
            if(isset($data['feed'])){

                $feedType = isset($data['feed_type'])?$this->getDoctrine()->getRepository(Setting::class)->find($data['feed_type']):null;
                $agent = isset($data['agent'])?$agentRepo->find($data['agent']):null;
                foreach ($data['feed'] as $feedId){


                    $feed = $this->getDoctrine()->getRepository(Setting::class)->find($feedId);

                    $entity = new FishCompanyAndSpeciesWiseAverageFcr();
                    $existReport = $this->getDoctrine()->getRepository(FishCompanyAndSpeciesWiseAverageFcr::class)->findOneBy(array('reportingMonth'=>$date, 'report'=>$report, 'agent'=>$agent, 'employee'=>$this->getUser(),'feed'=>$feed,'feedType'=>$feedType));

                    if($existReport){
                        $entity=$existReport;
                    }

                    if($existReport){
                        $entity->setReportingMonth($entity->getReportingMonth());
                    }else{
                        $entity->setReportingMonth(new \DateTime($reportingDate));
                    }
                    $entity->setAgent($agent?$agent:null);
                    $entity->setReport($report);
                    $entity->setEmployee($this->getUser());
                    $entity->setFeed($feed);
                    $entity->setFcrOfFeed('AFTER');
                    $entity->setFeedType($feedType);
                    $em->persist($entity);

                    $speciesNameByFeedType= $this->getDoctrine()->getRepository(Setting::class)->findBy(array('settingType'=>'SPECIES_NAME','status'=>1,'parent'=>$feedType));
                    if($speciesNameByFeedType){
                        foreach ($speciesNameByFeedType as $item){
                            $fishCompanySpeciesWiseFcrDetails = new FishCompanyAndSpeciesWiseAverageFcrDetails();

                            $existDetails = $this->getDoctrine()->getRepository(FishCompanyAndSpeciesWiseAverageFcrDetails::class)->findOneBy(array('speciesName'=>$item, 'fishCompanyAndSpeciesWiseAverageFcr'=>$entity));
                            if($existDetails){
                                $fishCompanySpeciesWiseFcrDetails=$existDetails;
                            }
                            $fishCompanySpeciesWiseFcrDetails->setSpeciesName($item);
                            $fishCompanySpeciesWiseFcrDetails->setFishCompanyAndSpeciesWiseAverageFcr($entity);
                            $em->persist($fishCompanySpeciesWiseFcrDetails);
                        }
                    }
                    $em->flush();
                    $returnIds[]=$entity->getId();
                }


            }

            $this->addFlash('success', 'post.created_successfully');

            return $this->redirectToRoute('fish_company_species_wise_fcr_details', ['beforeAfter'=>'AFTER','feedType'=>$feedType->getId(),'ids'=>$returnIds,'created_date'=>$reportingDate]);

        }


        $companyWiseFcrs = $this->getDoctrine()->getRepository(FishCompanyAndSpeciesWiseAverageFcrDetails::class)->getCompanyAndSpeciesWiseFcrByEmployee($report,'AFTER', $this->getUser());

        $feedTypes = $this->getDoctrine()->getRepository(Setting::class)->findBy(['settingType'=>'FEED_TYPE', 'parent'=>$report->getParent()]);
        $speciesName = $this->getDoctrine()->getRepository(Setting::class)->getSpeciesNameByFeedTypes($feedTypes);



        return $this->render('@TerminalbdCrm/fishCompanySpeciesWiseFcr/after/fish-company-species-wise-fcr.html.twig', [
            'report' => $report,
            'form' => $form->createView(),
            'feedTypes' => $feedTypes,
            'mainCultureSpecies' => $speciesName,
            'companyWiseFcrs' => $companyWiseFcrs,
        ]);
    }


    /**
     * Deletes a Fcr entity.
     * @Route("/details/{id}/delete", methods={"POST"}, name="fish_life_cycle_detail_delete", options={"expose"=true})
     * @param $id
     * @return Response
     */
    public function deleteDetails($id): Response
    {
        $entity = $this->getDoctrine()->getRepository(FishLifeCycleDetails::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($entity);
        $em->flush();
        $this->addFlash('success', 'post.deleted_successfully');
        return new Response('Success');
    }



}
