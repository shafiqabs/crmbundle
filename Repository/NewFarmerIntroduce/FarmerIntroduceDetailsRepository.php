<?php

namespace Terminalbd\CrmBundle\Repository\NewFarmerIntroduce;

use App\Entity\Core\Agent;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Terminalbd\CrmBundle\Entity\CrmCustomer;
use Terminalbd\CrmBundle\Entity\NewFarmerIntroduce\FarmerIntroduceDetails;
use Terminalbd\CrmBundle\Entity\Setting;
use Terminalbd\CrmBundle\Repository\BaseRepository;
use Terminalbd\KpiBundle\Entity\EmployeeBoard;

class FarmerIntroduceDetailsRepository extends BaseRepository
{
    public function insertCrmFarmerIntroduceDetails(CrmCustomer $customer, $user, $feed, $data)
    {
        $subAgent=null;


        if ($data['farmer_type']){
            $em = $this->_em;
            $entity = new FarmerIntroduceDetails();
            $entity->setCustomer($customer);
            $entity->setAgent($customer->getAgent());
            if($customer->getOtherAgent()){
                $entity->setOtherAgent($customer->getOtherAgent());
            }
            if(isset($data['sub_agent'])&&$data['sub_agent']!=''){
                $subAgent = $em->getRepository(Agent::class)->find($data['sub_agent']);
            }
            if($subAgent){
                $entity->setSubAgent($subAgent);
            }
            $entity->setFeed($feed?$feed:null);
            if ($feed->getName() != 'Nourish'){
                $entity->setOtherFeed($feed?$feed:null);
            }            
            $entity->setCultureSpeciesItemAndQty(json_encode($data['species_type']));
            /*$entity->setPreviousAgentName($data['previous_agent_name']);
            $entity->setPreviousAgentAddress($data['previous_agent_address']);
            $entity->setPreviousFeedName($data['previous_feed_name']);*/

            $entity->setEmployee($user);

            if($data['farmer_type']){
                $farmerType = $em->getRepository(Setting::class)->find($data['farmer_type']);
                $entity->setFarmerType($farmerType);
            }
            $em->persist($entity);
            $em->flush();
        }

    }

    public function getFarmerIntroduceReportByEmployeeDate($report, $filterBy, $loggedUser)
    {
        $returnArray = [];
        $breed= $report->getParent()->getParent();
        $species = $this->getEntityManager()->getRepository(Setting::class)->findBy(array('status'=>1,'settingType'=>'SPECIES_TYPE','parent'=>$breed));
//dd($species);
        if($report){
            $qb = $this->createQueryBuilder('e');
            $qb->select('e.id as eId', 'e.cultureSpeciesItemAndQty', 'e.remarks', 'e.createdAt', 'e.introduceDate');
            $qb->addSelect('farmer.name AS customerName', 'farmer.address AS customerAddress', 'farmer.mobile AS customerMobile');
            $qb->addSelect( 'customerRegion.id AS regionId', 'customerRegion.name AS regionName');
            $qb->addSelect('agent.name AS agentName','agent.address AS agentAddress');
            $qb->addSelect('otherAgent.name AS otherAgentName','otherAgent.address AS otherAgentAddress');
            $qb->addSelect('subAgent.name AS subAgentName','subAgent.address AS subAgentAddress');
            $qb->addSelect('farmerType.name AS farmerTypeName');
            $qb->addSelect('feed.name AS feedName');
            $qb->addSelect('otherFeed.name AS otherFeedName');
            $qb->addSelect('employee.id AS employeeId', 'employee.userId AS employeeUserId', 'employee.name AS employeeName');
            $qb->addSelect('designation.name AS employeeDesignationName');


            $qb->join('e.customer', 'farmer');
            $qb->join('farmer.location','customerUpazila');
            $qb->join('customerUpazila.parent', 'customerDistrict');
            $qb->join('customerDistrict.parent', 'customerRegion');
            $qb->join('e.employee', 'employee');
            $qb->join('e.farmerType', 'farmerType');
            $qb->leftJoin('employee.designation', 'designation');
            $qb->leftJoin('e.agent', 'agent');
            $qb->leftJoin('e.otherAgent', 'otherAgent');
            $qb->leftJoin('e.subAgent', 'subAgent');
            $qb->leftJoin('e.feed', 'feed');
            $qb->leftJoin('e.otherFeed', 'otherFeed');

            $qb->where('e.farmerType =:farmerType')->setParameter('farmerType', $breed);

            $startDate = isset($filterBy['startDate'])&&$filterBy['startDate']!=''? (new \DateTime($filterBy['startDate']))->format('Y-m-d') . ' 00:00:00': '';
            $endDate = isset($filterBy['endDate']) && $filterBy['endDate']!=''? (new \DateTime($filterBy['endDate']))->format('Y-m-d') . ' 23:59:59': '';

            $employee = isset($filterBy['employeeId'])&&$filterBy['employeeId']!=''? $filterBy['employeeId']: '';
            if (!empty($employee)){
                $qb->andWhere('employee.id = :employee')->setParameter('employee', $employee);
            }

            $rolesString = implode('_', $loggedUser->getRoles());
            if (!str_contains($rolesString, 'ADMIN') && !in_array('ROLE_LINE_MANAGER', $loggedUser->getRoles())){
                $qb->andWhere('employee.id = :employeeId')->setParameter('employeeId', $loggedUser->getId());
            }elseif (!str_contains($rolesString, 'ADMIN') && in_array('ROLE_LINE_MANAGER', $loggedUser->getRoles())){

                $employeeIdsByLineManager = $this->_em->getRepository(User::class)->getEmployeesByLineManager($loggedUser);
                $employeeIs=[];
                if($employeeIdsByLineManager){
                    $employeeIs=$employeeIdsByLineManager;
                }
                $qb->andWhere('employee.id IN (:employeeIds)')->setParameter('employeeIds', $employeeIs);
            }

            if (!empty($startDate) && !empty($endDate)){
                $qb->andWhere('e.introduceDate >= :startDate')->setParameter('startDate', $startDate);
                $qb->andWhere('e.introduceDate <= :endDate')->setParameter('endDate', $endDate);
            }
            $qb->andWhere('e.introduceDate IS NOT NULL');
            $qb->andWhere('e.createdAt IS NOT NULL');

            $region = isset($filterBy['region'])? $filterBy['region']: '';
            if (!empty($region)){
                $qb->andWhere('customerRegion.id = :regionId')->setParameter('regionId', $region);
            }
            
            $qb->orderBy('e.introduceDate','ASC');
            $results = $qb->getQuery()->getArrayResult();

            if($results){
                foreach ($results as $result){
                    $monthYear = $result['introduceDate']->format('F-Y');

                    /*$returnArray[$result['employeeId']]['name']=$result['employeeName'];
                    $returnArray[$result['employeeId']]['employeeDesignationName']=$result['employeeDesignationName'];
                    $returnArray[$result['employeeId']]['details'][$monthYear][]=$result;*/


                    $returnArray['details'][$monthYear][$result['regionId']][$result['employeeId']][]=$result;
                    $returnArray['regionRecord'][$monthYear][$result['regionId']][]=$result;
                    $returnArray['monthRecord'][$monthYear][]=$result;
                }
                $returnArray['totalRecord']=$results;
                $returnArray['species']=$species;

            }
        }
//        dd($returnArray);

        return $returnArray;

    }

    public function getFarmerIntroduceReportByEmployeeDateForKpiMonthlyReport($report, $filterBy)
    {
        $returnArray = [];
        $breed= $report->getParent()->getParent();

        $species = $this->getEntityManager()->getRepository(Setting::class)->findBy(array('status'=>1,'settingType'=>'SPECIES_TYPE','parent'=>$breed));

        $year = isset( $filterBy['year']) &&  $filterBy['year']!='' ?  $filterBy['year'] : date('Y');
        $startMonth = date('Y-m-d', strtotime($year . '-' . $filterBy['startMonth'] . '-01'));
        $endMonth = date('Y-m-t', strtotime($year . '-' . $filterBy['endMonth'] . '-01'));

        $startDate = isset($filterBy['startMonth']) ? (new \DateTime($startMonth))->format('Y-m-d 00:00:00') : date('Y-m-d 00:00:00');
        $endDate = isset($filterBy['endMonth']) ? (new \DateTime($endMonth))->format('Y-m-d 23:59:59') : date('Y-m-t 23:59:59');

        if($report){
            $qb = $this->createQueryBuilder('e');
            $qb->select('e.id as eId', 'e.cultureSpeciesItemAndQty', 'e.remarks', 'e.createdAt', 'e.introduceDate');
            $qb->addSelect('farmer.name AS customerName', 'farmer.address AS customerAddress', 'farmer.mobile AS customerMobile');
            $qb->addSelect( 'customerRegion.id AS regionId', 'customerRegion.name AS regionName');
            $qb->addSelect('agent.name AS agentName','agent.address AS agentAddress');
            $qb->addSelect('otherAgent.name AS otherAgentName','otherAgent.address AS otherAgentAddress');
            $qb->addSelect('subAgent.name AS subAgentName','subAgent.address AS subAgentAddress');
            $qb->addSelect('farmerType.name AS farmerTypeName');
            $qb->addSelect('feed.name AS feedName');
            $qb->addSelect('otherFeed.name AS otherFeedName');
            $qb->addSelect('employee.id AS employeeId', 'employee.userId AS employeeUserId', 'employee.name AS employeeName');
            $qb->addSelect('designation.name AS employeeDesignationName');


            $qb->join('e.customer', 'farmer');
            $qb->join('farmer.location','customerUpazila');
            $qb->join('customerUpazila.parent', 'customerDistrict');
            $qb->join('customerDistrict.parent', 'customerRegion');
            $qb->join('e.employee', 'employee');
            $qb->join('e.farmerType', 'farmerType');
            $qb->leftJoin('employee.designation', 'designation');
            $qb->leftJoin('e.agent', 'agent');
            $qb->leftJoin('e.otherAgent', 'otherAgent');
            $qb->leftJoin('e.subAgent', 'subAgent');
            $qb->leftJoin('e.feed', 'feed');
            $qb->leftJoin('e.otherFeed', 'otherFeed');

            $qb->where('e.farmerType =:farmerType')->setParameter('farmerType', $breed);
            $qb->andWhere('employee = :employee')->setParameter('employee', $filterBy['employee']);

            $qb->andWhere('e.introduceDate >= :startDate')->setParameter('startDate', $startDate);
            $qb->andWhere('e.introduceDate <= :endDate')->setParameter('endDate', $endDate);

            $qb->andWhere('e.introduceDate IS NOT NULL');
            $qb->andWhere('e.createdAt IS NOT NULL');

            $qb->orderBy('e.introduceDate','ASC');
            $results = $qb->getQuery()->getArrayResult();

            if($results){
                foreach ($results as $result){
                    $monthYear = $result['introduceDate']->format('F-Y');
                    $returnArray['records'][$monthYear][]=$result;
                }
                $returnArray['species']=$species;

            }
        }

        return $returnArray;

    }

    public function getMonthlyNewFarmerIntroduceTotalReport($filterBy)
    {
        $qb = $this->createQueryBuilder('e');
        $qb->select('COUNT(e) as totalReport');

        $qb->join('e.employee', 'employee');

        $qb->where('employee.id = :employeeId')->setParameter('employeeId', $filterBy['employeeId']);
        $qb->andWhere('e.createdAt >= :monthStart')->setParameter('monthStart', $filterBy['monthStart'] . ' 00:00:00');
        $qb->andWhere('e.createdAt <= :monthEnd')->setParameter('monthEnd', $filterBy['monthEnd'] . ' 23:59:59');

        $results = $qb->getQuery()->getSingleResult();
        return $results['totalReport'];
    }

    public function getFarmerSurveyReport($filterBy, User $loggedUser)
    {

        $start = isset($filterBy['startDate']) ? (new \DateTime($filterBy['startDate']))->format('Y-m-d') . ' 00:00:00' : null;
        $end = isset($filterBy['endDate']) ? (new \DateTime($filterBy['endDate']))->format('Y-m-d') . ' 23:59:59' : null;
        $employee = isset($filterBy['employee']) ? $filterBy['employee']->getId() : null;
        $qb = $this->createQueryBuilder('e');

        $qb->join('e.employee', 'employee');
        $qb->leftJoin('employee.designation', 'designation');
        $qb->leftJoin('employee.regional', 'regional');
        $qb->join('e.customer', 'farmer');
        $qb->join('e.agent', 'agent');
        $qb->leftJoin('e.feed', 'feed');
        $qb->leftJoin('e.otherFeed', 'other_feed');
        $qb->leftJoin('agent.district', 'agentDistrict');
        $qb->leftJoin('agent.upozila', 'agentUpozila');

        $qb->select('e.cultureSpeciesItemAndQty', 'e.remarks', 'e.createdAt');
        $qb->addSelect('farmer.id AS farmerId', 'farmer.name AS farmerName', 'farmer.address AS farmerAddress', 'farmer.mobile AS farmerMobile');
        $qb->addSelect('agent.id as agentAutoId','agent.agentId', 'agent.name AS agentName', 'agentDistrict.name AS agentDistrictName', 'agentUpozila.name AS agentUpozilaName');
        $qb->addSelect('other_feed.name AS otherFeedName');
        $qb->addSelect('feed.name AS feedName');
        $qb->addSelect('employee.id AS employeeId', 'employee.userId as employeeUserId', 'employee.name AS employeeName');
        $qb->addSelect('designation.name AS employeeDesignationName');
        $qb->addSelect('regional.name AS employeeRegionName');
        if($employee){
            $qb->where('employee.id = :employeeId')->setParameter('employeeId', $employee);
        }

        $rolesString = implode('_', $loggedUser->getRoles());
        if (!str_contains($rolesString, 'ADMIN') && !in_array('ROLE_LINE_MANAGER', $loggedUser->getRoles())){
            $qb->andWhere('employee.id = :employeeId')->setParameter('employeeId', $loggedUser->getId());
        }elseif (!str_contains($rolesString, 'ADMIN') && in_array('ROLE_LINE_MANAGER', $loggedUser->getRoles())){

            $employeeIdsByLineManager = $this->_em->getRepository(User::class)->getEmployeesByLineManager($loggedUser);
            $employeeIs=[];
            if($employeeIdsByLineManager){
                $employeeIs=$employeeIdsByLineManager;
            }
            $qb->andWhere('employee.id IN (:employeeIds)')->setParameter('employeeIds', $employeeIs);
        }
        
        
        $qb->andWhere('e.createdAt >= :start')->setParameter('start', $start);
        $qb->andWhere('e.createdAt <= :end')->setParameter('end', $end);

        $results = $qb->getQuery()->getArrayResult();

        $data = [];
        foreach ($results as $result) {
            $month = $result['createdAt']->format('Y-m-F');
            $data['employeeInfo'][$result['employeeId']] = [
                'employeeId' => $result['employeeUserId'],
                'employeeName' => $result['employeeName'],
                'employeeDesignationName' => $result['employeeDesignationName'],
                'employeeRegionName' => $result['employeeRegionName'],
            ];
            $data['agentInfo'][$result['agentAutoId']] = [
                'agentId' => $result['agentId'],
                'agentName' => $result['agentName'],
                'agentDistrictName' => $result['agentDistrictName'],
                'agentUpozilaName' => $result['agentUpozilaName'],
            ];
            $data['records'][$result['employeeId']][$month][$result['agentAutoId']][$result['farmerId']] = $result;
        }
        ksort($data);
        return $data;
    }


    public function getNumberOfReportsForKpi($board, $type)
    {
        /**
         * @var EmployeeBoard $board
         */
        $startDate = (new \DateTime('01-' . date('m', strtotime($board->getMonth())) . '-' . $board->getYear()))->format('Y-m-d');
        $endDate = (new \DateTime('01-' . date('m', strtotime($board->getMonth())) . '-' . $board->getYear()))->format('Y-m-t');

        $qb = $this->createQueryBuilder('e');

        $qb->join('e.farmerType', 'farmer_type');
        $qb->where('e.employee = :employee')->setParameter('employee',$board->getEmployee());
        $qb->andWhere('e.introduceDate >= :startDate')->setParameter('startDate', $startDate);
        $qb->andWhere('e.introduceDate <= :endDate')->setParameter('endDate', $endDate);
        $qb->andWhere('farmer_type.slug = :slug')->setParameter('slug', $type);


        return count($qb->getQuery()->getArrayResult());
    }

    public function getNumberOfReportsNewFarmerForKpi($board, $type)
    {
        /**
         * @var EmployeeBoard $board
         */
        $startDate = (new \DateTime('01-' . date('m', strtotime($board->getMonth())) . '-' . $board->getYear()))->format('Y-m-d');
        $endDate = (new \DateTime('01-' . date('m', strtotime($board->getMonth())) . '-' . $board->getYear()))->format('Y-m-t');

        $qb = $this->createQueryBuilder('e');

        $qb->join('e.farmerType', 'farmer_type');
        $qb->where('e.employee = :employee')->setParameter('employee',$board->getEmployee());
        $qb->andWhere('e.createdAt >= :startDate')->setParameter('startDate', $startDate);
        $qb->andWhere('e.createdAt <= :endDate')->setParameter('endDate', $endDate);
        $qb->andWhere('farmer_type.slug = :slug')->setParameter('slug', $type);


        return count($qb->getQuery()->getArrayResult());
    }

}