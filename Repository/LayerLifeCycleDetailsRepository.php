<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\CrmBundle\Repository;

//use Doctrine\ORM\EntityRepository;
use Terminalbd\KpiBundle\Entity\EmployeeBoard;

/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */
class LayerLifeCycleDetailsRepository extends BaseRepository
{
    public function getLayerLifeCycleDetails($lifeCycleSlug, $filterBy)
    {
        $startDate = $filterBy['startDate'] ? (new \DateTime($filterBy['startDate']))->format('Y-m-d') : '';
        $endDate = $filterBy['endDate'] ? (new \DateTime($filterBy['endDate']))->format('Y-m-d') : '';

        $data = [];

        $lifeCycleIds=$this->getLayerLifeCycles($lifeCycleSlug, $filterBy);

        if(sizeof($lifeCycleIds)){
            foreach ($lifeCycleIds as $lifeCycleId=>$item) {
                $qb = $this->createQueryBuilder('e');

                $qb->join('e.crmLayerLifeCycle', 'crm_layer_life_cycle');
                $qb->join('crm_layer_life_cycle.report', 'report');
                $qb->join('crm_layer_life_cycle.employee', 'employee');
//        $qb->leftJoin('crm_layer_life_cycle.agent', 'agent');
                $qb->join('crm_layer_life_cycle.customer', 'customer');
                $qb->leftJoin('crm_layer_life_cycle.hatchery', 'hatchery');
                $qb->leftJoin('crm_layer_life_cycle.feed', 'feed');
                $qb->leftJoin('crm_layer_life_cycle.breed', 'breed');
                $qb->leftJoin('e.feedType', 'feed_type');
                $qb->leftJoin('e.feedMill', 'feed_mill');

                $qb->select('e AS details');
//        $qb->addSelect('agent.name AS agentName','agent.address AS agentAddress');
                $qb->addSelect('customer.name AS customerName','customer.address AS customerAddress','customer.mobile AS customerMobile');
                $qb->addSelect('crm_layer_life_cycle.id AS lifeCycleId','crm_layer_life_cycle.lifeCycleState', 'crm_layer_life_cycle.totalBirds', 'crm_layer_life_cycle.hatcheryDate');
                $qb->addSelect('hatchery.name AS hatcheryName');
                $qb->addSelect('feed.name AS feedName');
                $qb->addSelect('breed.name AS breedName');
                $qb->addSelect('feed_type.name AS feedTypeName');
                $qb->addSelect('feed_mill.name AS feedMillName');

                $qb->where('report.slug = :slug')->setParameter('slug', $lifeCycleSlug);
//                $qb->andWhere('e.visitingDate >= :startDate')->setParameter('startDate', $startDate);
                $qb->andWhere('e.ageWeek <= :maxWeek')->setParameter('maxWeek', (int)$item['maxWeek']);
                $qb->andWhere('employee.id = :employeeId')->setParameter('employeeId', $filterBy['employeeId']);
                $qb->andWhere('crm_layer_life_cycle.id = :id')->setParameter('id', $lifeCycleId);

                /*if(isset($filterBy['farmerId']) && $filterBy['farmerId']!=""){
                    $qb->andWhere('customer.id = :farmerId')->setParameter('farmerId', $filterBy['farmerId']);
                }*/


                $results = $qb->getQuery()->getArrayResult();

                foreach ($results as $result) {

                    $result['details']['feedTypeName'] = $result['feedTypeName'];
                    $result['details']['feedMillName'] = $result['feedMillName'];

                    $data[$result['lifeCycleId']]['details'][] = $result['details'];
                    $data[$result['lifeCycleId']]['parent'] = [
                        'customerName' => $result['customerName'],
                        'customerAddress' => $result['customerAddress'],
                        'customerMobile' => $result['customerMobile'],
                        'lifeCycleId' => $result['lifeCycleId'],
                        'lifeCycleState' => $result['lifeCycleState'],
                        'totalBirds' => $result['totalBirds'],
                        'hatchingDate' => $result['hatcheryDate'],
                        'hatcheryName' => $result['hatcheryName'],
                        'feedName' => $result['feedName'],
                        'breedName' => $result['breedName'],
                    ];
                }
            }
        }


        ksort($data);
//        dd($data);
        return $data;

    }

    private function getLayerLifeCycles($lifeCycleSlug, $filterBy){
        $startDate = $filterBy['startDate'] ? (new \DateTime($filterBy['startDate']))->format('Y-m-d') : '';
        $endDate = $filterBy['endDate'] ? (new \DateTime($filterBy['endDate']))->format('Y-m-d') : '';

        $qb = $this->createQueryBuilder('e');

        $qb->join('e.crmLayerLifeCycle', 'crm_layer_life_cycle');
        $qb->join('crm_layer_life_cycle.report', 'report');
        $qb->join('crm_layer_life_cycle.employee', 'employee');
        $qb->join('crm_layer_life_cycle.customer', 'customer');
        $qb->leftJoin('crm_layer_life_cycle.hatchery', 'hatchery');
        $qb->leftJoin('crm_layer_life_cycle.feed', 'feed');
        $qb->leftJoin('crm_layer_life_cycle.breed', 'breed');
        $qb->leftJoin('e.feedType', 'feed_type');
        $qb->leftJoin('e.feedMill', 'feed_mill');

        $qb->select('MAX(e.ageWeek) AS maxWeek');
        $qb->addSelect('crm_layer_life_cycle.id AS lifeCycleId');

        $qb->where('report.slug = :slug')->setParameter('slug', $lifeCycleSlug);
        if($startDate && $endDate){
            $qb->andWhere('e.visitingDate >= :startDate')->setParameter('startDate', $startDate);
            $qb->andWhere('e.visitingDate <= :endDate')->setParameter('endDate', $endDate);
        }

        $qb->andWhere('employee.id = :employeeId')->setParameter('employeeId', $filterBy['employeeId']);
        $qb->andWhere('crm_layer_life_cycle.lifeCycleState = :reportStatus')->setParameter('reportStatus', $filterBy['reportStatus']);

        if(isset($filterBy['farmerId']) && $filterBy['farmerId']!=""){
            $qb->andWhere('customer.id = :farmerId')->setParameter('farmerId', $filterBy['farmerId']);
        }
        $qb->groupBy('crm_layer_life_cycle.id');

        $results = $qb->getQuery()->getArrayResult();
//        dd($results);
        $returnArray=[];
        if($results){
            foreach ($results as $result) {
                $returnArray[$result['lifeCycleId']]=$result;
            }
        }
//dd($returnArray);
        return $returnArray;
    }

    public function getNumberOfReportsForKpi($board)
    {
        /**
         * @var EmployeeBoard $board
         */
        $startDate = (new \DateTime('01-' . date('m', strtotime($board->getMonth())) . '-' . $board->getYear()))->format('Y-m-d');
        $endDate = (new \DateTime('01-' . date('m', strtotime($board->getMonth())) . '-' . $board->getYear()))->format('Y-m-t');

        $qb = $this->createQueryBuilder('e');

        $qb->join('e.crmLayerLifeCycle', 'crm_layer_life_cycle');

        $qb->where('crm_layer_life_cycle.employee = :employee')->setParameter('employee',$board->getEmployee());
        $qb->andWhere('e.visitingDate >= :startDate')->setParameter('startDate', $startDate);
        $qb->andWhere('e.visitingDate <= :endDate')->setParameter('endDate', $endDate);

        return count($qb->getQuery()->getArrayResult());
    }
}
